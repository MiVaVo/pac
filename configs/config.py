import platform
class DefaultConfigs(object):
    plat=platform.system()
    if plat=="Windows":
        path_to_input="C:/PycharmProjects/PytorchTut/human_prot_atlas_tut/input/"
        train_data = path_to_input+"train/" # where is your train data
        test_data = path_to_input+"test/"   # your test data
        path_train_csv= path_to_input+"train.csv"
        path_test_csv = path_to_input+"sample_submission.csv"
        path_sample_submission_csv=path_to_input+"sample_submission.csv"
        weights = "./checkpoints/"
        best_models = "./checkpoints/best_models/"
        submit = "./submit/"
        model_name = "bninception_bcelog"
        ###### extension
        path_to_input_ext="C:/PycharmProjects/pac/input_ext/"
        path_train_csv_ext= path_to_input_ext + "HPAv18RBGY_wodpl.csv"  # where is your train data
        cuda_exists=False
    elif plat=="Linux":
        path_to_input = "/home/minasian/input/"
        train_data = path_to_input + "train/"  # where is your train data
        test_data = path_to_input + "test/"  # your test data
        path_train_csv = path_to_input + "train.csv"
        path_test_csv = path_to_input + "sample_submission.csv"
        path_sample_submission_csv = path_to_input + "sample_submission.csv"
        weights = "./checkpoints/"
        best_models = "./checkpoints/best_models/"
        submit = "./submit/"
        model_name = "bninception"
        path_to_input_ext = "/home/minasian/PycharmProjects/prot_atlas/pac/input_ext/HPAv18/"
        path_train_csv_ext = path_to_input_ext + "HPAv18RGBY_WithoutUncertain_wodpl.csv"  # where is your train data
        train_data_ext = path_to_input_ext+"train/" # where is your train data
        cuda_exists = True
    w_h_xception_custom = 299
    num_classes = 28
    img_weight = w_h_xception_custom
    img_height = w_h_xception_custom
    channels = 4
    lr = 0.03
    sampler_type="pobdict"
    batch_size = 50
    epochs = 65
    n_folds=7
    random_state=432
    ratio_of_ds_to_use=1
    min_number_of_occur_to_test=100
    # prob_of_increase_of_shape=
    # img_weight = 1024
    # img_height = 1024
config = DefaultConfigs()

# import cv2
# cv2.imread("/home/minasian/PycharmProjects/prot_atlas/pac/input_ext/HPAv18/train/10135_47_E8_2_red.jpg")
