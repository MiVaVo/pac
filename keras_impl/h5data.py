import numpy as np
import h5py
import gc

# simple template
hf = h5py.File('data.h5', 'w')
for i in range(0,10000):
    m=(np.random.random(size = (500,500,4))*255).astype("uint8")
    hf.create_dataset(str(i), data=m)
    gc.collect()
    print(i)
hf.close()


hfr = h5py.File('data.h5', 'r')

from datetime import  datetime as dt
st=dt.now()
n=1000
res_list=[]
for i in range(0,n):
    ret=np.asarray(hfr.get(str(i)))
    # res_list.append(ret)
    # np.asarray(ret)
end=dt.now()-st
print(end.total_seconds()/n)

########## Load my own data
hf = h5py.File('pac_base.h5', 'w')
for i in range(0,10000):
    m=(np.random.random(size = (500,500,4))*255).astype("uint8")
    hf.create_dataset(str(i), data=m)
    gc.collect()
    print(i)
hf.close()