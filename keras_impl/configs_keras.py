from configs.config import  config
BATCH_SIZE = 40
SHAPE = (128, 128, 4)
DIR =config.path_to_input
VAL_RATIO = 0.1 # 10 % as validation
THRESHOLD = 0.05 # due to different cost of True Positive vs False Positive, this is the probability threshold to predict the class as 'yes'
SEED=777