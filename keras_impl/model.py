from keras.legacy import layers
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential, load_model, Model
from keras.layers import Activation, Dropout, Flatten, Dense, Input, Conv2D, MaxPooling2D, BatchNormalization, \
    Concatenate, ReLU, LeakyReLU, GlobalAveragePooling2D
from keras.callbacks import ModelCheckpoint, LearningRateScheduler, EarlyStopping, ReduceLROnPlateau
from keras import metrics
from keras.optimizers import Adam
from keras.callbacks import ModelCheckpoint
from keras import backend as K
import keras
import tensorflow as tf
from keras_impl.configs_keras import *
nb_classes=28
# inception_input=keras.applications.inception_v3(include_top=False, weights=None,input_tensor=None, input_shape=(299,299,4), pooling=None, classes=1000)

def dilated_inception_block(input,n_filters):
    tower_1 = Conv2D(n_filters, (1,1), padding='same', activation='relu')(input)
    tower_1 = Conv2D(n_filters, (3,3), padding='same', activation='relu')(tower_1)
    tower_2 = Conv2D(n_filters, (1,1), padding='same', activation='relu')(input)
    tower_2 = Conv2D(n_filters, (5,5), padding='same', activation='relu')(tower_2)
    tower_3 = MaxPooling2D((3,3), strides=(1,1), padding='same')(input)
    tower_3 = Conv2D(n_filters, (1,1), padding='same', activation='relu')(tower_3)
    tower_dilated_1 = Conv2D(n_filters, (1,1), padding='same', activation='relu',dilation_rate=2)(input)
    tower_dilated_1  = Conv2D(n_filters, (3,3), padding='same', activation='relu',dilation_rate=2)(tower_dilated_1)
    tower_dilated_2 = Conv2D(n_filters, (1,1), padding='same', activation='relu',dilation_rate=4)(input)
    tower_dilated_2  = Conv2D(n_filters, (3,3), padding='same', activation='relu',dilation_rate=4)(tower_dilated_2)
    towers_output=keras.layers.concatenate([tower_1, tower_2, tower_3,
                                            tower_dilated_1,tower_dilated_2], axis = 3)
    return towers_output

def octupus_upper_block(input_tensor2,n_filters):
    channel_1=dilated_inception_block(input_tensor2,n_filters)
    channel_2=dilated_inception_block(input_tensor2,n_filters)
    channel_3=dilated_inception_block(input_tensor2,n_filters)
    octupus_output=keras.layers.concatenate([channel_1,channel_2,channel_3], axis = 3)
    output_after_conv=Conv2D(3,(1,1),padding="same",activation="relu")(octupus_output)
    return output_after_conv



def create_xception_with_octupus(shape,n_filters_octupus=12):
    input_image = Input(shape=shape)
    output_octupus = octupus_upper_block(input_image, n_filters=n_filters_octupus)
    octupus_model=Model(input_image,output_octupus)
    res_of_octopus=octupus_model(input_image)

    input_xception = Input(shape=(shape[0], shape[1], 3))
    xception_my = keras.applications.xception.Xception(include_top=False,
                                                       input_tensor=input_xception,
                                                       weights="imagenet",
                                                       input_shape=(shape[0], shape[1], 3),
                                                       pooling=None)

    x = xception_my.output
    x = GlobalAveragePooling2D()(x)
    output_final = Dense(nb_classes, activation='sigmoid')(x)
    my_own_final_excpetion_model=Model(input_xception,output_final)
    res_of_my_own_final_excpetion_model=my_own_final_excpetion_model(res_of_octopus)

    final_model=Model(input_image,res_of_my_own_final_excpetion_model)

    return final_model


# some basic useless model
def create_model(input_shape):
    dropRate = 0.25

    init = Input(input_shape)
    x = BatchNormalization(axis=-1)(init)
    x = Conv2D(8, (3, 3))(x)
    x = ReLU()(x)
    x = BatchNormalization(axis=-1)(x)
    x = Conv2D(8, (3, 3))(x)
    x = ReLU()(x)
    x = BatchNormalization(axis=-1)(x)
    x = Conv2D(16, (3, 3))(x)
    x = ReLU()(x)
    x = BatchNormalization(axis=-1)(x)
    x = MaxPooling2D(pool_size=(2, 2))(x)
    x = Dropout(dropRate)(x)
    c1 = Conv2D(16, (3, 3), padding='same')(x)
    c1 = ReLU()(c1)
    c2 = Conv2D(16, (5, 5), padding='same')(x)
    c2 = ReLU()(c2)
    c3 = Conv2D(16, (7, 7), padding='same')(x)
    c3 = ReLU()(c3)
    c4 = Conv2D(16, (1, 1), padding='same')(x)
    c4 = ReLU()(c4)
    x = Concatenate()([c1, c2, c3, c4])
    x = BatchNormalization(axis=-1)(x)
    x = MaxPooling2D(pool_size=(2, 2))(x)
    x = Dropout(dropRate)(x)
    x = Conv2D(32, (3, 3))(x)
    x = ReLU()(x)
    x = BatchNormalization(axis=-1)(x)
    x = MaxPooling2D(pool_size=(2, 2))(x)
    x = Dropout(dropRate)(x)
    x = Conv2D(64, (3, 3))(x)
    x = ReLU()(x)
    x = BatchNormalization(axis=-1)(x)
    x = MaxPooling2D(pool_size=(2, 2))(x)
    x = Dropout(dropRate)(x)
    x = Conv2D(128, (3, 3))(x)
    x = ReLU()(x)
    x = BatchNormalization(axis=-1)(x)
    x = MaxPooling2D(pool_size=(2, 2))(x)
    x = Dropout(dropRate)(x)
    # x = Conv2D(256, (1, 1), activation='relu')(x)
    # x = BatchNormalization(axis=-1)(x)
    # x = MaxPooling2D(pool_size=(2, 2))(x)
    # x = Dropout(0.25)(x)
    x = Flatten()(x)
    x = Dropout(0.5)(x)
    x = Dense(28)(x)
    x = ReLU()(x)
    x = BatchNormalization(axis=-1)(x)
    x = Dropout(0.1)(x)
    x = Dense(28)(x)
    x = Activation('sigmoid')(x)

    model = Model(init, x)

    return model