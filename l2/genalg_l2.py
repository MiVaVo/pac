from platypus import Problem, Real, GeneticAlgorithm
from sklearn.metrics import f1_score
import numpy as np

# df=pd.read_csv("2.csv")
# list(df["y_prob"][0])
# from six.moves import cPickle as pickle
# with open('d2.pkl', 'rb') as f:
#     dict_of_df=pickle.load(f)
# y_prob=np.asarray([dict_of_df["y_prob"][i] for i in range(0,len(dict_of_df["y_prob"].keys()))])[:,0,:].tolist()
# y_true=np.asarray([dict_of_df["y_true"][i] for i in range(0,len(dict_of_df["y_true"].keys()))]).tolist()
from utils import find_best_f1_and_ths_diversed


def fit_ga(y_true, y_prob, its=10000):
    def schaffer(x):
        y_pred = np.asarray(np.asarray(y_prob) > np.asarray(x)).astype(int)
        return [-f1_score(np.asarray(y_true), y_pred, average="macro")]

    problem = Problem(28, 1)
    # problem.types[☺ = [Real(0.2, 0.95) for i in range(0,28)]
    problem.types[:] = [Real(0.2, 0.8) for i in range(0, 28)]
    problem.function = schaffer
    algorithm = GeneticAlgorithm(problem, population_size=100, offspring_size=100)
    algorithm.run(its)
    feasible_solutions = [s for s in algorithm.result if s.feasible]
    print(feasible_solutions[0].objectives)
    print(feasible_solutions[0].variables)
    best_f1_ga = feasible_solutions[0].objectives
    best_ths_ga = feasible_solutions[0].variables

    best_th_loop, best_f1_loop = find_best_f1_and_ths_diversed(y_prob, y_true)
    return best_f1_ga, best_ths_ga, best_f1_loop, best_th_loop
################

# th_each_class,final_f1=find_best_f1_and_ths_diversed(output=np.asarray(y_prob),target=np.asarray(y_true))
# 0.5979 - best th based on f1 finder
# 0.57432
# 0.598513- best based on gen alg
# [feasible_solutions[i].objectives[0] for i in range(0,feasible_solutions.__len__())]
# feasible_solutions[4].variables
# averaged_ths=[i.variables for i in feasible_solutions]
# np.asarray(averaged_ths)

# def fine_best_ths_based_on_ga(y_true,y_prob):
# def f1_loss_function(x):
# # print(x)
# y_pred = np.asarray(np.asarray(y_prob) > np.asarray(x)).astype(int)
#
# return [-f1_score(np.asarray(y_true), y_pred, average="macro")]
#
# problem = Problem(28, 1)
# problem.types[☺ = [Real(0.01, 0.99) for i in range(0, 28)]
# # problem.types[☺ = [Subset(np.linspace(0,1,2), 28) for i in range(0,28)]
#
# problem.function = f1_loss_function
