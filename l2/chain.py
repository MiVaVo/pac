import pickle
import sklearn

from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import f1_score
from sklearn.preprocessing import MultiLabelBinarizer
import  numpy as np
import pandas as pd
from configs.config import config
from sklearn.multioutput import ClassifierChain

from src.helpers import prepare_validation_strategy

with open('embs_train.pkl', 'rb') as f:
    dict_of_df=pickle.load(f)

X=np.asarray([i[0] for i in dict_of_df])[:,0,:]
y=[[item for sublist in i[1].tolist()  for item in sublist] for i in dict_of_df]

all_files = pd.read_csv(config.path_train_csv)
test_files = pd.read_csv(config.path_test_csv)
all_files_shuffled = sklearn.utils.shuffle(all_files, random_state=config.random_state)
# print(all_files_shuffled.shape[0])
all_files_shuffled = all_files_shuffled.iloc[:int(all_files_shuffled.shape[0] * config.ratio_of_ds_to_use), :]
all_files_shuffled = all_files_shuffled.reset_index(drop=True)
all_files_strategy = prepare_validation_strategy(all_files_shuffled, n_folds=config.n_folds)
train=all_files_strategy[0]["train"]
test=all_files_strategy[0]["train"]
list_to_train=train.iloc[:,0].tolist()
X_train=[i[0] for i in dict_of_df if i[2][0][0].split("/")[-1] in list_to_train]
Y_train=[[item for sublist in i[1].tolist()  for item in sublist] for i in dict_of_df if i[2][0][0].split("/")[-1] in list_to_train]
X_test=[i[0] for i in dict_of_df if i[2][0][0].split("/")[-1] not in list_to_train]
Y_test= [[item for sublist in i[1].tolist()  for item in sublist] for i in dict_of_df if i[2][0][0].split("/")[-1] not in list_to_train]
mlb = MultiLabelBinarizer(classes = np.arange(0,config.num_classes))
Y_train=mlb.fit_transform(Y_train)
Y_test=mlb.fit_transform(Y_test)
X_train=np.asarray(X_train)[:,0,:]
X_test=np.asarray(X_test)[:,0,:]



########### 2. Fit chain
base_lr_gbm = RandomForestClassifier(n_estimators=10, max_depth=2,random_state=0,n_jobs=-1)
base_lr_gbm=LogisticRegression(solver='lbfgs',max_iter=1000)
chains = [ClassifierChain(base_lr_gbm, order='random', random_state=i)
          for i in range(10)]
for chain in chains:
    chain.fit(X_train, Y_train)

Y_pred_chains = np.array([chain.predict(X_test) for chain in
                          chains])
Y_pred_ensemble = Y_pred_chains.mean(axis=0)
Y_pred_ensemble_ing=np.asarray(Y_pred_ensemble>0.5).astype(int)

f1_batch_chain=f1_score(Y_test,Y_pred_ensemble_ing, average='macro')
print("f1_batch_gbm=",f1_batch_chain) #0.39513
