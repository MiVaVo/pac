import pickle
import sklearn

from sklearn.metrics import f1_score
from sklearn.multiclass import OneVsRestClassifier
from sklearn.preprocessing import MultiLabelBinarizer
import  numpy as np
import pandas as pd
from configs.config import config
import lightgbm as lgb
from joblib import dump, load

from src.helpers import prepare_validation_strategy

with open('embs_train.pkl', 'rb') as f:
    dict_of_df=pickle.load(f)

X=np.asarray([i[0] for i in dict_of_df])[:,0,:]
y=[[item for sublist in i[1].tolist()  for item in sublist] for i in dict_of_df]

all_files = pd.read_csv(config.path_train_csv)
test_files = pd.read_csv(config.path_test_csv)
all_files_shuffled = sklearn.utils.shuffle(all_files, random_state=config.random_state)
# print(all_files_shuffled.shape[0])
all_files_shuffled = all_files_shuffled.iloc[:int(all_files_shuffled.shape[0] * config.ratio_of_ds_to_use), :]
all_files_shuffled = all_files_shuffled.reset_index(drop=True)
all_files_strategy = prepare_validation_strategy(all_files_shuffled, n_folds=config.n_folds)
train=all_files_strategy[0]["train"]
test=all_files_strategy[0]["train"]
list_to_train=train.iloc[:,0].tolist()
X_train=[i[0] for i in dict_of_df if i[2][0][0].split("/")[-1] in list_to_train]
Y_train=[[item for sublist in i[1].tolist()  for item in sublist] for i in dict_of_df if i[2][0][0].split("/")[-1] in list_to_train]
X_test=[i[0] for i in dict_of_df if i[2][0][0].split("/")[-1] not in list_to_train]
Y_test= [[item for sublist in i[1].tolist()  for item in sublist] for i in dict_of_df if i[2][0][0].split("/")[-1] not in list_to_train]
mlb = MultiLabelBinarizer(classes = np.arange(0,config.num_classes))
Y_train=mlb.fit_transform(Y_train)
Y_test=mlb.fit_transform(Y_test)
X_train=np.asarray(X_train)[:,0,:]
X_test=np.asarray(X_test)[:,0,:]
########### 1. Fit logistic regression
# base_lr = RandomForestClassifier(n_estimators=100, max_depth=20,random_state=0,n_jobs=-1)
# ovr = OneVsRestClassifier(base_lr)
# ovr.fit(X_train, Y_train)
# Y_pred_ovr = ovr.predict(X_test)
# f1_batch = f1_score(Y_test,Y_pred_ovr, average='macro')
# print(f1_batch)

########### 1.2. Fit lgm
# base_lr_gbm = lgb.LGBMRegressor(num_leaves=31,
#                         learning_rate=0.05,
#                         n_estimators=20)
base_lr_gbm = lgb.LGBMClassifier(num_leaves=31,
                        learning_rate=0.05,
                        n_estimators=40)
# base_lr_gbm = RandomForestClassifier(n_estimators=100, max_depth=20,random_state=0,n_jobs=-1)
ovr_gbm = OneVsRestClassifier(base_lr_gbm,n_jobs=4)
ovr_gbm.fit(X_train, Y_train)
Y_pred_ovr_gbm = ovr_gbm.predict_proba(X_test)
Y_pred_ovr_gbm_int=np.asarray(Y_pred_ovr_gbm>0.1863).astype(int)
for key,value in enumerate(Y_pred_ovr_gbm_int):
    if np.sum(value!=0)==0:
        row_to_insert=np.asarray(Y_pred_ovr_gbm[key]==np.max(Y_pred_ovr_gbm[key])).astype(int)
        Y_pred_ovr_gbm_int[key]=row_to_insert
dump(ovr_gbm, 'ovr_gbm_lgb.joblib')


ovr_gbm = load("ovr_gbm_lgb.joblib")
Y_pred_ovr_gbm = ovr_gbm.predict_proba(X_test)
Y_pred_ovr_gbm_int = ovr_gbm.predict(X_test)

# Y_pred_ovr_gbm_int=np.asarray(Y_pred_ovr_gbm>0.1863).astype(int)

for i in np.linspace(0,1,100):
    Y_pred_int=np.asarray(Y_pred_ovr_gbm>i).astype(int)
    f1_batch_gbm = f1_score(Y_test,Y_pred_int, average='macro')
    print("f1_batch_gbm=",f1_batch_gbm,i)  #f1_batch_gbm= 0.4542520 (20) #f1_batch_gbm= 0.553608969108713 (120)
f1_batch_gbm = f1_score(Y_test,Y_pred_ovr_gbm_int, average='macro')



# from joblib import dump, load
dump(ovr_gbm, 'ovr_gbm.joblib')
# ovr_gbm = load("ovr_gbm.joblib")














