from tqdm import tqdm
from data_management.data import HumanDataset
from models.model import get_net
from utils import *
from configs.config import *
from datetime import datetime as dt
from torch.utils.data import DataLoader
import pandas as pd
######### 0. Prepare dt with files
n_folds = 1
all_files_test = pd.read_csv(config.path_test_csv)
# df_test=pd.read_csv(config.path_test_csv)
files=all_files_test
# files=df_test
########### 1. Prepare dataloader
current_model_path="/home/minasian/PycharmProjects/prot_atlas/pac/best_models_artem/bninception_bcelog_fold_0_model_best_loss.pth.tar"
get_tes = HumanDataset(files, config.test_data, augument=False, mode="pred", do_TTA=False)
loader_test = DataLoader(get_tes, 1, shuffle=False, pin_memory=True, num_workers=4)
best_model = torch.load(current_model_path)
# plt.plot(best_model["val_losses"])
model = get_net()
model.load_state_dict(best_model["state_dict"])
model.cuda()
model.eval()
########## debug loader
# image_var = x_row.cuda(non_blocking=True)
# y_pred = model(image_var)
# bs,aug,c,h,w=x_row.shape
# # x_row.view(-1,4,512,512).shape
# # y[1]
# y_true = np.asarray(y[1], dtype=np.float16)
# image_var = x.cuda(non_blocking=True)
# model(image_var)
########################
# cv2.imread("/home/minasian/input/train/00008af0-bad0-11e8-b2b8-ac1f6b6435d0_red.jpg")

def get_embeddings(image_var, model):
    my_embedding = torch.zeros([image_var.shape[0], 1024, 1, 1])
    layer = model._modules.get('global_pool')
    def copy_data(m, i, o):
        my_embedding.copy_(o.data)
    h = layer.register_forward_hook(copy_data)
    model(image_var)
    h.remove()
    return my_embedding

res_emb_true_tta_list=[]
for i, (x_row, y) in enumerate(tqdm(loader_test)):
    # x_row, y = next(iter(loader_test))
    x = x_row[0, :, :, :, :]
    y_id = y[0]
    y_true = np.asarray(y[1], dtype=np.float16)
    # x_row, y = next(iter(loader))
    image_var = x.cuda(non_blocking=True)
    st=dt.now()
    emb = get_embeddings(image_var, model)
    res_emb_true_tta_list.append([np.asarray(emb[:,:,0,0]),np.argwhere(y_true>0),y_id])

import pickle
with open('embs_test.pkl', 'wb') as f:
    pickle.dump(res_emb_true_tta_list, f)