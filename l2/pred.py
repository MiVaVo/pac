import pickle
import  numpy as np
import pandas as pd
from configs.config import config
from joblib import load

with open('/home/minasian/PycharmProjects/prot_atlas/pac/embs_test.pkl', 'rb') as f:
    dict_of_df=pickle.load(f)
X=np.asarray([i[0] for i in dict_of_df])[:,0,:]
y=np.asarray([i[2][0].split("/")[-1] for i in dict_of_df])
ovr_gbm = load("ovr_gbm_rf.joblib")
Y_pred_ovr_gbm = ovr_gbm.predict_proba(X)

Y_pred_ovr_gbm_int=np.asarray(Y_pred_ovr_gbm>0.1863).astype(int)
for key,value in enumerate(Y_pred_ovr_gbm_int):
    if np.sum(value!=0)==0:
        print(np.sum(value!=0),value)
        row_to_insert=np.asarray(Y_pred_ovr_gbm[key]==np.max(Y_pred_ovr_gbm[key])).astype(int)
        Y_pred_ovr_gbm_int[key]=row_to_insert
np.sum(np.sum(Y_pred_ovr_gbm_int,axis=1)==0)
all_files_test = pd.read_csv(config.path_test_csv)
# all_files_test.iloc[:,0].tolist()==y.tolist()
submission_args=[list(np.argwhere(i>0)[:,0]) for i in Y_pred_ovr_gbm_int]
submissions=[]
sample_submission_df = pd.read_csv(config.path_sample_submission_csv)

for i in submission_args:
    subrow=' '.join(list([str(m) for m in i]))
    submissions.append(subrow)
sample_submission_df['Predicted'] = submissions
sample_submission_df.to_csv("s5.csv", index=None)


df_test=pd.read_csv("s5.csv")
