import pickle
from iterstrat.ml_stratifiers import MultilabelStratifiedShuffleSplit
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import f1_score
from sklearn.multiclass import OneVsRestClassifier
from sklearn.preprocessing import MultiLabelBinarizer
import  numpy as np
from configs.config import config
from sklearn.multioutput import ClassifierChain
with open('embs.pkl', 'rb') as f:
    dict_of_df=pickle.load(f)

X=np.asarray([i[0] for i in dict_of_df])[:,0,:]
y=[[item for sublist in i[1].tolist()  for item in sublist] for i in dict_of_df]



mlb = MultiLabelBinarizer(classes = np.arange(0,config.num_classes))
y_bin=mlb.fit_transform(y)

rmskf = MultilabelStratifiedShuffleSplit(n_splits=1, test_size=0.25, random_state=config.random_state)

for train_index, test_index in rmskf.split(np.zeros(y_bin.shape[0]), y_bin):
    X_train=X[train_index]
    Y_train=y_bin[train_index]
    X_test=X[test_index]
    Y_test=y_bin[test_index]

clf = RandomForestClassifier(n_estimators=100, max_depth=2000,random_state=0,n_jobs=-1)
clf.fit(X_train, Y_train)
y_hat=clf.predict(X_test)
f1_batch = f1_score(Y_test,y_hat, average='macro')
print(f1_batch)
# 0.3675226005138682
#0.3650394544
#0.3677
######################### CHAIN CLASSIFIER ###############
########### 1. Fit logistic regression
base_lr = LogisticRegression(solver='lbfgs')
ovr = OneVsRestClassifier(base_lr)
ovr.fit(X_train, Y_train)
Y_pred_ovr = ovr.predict(X_test)

########### 2. Fit chain
chains = [ClassifierChain(base_lr, order='random', random_state=i)
          for i in range(10)]
for chain in chains:
    chain.fit(X_train, Y_train)