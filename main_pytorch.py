import time
import warnings
import  pandas as pd
import sklearn
from numpy.ma.bench import timer
from torch import optim
from torch.optim import lr_scheduler
from torch.utils.data import WeightedRandomSampler, DataLoader
from configs.weights import probdict
from data_management.data import HumanDataset
from models.model import get_xception
from src.forecast_best import make_forecast, prepare_submission_file
from src.helpers import make_folders, logger, return_mlsss
from src.libs import *
from utils import *
import gc
from datetime import  datetime as dt
if config.cuda_exists:
    os.environ["CUDA_VISIBLE_DEVICES"] = "0,1"
    torch.backends.cudnn.benchmark = True
warnings.filterwarnings('ignore')

config.train_data
log=logger()


def update_loaders(fold,include_ext,model_add_name,all_files_strategy_base,all_files_strategy_ext):
    include_ext=True
    config.model_name = "aug_with_ext" + model_add_name
    train_data_list_base = all_files_strategy_base[fold]["train"]
    val_data_list_base = all_files_strategy_base[fold]["test"]
    train_data_list_ext = all_files_strategy_ext[fold]["train"]
    val_data_list_ext = all_files_strategy_ext[fold]["test"]
    train_data_list_all = pd.concat([train_data_list_base, train_data_list_ext])
    labels_l_l = [list(map(int, train_data_list_all.iloc[i].Target.split(' '))) for i in
                  range(0, train_data_list_all.shape[0])]
    sampler = WeightedRandomSampler([max([probdict[j] for j in i]) for i in labels_l_l],
                                    num_samples=labels_l_l.__len__(), replacement=True)
    train_gen = HumanDataset(train_data_list_base, config.train_data,
                             train_data_list_ext, config.train_data_ext, augument=True,
                             mode="train",
                             there_is_extension=True)
    train_loader = DataLoader(train_gen, batch_size=config.batch_size, shuffle=False,
                              pin_memory=True, num_workers=config.num_workers, sampler=sampler)
    print(train_data_list_base.shape[0] + train_data_list_ext.shape[0])

    val_gen = HumanDataset(val_data_list_base, config.train_data,
                           val_data_list_ext, config.train_data_ext,
                           augument=False, mode="train", there_is_extension=True)
    val_loader = DataLoader(val_gen, batch_size=config.batch_size, shuffle=False, pin_memory=True,
                            num_workers=config.num_workers)
    train_data_list = [train_data_list_base, train_data_list_ext]
    val_data_list = [val_data_list_base, val_data_list_ext]
    return train_loader, val_loader, train_data_list, val_data_list


def train(train_loader, model, criterion, optimizer, epoch, valid_loss, best_results, start,scheduler):
    losses = AverageMeter()
    f1 = AverageMeter()
    # model.cuda()
    model.train()
    y_porbs=[]
    y_true=[]
    st=dt.now()
    # torch.cuda.empty_cache()
    for i, (images, target) in enumerate(train_loader):
        time_to_load=dt.now()-st
        images = images.cuda(non_blocking=True) if config.cuda_exists else images
        target= torch.from_numpy(np.array(target)).float().cuda(non_blocking=True) if config.cuda_exists else torch.from_numpy(np.array(target)).float()
        output = model(images)
        loss = criterion(output, target)
        losses.update(loss.item(), images.size(0))
        y_prob = output.sigmoid().cpu().data.numpy()
        y_porbs.append(y_prob)
        y_true.append(target.cpu().data.numpy())

        optimizer.zero_grad()
        # loss=nn.DataParallel(loss)
        loss.backward()
        optimizer.step()
        print('\r', end='', flush=True)

        message = '%s %5.1f %6.1f         |         %0.3f  %0.3f           |         %0.3f  %0.4f         |         %s  %s    | %s' % ( \
            "train", i / len(train_loader) + epoch, epoch,
            losses.avg, f1.avg,
            valid_loss[0], valid_loss[1],
            str(best_results[0])[:8], str(best_results[1])[:8],
            time_to_str((timer() - start), 'min'))
        print(message, end='', flush=True)
        # print("\n//////////////////////////////////////////////////////////// end of cycle /////////////////////////////////////////////////")
        # torch.cuda.empty_cache()
        st=dt.now()
    log.write("\n")


    y_true=np.concatenate(y_true)
    y_porbs=np.concatenate(y_porbs)
    th_each_class,f1_overall=find_best_f1_and_ths_diversed(y_porbs,y_true)
    th_unified, f1_unified=find_best_f1_and_ths_unified(y_porbs,y_true)
    print("Macro F1 div best train = ",round(f1_overall,3))# " Best th = ",th_each_class)
    print("Macro F1 unif best train = ",round(f1_unified,3))# " Best th = ",th_unified)
    return [losses.avg, f1.avg,
            th_each_class,f1_overall,
            th_unified,f1_unified]

def evaluate(val_loader,model,criterion,epoch,train_loss,best_results,start,current_lr):
    print("Start evalutaion at lr=",np.around(current_lr,5))
    losses = AverageMeter()
    f1 = AverageMeter()
    model.eval()
    y_porbs=[]
    y_true=[]
    st = dt.now()
    with torch.no_grad():
        for i, (images,target) in enumerate(val_loader):
            time_to_load = dt.now() - st
            images_var= images.cuda(non_blocking=True)
            target=torch.from_numpy(np.array(target)).float().cuda(non_blocking=True)

            output = model(images_var)
            loss = criterion(output,target)
            losses.update(loss.item(),images_var.size(0))
            y_porbs.append(output.sigmoid().cpu().data.numpy())
            y_true.append(target.cpu().data.numpy())

            print('\r',end='',flush=True)
            message = '%s   %5.1f %6.1f         |         %0.3f  %0.3f           |         %0.3f  %0.4f         |         %s  %s    | %s' % (\
                    "val", i/len(val_loader) + epoch, epoch,
                    train_loss[0], train_loss[1],
                    losses.avg, f1.avg,
                    str(best_results[0])[:8],str(best_results[1])[:8],
                    time_to_str((timer() - start),'min'))

            print(message, end='',flush=True)
            st = dt.now()

        log.write("\n")
        #log.write(message)
        #log.write("\n")

    y_true = np.concatenate(y_true)
    y_porbs = np.concatenate(y_porbs)
    th_each_class, f1_overall_val_th = find_best_f1_and_ths_diversed(y_porbs,y_true)
    th_each_class_train_th, f1_overall_train_th = find_best_f1_and_ths_diversed( y_porbs,y_true,th_each_class=train_loss[2])
    th_unified, f1_unified=find_best_f1_and_ths_unified(y_porbs,y_true)
    th_unified_train_th, f1_unified_train_th= find_best_f1_and_ths_unified(y_porbs,y_true,th=train_loss[4])
    print("Macro F1  div based on val ths = ", round(f1_overall_val_th,3))#  "Th div on this set = ", th_each_class)
    print("Macro F1  div based on train ths = ",round( f1_overall_train_th,3))#  "Th div on this set = ", th_each_class_train_th)
    print("Macro F1  unif based on val ths = ",round( f1_unified,3))#   "Th unif based on this set = ", th_unified)
    print("Macro F1  unif based based on train ths = ",round( f1_unified_train_th,3))#"Th unif based on this set = ", th_unified_train_th)


    return [losses.avg,f1.avg,
            th_each_class_train_th,f1_overall_train_th,
            th_each_class,f1_overall_val_th,
            th_unified_train_th,f1_unified_train_th,
            th_unified,f1_unified]

def main():
        include_ext=True
        n_folds=config.n_folds
        all_files_base = pd.read_csv(config.path_train_csv)
        labels_l_l = [list(map(int, all_files_base.iloc[i].Target.split(' '))) for i in range(0, all_files_base.shape[0])]
        all_files_strategy_base = return_mlsss(config.n_folds, 0.2, all_files_base, config,
                                                   shurely_train_bool=None)
        all_files_ext = pd.read_csv(config.path_train_csv_ext)
        all_files_ext = sklearn.utils.shuffle(all_files_ext, random_state=config.random_state)
        all_files_ext.reset_index(drop=True, inplace=True)
        all_files_strategy_ext = return_mlsss(config.n_folds, 0.2, all_files_ext, config,
                                                  shurely_train_bool=None)
        test_files = pd.read_csv(config.path_test_csv)

        for fold in range(0,n_folds):
            config.img_height = 299
            config.img_weight = 299
            config.batch_size = 45
            config.lr = 0.03
            config.epochs = 50
            config.step_size = 10
            config.gamma = 0.1
            config.num_workers = 6
            current_epoch=0
            print("Current states", config.img_height, config.batch_size)
            model=get_xception(pretrained=False,modified_last=False,with_dropout=False)
            model.cuda()
            model=nn.DataParallel(model)
            # if fold==0:
            #     m=torch.load("/home/minasian/checkpoints/aug_with_extxceptionimghw__299__BCE_ADAMX_full_train/0/checkpoint.pth.tar")
            #     model.load_state_dict(m["state_dict"])
            #     del m
            #     current_epoch=50
            #     config.lr=0.0012
            #     config.epochs=75

            # for total_modes in ["full_train"]:
            torch.cuda.empty_cache()
            model_add_name = "xception" + "imghw__" + str(config.img_weight) + "_" + "_BCE_ADAMX_"

            optimizer = optim.Adamax(model.parameters(), weight_decay=1e-06, lr=config.lr)
            criterion = nn.BCEWithLogitsLoss().cuda()
            # criterion = FocalLoss()
            best_results = [np.inf, 0]
            val_metrics = [np.inf, 0]
            print("Current fold "+str(fold))
            make_folders(fold)

            test_gens = HumanDataset(test_files, config.test_data, augument=False, mode="pred", do_TTA=True,
                                     there_is_extension=False)
            test_loader = DataLoader(test_gens,int(config.batch_size/1.5), shuffle=False, pin_memory=True, num_workers=config.num_workers)
            # scheduler = lr_scheduler.StepLR(optimizer,step_size=config.step_size,gamma=config.gamma)
            scheduler = lr_scheduler.ReduceLROnPlateau(optimizer,
                                                       factor=0.1,
                                                       patience=3,
                                                       min_lr=0.000000001)
            start = timer()
            val_hist=[]
            train_hist=[]
            train_loader, val_loader, train_data_list, val_data_list = update_loaders(fold,include_ext,model_add_name,all_files_strategy_base,all_files_strategy_ext)

            for epoch in range(current_epoch,config.epochs):
                # epoch=0
                # scheduler.step(epoch)
                lr=get_learning_rate(optimizer)
                if lr<=0.000000001:
                    break
                print( config.img_weight,config.batch_size,"ext=",include_ext,"Current LR=",lr)
                train_metrics = train(train_loader,model,criterion,optimizer,epoch,val_metrics,best_results,start,scheduler)
                # va
                # if (epoch)%5==0:
                # torch.cuda.empty_cache()
                lr=get_learning_rate(optimizer)
                val_metrics = evaluate(val_loader,model,criterion,epoch,train_metrics,best_results,start,lr)
                val_loss=val_metrics[0]
                scheduler.step(val_loss)
                is_best_loss = val_metrics[0] < best_results[0]
                best_results[0] = min(val_metrics[0],best_results[0])
                is_best_f1 = val_metrics[7] > best_results[1]
                best_results[1] = max(val_metrics[7],best_results[1])
                val_hist.append(val_metrics)
                train_hist.append(train_metrics)
                # save model
                save_checkpoint({
                            "epoch":epoch + 1,
                            "model_name":config.model_name+"best_f1_"+str(best_results[1])+"th_unif_train_"+str(val_metrics[6]),
                            "state_dict":model.state_dict(),
                            "best_loss":best_results[0],
                            "optimizer":optimizer.state_dict(),
                            "fold":fold,
                            "best_f1":best_results[1],
                            "val_hist":val_hist,
                            "best_f1_th_unified_train":val_metrics[6],
                            "best_f1_th_unified_val":val_metrics[8],
                            "best_f1_th_div_train":val_metrics[2],
                            "best_f1_th_div_val":val_metrics[4],
                            "train_hist": train_hist,
                            "train_data_list":train_data_list,
                            "val_data_list":val_data_list
                },is_best_loss,is_best_f1,fold,model_ext_name="best_f1_"+str(round(best_results[1],4))+"th_unif_train_"+str(round(val_metrics[6],4)))
                # print logs
                print('\r',end='',flush=True)
                log.write('%s  %5.1f %6.1f         |         %0.3f  %0.3f           |         %0.3f  %0.4f         |         %s  %s    | %s' % (\
                        "best", epoch, epoch,
                        train_metrics[0], train_metrics[1],
                        val_metrics[0], val_metrics[1],
                        str(best_results[0])[:8],str(best_results[1])[:8],
                        time_to_str((timer() - start),'min'))
                    )
                log.write("\n")
                log.write("\n")
                time.sleep(0.01)
            torch.cuda.empty_cache()
            path_to_save_forecast="/home/minasian/checkpoints/forecasts/"
            # model.eval()
            torch.cuda.empty_cache()

            df_output=make_forecast(model,
                          test_loader,
                          limit=None,
                          path_to_save_forecast=path_to_save_forecast+config.model_name+"fold___"+str(fold))
            all_preds = np.asarray([df_output["y_prob"].tolist()])
            input_idx = np.asarray([i.split("/")[-1] for i in df_output["idx"].tolist()])
            input_prob = np.mean(np.mean(all_preds[:, :, :, :], axis=0), axis=1)
            th_unified_train=np.around(val_metrics[6],3)
            th_unified_val=np.around(val_metrics[8],3)
            prepare_submission_file(input_prob, th=th_unified_train,
                                        name_of_submission="299_BCE_WS_ADAMAX_EXTTRUE_FOLD=" + str(fold) + "_th=" + str(
                                            th_unified_train) + "_")
            prepare_submission_file(input_prob, th=th_unified_val,
                                        name_of_submission="299_BCE_WS_ADAMAX_EXTTRUE_FOLD=" + str(fold) + "_th=" + str(
                                            th_unified_val) + "_")
            del model, train_loader,  test_loader
            gc.collect()
        torch.cuda.empty_cache()

# import torch

if __name__ == "__main__":
    main()

