import numpy as np
import pickle
import pandas as pd
from configs.config import config
with open('/home/minasian/PycharmProjects/prot_atlas/pac/list_of_filled_models_3_folds_TTA.pkl', 'rb') as f:
    list_of_filled_models=pickle.load(f)

all_fcs=[]
for i in range(0,list_of_filled_models.__len__()):
    fcs = []
    for j in range(0,list_of_filled_models[0]["mio"].__len__()):
        fcs.append(list_of_filled_models[i]["mio"][j]["probs_pred"])
    all_fcs.append(fcs)
all_fcs_array=np.asarray(all_fcs,dtype="float32")
# pred_super_mean=np.mean(all_fcs_array,axis=(0,2))
pred_super_mean=np.mean(all_fcs_array,axis=0)
pred_super_mean=np.mean(pred_super_mean,axis=1)
# pred_super_mean[0]>0.1836

mtr=[np.argwhere(i>0.1836)[:,0] for i in pred_super_mean]
submissions_array=np.asarray(mtr)
# [i.tolist() for i in mtr]
# list([str(m) for m in mtr])
# fcs.shape
# preds=[[j["probs_pred"] for j in i["mio"]] for i in list_of_filled_models]
# preds_array=np.asarray(preds)

# for i in range(0,df[0]['mio'].__len__()):
#     df[0]['mio'][i]["labels_pred"]=list(np.argwhere(np.mean(df[0]['mio'][i]["probs_pred"],axis=0)>0.184)[:,0])

# df[0]['mio'][0]["labels_pred"]=np.argwhere(np.mean(df[0]['mio'][0]["probs_pred"],axis=0)>0.184)[:,0]
sample_submission_df = pd.read_csv(config.path_sample_submission_csv)
submissions=[]
for i in submissions_array:
    subrow=' '.join(list([str(m) for m in i]))
    submissions.append(subrow)
sample_submission_df['Predicted'] = submissions
sample_submission_df.to_csv("v2_artem_best_th_3_folds_TTA.csv", index=None)