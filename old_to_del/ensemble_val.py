import sklearn
from data_management.data import HumanDataset
from models.model import get_net
from src.helpers import list_all_contained_files_in_dir, prepare_validation_strategy, make_forecast
from utils import *
import gc
from configs.config import *
from torch.utils.data import DataLoader
import random
import pandas as pd
# set random seed
random.seed(2050)
np.random.seed(2050)
torch.manual_seed(2050)
torch.cuda.manual_seed_all(2050)


######### 0. Prepare dt with files
n_folds=config.n_folds
# 4.2 get model
all_files = pd.read_csv(config.path_train_csv)
test_files = pd.read_csv(config.path_test_csv)
all_files_shuffled = sklearn.utils.shuffle(all_files,random_state=config.random_state)
# print(all_files_shuffled.shape[0])
all_files_shuffled=all_files_shuffled.iloc[:int(all_files_shuffled.shape[0]*config.ratio_of_ds_to_use),:]
all_files_shuffled=all_files_shuffled.reset_index(drop=True)
all_files_strategy=prepare_validation_strategy(all_files_shuffled,n_folds=n_folds)
######### 1. Prepare dataloader
model_pathes=list_all_contained_files_in_dir("/home/minasian/PycharmProjects/prot_atlas/pac/best_models_artem/",pattern="*_loss.pth.tar")
# model_pathes=["/home/minasian/PycharmProjects/prot_atlas/pac/best_models_artem_backup/best_models/bninception_bcelog_fold_1_model_best_loss.pth.tar"]
# best_model = torch.load(model_pathes[0])
# test_file=best_model["val_data_list"]
# best_model.keys()
dic_of_models={}
config.img_height=512
config.img_weight=512
for files,current_model_path in zip(all_files_strategy,sorted(model_pathes)):
    # print(model_path)
    # current_model_path=model_pathes[0]
    gen = HumanDataset(files["test"], config.train_data, augument=False, mode="val", do_TTA=True)
    data_loader = DataLoader(gen, 1, shuffle=False, pin_memory=True, num_workers=4)
    # input,filepath=next(iter(loader))
    # filepath[1]
    # model = get_se_resnext50_32x4d()
    model=get_net()
    ######## 3. Prepare submission dics
    # dic_of_models = {}
    dic_of_models[current_model_path] = dict(make_forecast(data_loader, model, current_model_path, limit=None))
    del gen, data_loader,model
    torch.cuda.empty_cache()
    gc.collect()
dict_of_df = {k: pd.DataFrame(v) for k, v in dic_of_models.items()}
import pickle
with open('bninception_bcelog_fold_1_model_best_loss_for_diff_th_VAL_TTA.pkl', 'wb') as f:
    pickle.dump(dict_of_df, f)


#
#     df_of_all_models = pd.concat(dict_of_df, axis=1)
#     all_preds = np.asarray([df_of_all_models[i]["y_prob"].tolist() for i in model_pathes])
#     best_averager = np.mean(np.mean(all_preds, axis=0), axis=1)
#
#
# # test_files = pd.read_csv(config.path_test_csv)
# gen=HumanDataset(files,path_to_files,augument=False,mode="pred",do_TTA=True)
# loader=DataLoader(gen,1,shuffle=False,pin_memory=True,num_workers=4)
# input,filepath=next(iter(loader))
# #TODO: 1) Make batch Dataloader for TTA 2) Make combiner across models 3) Show confusion matrix to identify weak places 4) Do cyclic learning rate
# ######## 2. Load model
# model = get_net()
# ######## 3. Prepare submission dics
# dic_of_models={}
# for current_model_path in model_pathes:
#     dic_of_models[current_model_path]=dict(make_forecast(loader,model,current_model_path,limit=None))
# dict_of_df = {k: pd.DataFrame(v) for k,v in dic_of_models.items()}
#
# all_preds=np.asarray([df_of_all_models[i]["y_prob"].tolist() for i in model_pathes])
# best_averager=np.mean(np.mean(all_preds,axis=0),axis=1)
#
# submission_args=[list(np.argwhere(i>0.1836)[:,0]) for i in best_averager]
# submissions=[]
# sample_submission_df = pd.read_csv(config.path_sample_submission_csv)
#
# for i in submission_args:
#     subrow=' '.join(list([str(m) for m in i]))
#     submissions.append(subrow)
# sample_submission_df['Predicted'] = submissions
# sample_submission_df.to_csv("v3_artem_finetuned_cv5.csv", index=None)
#
# import pickle
# with open('dict_of_df.pkl', 'wb') as f:
#     pickle.dump(dict_of_df, f)
#
# all_trues=np.asarray([df_of_all_models[i]["y_true"].tolist() for i in model_pathes])
#
# #         test_gens = HumanDataset(all_files, config.train_data, augument=False, mode="val", do_TTA=True)
# #         test_loader = DataLoader(test_gens, 1, shuffle=False, pin_memory=True, num_workers=8)
# #         df_output=make_forecast(test_loader,
# #                       model,
# #                       current_model_path="/home/minasian/PycharmProjects/prot_atlas/pac/checkpoints/best_models/bninception_bcelog_fold_2_model_best_f1.pth.tar")
# #         df_output["y_true"][0].shape
# #         list_of_filled_models.append({"model_path":current_model_path,"mio":list_of_current_model})
# #
# # list_of_filled_models[0]