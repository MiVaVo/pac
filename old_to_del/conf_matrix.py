import pickle
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix


# with open('dict_of_df_5_models_val.pkl', 'wb') as f:
#     pickle.dump(dict_of_df, f)
from sklearn.metrics import f1_score

with open('/home/minasian/PycharmProjects/prot_atlas/pac/pred_managemenet/dict_of_df_5_models_val.pkl', 'rb') as f:
    df=pickle.load(f)
df_new=df["/home/minasian/PycharmProjects/prot_atlas/pac/best_models_artem/bninception_bcelog_fold_3_model_best_loss.pth.tar"]
df_new.keys()
y_true=np.asarray(df_new["y_true"].tolist())[:,0,:]
y_hat=np.asarray(df_new["y_prob"].tolist())[:,0,:]
y_true_numb=np.asarray([np.argwhere(i>0)[:,0] for i in y_true])

y_hat_numb=np.asarray([np.argwhere(i>0.21)[:,0] for i in y_hat])

cnf_matrix = confusion_matrix(y_true_numb, y_hat_numb)


find_best_based_on = list(df.keys())
test_on_rest = list(df.keys())

df_1 = df[find_best_based_on[0]]
df_1.keys()
y_prob = np.asarray(df_1["y_prob"].tolist())[:, 0, :]
y_true = np.asarray(df_1["y_true"].tolist())[:, 0, :]
plt.subplot(221)  # equivalent to: plt.subplot(2, 2, 1)
f1_results_all = []
best_f1_scores = []
