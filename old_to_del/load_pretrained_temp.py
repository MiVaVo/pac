from models.model import get_net
from utils import *
import matplotlib.pyplot as plt
from configs.config import *

current_model_path="/home/minasian/artem_bm_v2/best_models/bninception_bcelog_fold_0_model_best_loss.pth.tar"
model = get_net()
chp=torch.load(current_model_path)
chp.keys()
best_model = torch.load(current_model_path) if config.cuda_exists else torch.load(current_model_path,
                                                                                  map_location='cpu')
# plt.plot(best_model["val_losses"])
best_model.keys()
best_model.keys()
plt.plot(best_model["val_losses"][30:])
model.load_state_dict(best_model["state_dict"])
model.cuda() if config.cuda_exists else None
model.eval()