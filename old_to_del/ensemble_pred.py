from data_management.data import HumanDataset
from models.model import get_se_resnext50_32x4d
from src.helpers import list_all_contained_files_in_dir, make_forecast
from utils import *
from configs.config import *
import pickle
from torch.utils.data import DataLoader
import random
import pandas as pd
# set random seed
random.seed(2050)
np.random.seed(2050)
torch.manual_seed(2050)
torch.cuda.manual_seed_all(2050)


######### 0. Prepare dt with filesl
# config.img_weight=512
# config.img_height=512
n_folds = config.n_folds
test_files = pd.read_csv(config.path_test_csv)
######### 1. Prepare dataloader
# test_files = pd.read_csv(config.path_test_csv)
test_gens=HumanDataset(test_files,config.test_data,augument=False,mode="pred",do_TTA=True,there_is_extension=False)
test_loader=DataLoader(test_gens,1,shuffle=False,pin_memory=True,num_workers=2)
# input,filepath=next(iter(test_loader))
#TODO: 1) Make batch Dataloader for TTA 2) Make combiner across models 3) Show confusion matrix to identify weak places 4) Do cyclic learning rate
######## 2. Load model
# model = get_net()
model=nn.DataParallel(get_se_resnext50_32x4d())
# model=get_net()
# model=get_resnet50()

model_pathes=list_all_contained_files_in_dir("/home/minasian/PycharmProjects/prot_atlas/pac/best_models_artem/",pattern="*_loss.pth.tar")
model_pathes=["/home/minasian/checkpoints/bnwith_aug_with_ext_withsampler_v2get_xceptionimghw__256_/0/checkpoint.pth.tar"]
# ######## 3. Make forecast (done once)


best_model=torch.load(model_pathes[0])
model.load_state_dict(best_model["state_dict"])
# best_model.keys()
# plt.plot([i[-7] for i in best_model["val_hist"]])
dic_of_models={}
# epoch=best_model["epoch"]
# th_each_class=best_model["val_hist"][epoch-1][2]
# f1_score=best_model["val_hist"][epoch-1][3]
# th_each_class=best_model["val_hist"][epoch-1][6]
# print(config.img_weight)
for current_model_path in model_pathes:
    dic_of_models[current_model_path]=dict(make_forecast(test_loader,model,current_model_path,limit=None))
# dic_of_models={}
# for current_model_path in model_pathes:
#     dic_of_models[current_model_path]=dict(make_forecast_without_sigmoid(test_loader,model,current_model_path,limit=None))


dict_of_df = {k: pd.DataFrame(v) for k,v in dic_of_models.items()}
dict_of_df[list(dict_of_df.keys())[0]]["y_prob"][0].shape
# dict_of_df["/home/minasian/artem_bm_v2/best_models/bninception_bcelog_fold_0_model_best_loss.pth.tar"]
# model_pathes=list(dict_of_df.keys())
# df_of_all_models = pd.concat(dict_of_df, axis=1)
# all_preds=np.asarray([df_of_all_models[i]["y_prob"].tolist() for i in model_pathes])
# expit(np.mean(np.mean(all_preds,axis=0),axis=1))

with open('/home/minasian/PycharmProjects/prot_atlas/pac/variables/bnwith_aug_wo_ext_withsampler_v2get_se_resnext50_32x4d_fold_0_model_best_f1_best_f1_0.4042th_unif_train_0.2121_.pkl', 'wb') as f:
    pickle.dump(dict_of_df, f)
import pickle
with open('/home/minasian/PycharmProjects/prot_atlas/pac/variables/bnwith_aug_wo_ext_withsampler_v2get_se_resnext50_32x4d_fold_0_model_best_f1_best_f1_0.4042th_unif_train_0.2121_.pkl', 'rb') as f:
    dict_of_df=pickle.load(f)
model_pathes=dict_of_df.keys()
df_of_all_models = pd.concat(dict_of_df, axis=1)

all_preds=np.asarray([df_of_all_models[i]["y_prob"].tolist() for i in model_pathes])
# all_preds=expit(all_preds)
best_averager_after_sigmoid=np.mean(np.mean(all_preds,axis=0),axis=1)

# geometric_mean_after_sigmoid=gmean(gmean(all_preds,axis=0),axis=1)


# best_averager=np.mean(np.mean(all_preds,axis=0),axis=1)
# th=np.asarray(pd.read_csv("meaned_th.csv",index_col=False))[:,0]
# th[th<0.1863]=0.1863
# th=np.asarray([0.4693877551020408, 0.4081632653061224, 0.4693877551020408, 0.44897959183673464, 0.4693877551020408, 0.42857142857142855, 0.42857142857142855, 0.44897959183673464, 0.4081632653061224, 0.36734693877551017, 0.36734693877551017, 0.4081632653061224, 0.3877551020408163, 0.44897959183673464, 0.4693877551020408, 0.32653061224489793, 0.36734693877551017, 0.36734693877551017, 0.3877551020408163, 0.4081632653061224, 0.3469387755102041, 0.42857142857142855, 0.4081632653061224, 0.4897959183673469, 0.4081632653061224, 0.44897959183673464, 0.4081632653061224, 0.22448979591836732])
# th=[0.48484848484848486, 0.38383838383838387, 0.4141414141414142, 0.393939393939394, 0.393939393939394, 0.4040404040404041, 0.38383838383838387, 0.42424242424242425, 0.27272727272727276, 0.27272727272727276, 0.23232323232323235, 0.42424242424242425, 0.4141414141414142, 0.393939393939394, 0.4646464646464647, 0.12121212121212122, 0.27272727272727276, 0.31313131313131315, 0.36363636363636365, 0.37373737373737376, 0.30303030303030304, 0.42424242424242425, 0.3535353535353536, 0.4646464646464647, 0.42424242424242425, 0.4646464646464647, 0.32323232323232326, 0.11111111111111112]
th=[0.48484848484848486, 0.43434343434343436, 0.4747474747474748, 0.4646464646464647, 0.48484848484848486, 0.43434343434343436, 0.4141414141414142, 0.4646464646464647, 0.393939393939394, 0.4141414141414142, 0.393939393939394, 0.48484848484848486, 0.4040404040404041, 0.393939393939394, 0.4747474747474748, 0.42424242424242425, 0.42424242424242425, 0.38383838383838387, 0.4444444444444445, 0.4141414141414142, 0.393939393939394, 0.4545454545454546, 0.43434343434343436, 0.4444444444444445, 0.4141414141414142, 0.4646464646464647, 0.4141414141414142, 0.32323232323232326]
th=0.19
submission_args=[list(np.argwhere(i>th)[:,0]) for i in best_averager_after_sigmoid]
submission_args=np.asarray(submission_args)
submission_args[[len(i)==0 for i in submission_args  ]]=np.asarray([i for i in np.argmax(best_averager_after_sigmoid[[len(i)==0 for i in submission_args  ]],axis=1)])
submission_args=[[i] if not isinstance(i,list) else i for i in  submission_args]
submission_args=[sorted(i) for i in submission_args]
submissions=[]
sample_submission_df = pd.read_csv(config.path_sample_submission_csv)
# np.sum([len(i)==0 for i in sample_submission_df.Predicted])


for i in submission_args:
    subrow=' '.join(list([str(m) for m in i]))
    submissions.append(subrow)
sample_submission_df['Predicted'] = submissions
sample_submission_df.to_csv("bnwith_aug_wo_ext_withsampler_v2get_se_resnext50_32x4d_fold_0_model_best_f1_best_f1_0.4042th_unif_train_0.2121_.csv", index=None)



all_trues=np.asarray([df_of_all_models[i]["y_true"].tolist() for i in model_pathes])



#         test_gens = HumanDataset(all_files, config.train_data, augument=False, mode="val", do_TTA=True)
#         test_loader = DataLoader(test_gens, 1, shuffle=False, pin_memory=True, num_workers=8)
#         df_output=make_forecast(test_loader,
#                       model,
#                       current_model_path="/home/minasian/PycharmProjects/prot_atlas/pac/checkpoints/best_models/bninception_bcelog_fold_2_model_best_f1.pth.tar")
#         df_output["y_true"][0].shape
#         list_of_filled_models.append({"model_path":current_model_path,"mio":list_of_current_model})
#
# list_of_filled_models[0]