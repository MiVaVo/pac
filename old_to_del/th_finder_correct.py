import pandas as pd
import  numpy as np
import numpy as np
import pickle

from sklearn.metrics import f1_score,accuracy_score

from utils import find_best_f1_and_ths_diversed

with open('d2.pkl', 'rb') as f:
    df=pickle.load(f)
df_keys=list(df.keys())
y_prob=df["y_prob"]
y_true=df["y_true"]
y_prob=np.asarray([y_prob[i] for i in y_prob.keys()])
y_true=np.asarray([y_true[i] for i in y_true.keys()])
y_prob=np.mean(y_prob,axis=1)
def find_best_f1_and_ths_diversed_prev(output,target,th_each_class=None):
    if th_each_class is None :
        f1_each_class=[]
        th_each_class=[]
        for each_class in range(0,28):
            # each_class=0
            f1_best=0
            th_best=0
            for th in np.linspace(0, 1, 100):
                # print(th)
                # th=0
                # f1_current=f1_score(target[:,each_class], np.asarray(output[:,each_class]>th).astype(int),average="macro")
                # print(th,f1_current)
                f1_current=accuracy_score(target[:,each_class], np.asarray(output[:,each_class]>th).astype(int))

                if f1_current>f1_best:
                    f1_best=f1_current
                    th_best=th
            f1_each_class.append(f1_best)
            th_each_class.append(th_best)
    final_f1=f1_score(target, np.asarray(output>np.asarray(th_each_class)).astype(int),average="macro")
    return th_each_class,final_f1

def find_best_f1_and_ths_diversed_correct(output,target,th_each_class=None):
    if th_each_class is None :
        f1_each_class=[]
        th_each_class=[]
        for each_class in range(0,28):
            # each_class=0
            f1_best=0
            th_best=0
            current_target = np.ones(target.shape)
            current_target[:, each_class] = target[:, each_class]
            for th in np.linspace(0, 1, 100):
                # print(th)
                # th=0

                current_output=np.ones(output.shape)
                current_output[:, each_class] =np.asarray(output[:,each_class]>th).astype(int)

                f1_current=f1_score(current_target, current_output,average="macro")
                # f1_current=accuracy_score(current_target, current_output)
                # print(th,f1_current)

                if f1_current>f1_best:
                    f1_best=f1_current
                    th_best=th
            f1_each_class.append(f1_best)
            th_each_class.append(th_best)
    final_f1=f1_score(target, np.asarray(output>np.asarray(th_each_class)).astype(int),average="macro")
    return th_each_class,final_f1


def find_best_f1_and_ths_diversed_correct_iterative_v2(output,target,th_each_class=None):
    if th_each_class is None :

        current_output = np.zeros(output.shape)
        current_target = target.copy()

        for its in range(0,3):
            f1_each_class = []
            th_each_class = []

            for each_class in range(0,28):
                # each_class=0
                f1_best=0
                th_best=0
                # current_target[:, each_class] = target[:, each_class]

                for th in np.linspace(0, 1, 20):
                    # print(th)
                    # th=0
                    current_output[:, each_class] = np.asarray(output[:, each_class] > th_best).astype(int)

                    f1_current=f1_score(current_target, current_output,average="macro")
                    # print(th,f1_current)

                    if f1_current>f1_best:
                        f1_best=f1_current
                        th_best=th
                current_output[:, each_class] = np.asarray(output[:, each_class] > th_best).astype(int)
                f1_each_class.append(f1_best)
                th_each_class.append(th_best)
    final_f1=f1_score(target, np.asarray(output>np.asarray(th_each_class)).astype(int),average="macro")
    return th_each_class,final_f1

th_each_class_prev,final_f1_prev=find_best_f1_and_ths_diversed_prev(y_prob,y_true)
# th_each_class_correct,final_f1_correct=find_best_f1_and_ths_diversed_correct(y_prob,y_true)
th_each_class_iterative,final_f1_iterative=find_best_f1_and_ths_diversed_correct_iterative_v2(y_prob,y_true)

print("prevt:",final_f1_prev)
# print("correct:",final_f1_correct)
print("iterative:",final_f1_iterative)

# correct if all are ones is better by 0.0002 it was 0.60