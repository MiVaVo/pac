import  pandas as pd
import  numpy as np
from six.moves import cPickle as pickle

from configs.config import config
with open("/home/minasian/checkpoints/forecasts/aug_wo_extget_xceptionimghw__299__FOCAL_Final_Fantazy with_samplerfold___0.pkl", 'rb') as f:
    df_output=pickle.load(f)



all_preds=np.asarray([df_output["y_prob"].tolist()])
input_idx=np.asarray([i.split("/")[-1] for i in df_output["idx"].tolist()])
input_prob=np.mean(np.mean(all_preds[:,:,:,:],axis=0),axis=1)
# pd.DataFrame(input_prob).to_csv("input_prob_299_MIGHT_BE_BEST.csv")
from configs.weights import th_dics
best_ths=np.asarray([th_dics[i] for i in th_dics.keys()])
# best_ths
# input_prob=np.asarray(pd.read_csv("input_prob_512.csv").iloc[:,1:])

def prepare_submission_file(input_prob,th,name_of_submission):
    '''
    :param input_prob:  array with float shaped (n_rows,28). NOT SHUFFLED !!
    :param input_idx: array of idx correspondence (n_rows,) SHOULD BE CAREFUL!!
    :param th:
    :param name_of_submission:
    :return:
    '''
    if isinstance(th,float):
        th_type="unf_th"
    else:
        th_type="dive"
        th=np.asarray(th)
    submission_args=[list(np.argwhere(i>th)[:,0]) for i in input_prob]
    submission_args=np.asarray(submission_args)
    # if there was no prediction => take the one with highes probability
    submission_args[[len(i)==0 for i in submission_args  ]]=np.asarray([i for i in np.argmax(input_prob[[len(i)==0 for i in submission_args  ]],axis=1)])
    submission_args=[[i] if not isinstance(i,list) else i for i in  submission_args]
    submission_args=[sorted(i) for i in submission_args]
    submissions=[]
    sample_submission_df = pd.read_csv(config.path_sample_submission_csv)
    for i in submission_args:
        subrow=' '.join(list([str(m) for m in i]))
        submissions.append(subrow)
    sample_submission_df['Predicted'] = submissions
    sample_submission_df.to_csv(name_of_submission+"_"+th_type+".csv", index=None)
prepare_submission_file(input_prob,th=0.3,name_of_submission="aug_wo_extget_xceptionimghw__299__FOCAL_Final_Fantazy with_samplerfold___0.csv")


