from configs.config import  config
from src.helpers import return_mlsss
import cv2

########################
import pandas as pd

# df = pd.read_csv(config.path_train_csv_ext)
#
# def download(id_):
#     # id_=df.Id[0]
#     try:
#         hpa_dir = Path('../HPAv18')
#         hpa_dir.mkdir(parents=True, exist_ok=True)
#         image_dir, _, image_id = id_.partition('_')
#         # url = "http://v18.proteinatlas.org/images/"+image_dir+"/"+image_id+"_blue_red_green_yellow.jpg"
#         # url = "http://v18.proteinatlas.org/images/"+image_dir+"/"+image_id+"_blue.jpg"
#         # r = requests.get(url)
#         # image = Image.open(BytesIO(r.content)).resize((512, 512), Image.LANCZOS)
#         # import numpy as np
#         # np.asarray(image).shape
#         # image.save(hpa_dir / str(id_+'.png'), format='png')
#         print(id_+" ok")
#     except:
#         print(id_ +'broke...')
#
# p = Pool()
# p.map(download, df.Id)
# ##############
n_folds=config.n_folds
fold=0
all_files_base = pd.read_csv(config.path_train_csv)
all_files_strategy_base = return_mlsss(config.n_folds, 0.2, all_files_base, config)
all_files_ext = pd.read_csv(config.path_train_csv_ext)
# all_files_ext=sklearn.utils.shuffle(all_files_ext,random_state=config.random_state)
# all_files_ext.reset_index(drop=True,inplace=True)
# all_files_strategy_ext = return_mlsss(config.n_folds, 0.2, all_files_ext, config)
for i in range(0,all_files_ext.shape[0]):
    for type in ["red","green","blue","yellow"]:
        img_path=config.train_data_ext+all_files_ext.iloc[i,0]+"_"+type+".jpg"
        img=cv2.imread(img_path)
        if img.shape[0]!=512:
            # print(img.shape)
            cv2.imwrite(img_path, cv2.resize(img,(512,512),interpolation=cv2.INTER_AREA))
    # all_files_ext.iloc[i,0]+"_green.jpg")
test_files = pd.read_csv(config.path_test_csv)
# cv2.imread("/home/minasian/input/train/e569f838-bba7-11e8-b2ba-ac1f6b6435d0_red.png",0)
# config.model_name="bnwith_aug_with_ext"
# train_data_list_base = all_files_strategy_base[fold]["train"]
# val_data_list_base = all_files_strategy_base[fold]["test"]
#
# train_data_list_ext = all_files_strategy_ext[fold]["train"]
# val_data_list_ext = all_files_strategy_ext[fold]["test"]
# print(train_data_list_base.shape[0]+train_data_list_ext.shape[0])
# train_gen = HumanDataset(train_data_list_base,config.train_data,
#                          train_data_list_ext, config.train_data_ext,augument=True,mode="val",there_is_extension=True)
# train_loader = DataLoader(train_gen,batch_size=config.batch_size,shuffle=True,pin_memory=True,num_workers=12)
#
# img,y=next(iter(train_loader))
# fast_plot(img.view(-1,512,512).numpy()[0]*255)
#
# val_gen = HumanDataset(val_data_list_base, config.train_data, augument=False, mode="train")
# val_loader = DataLoader(val_gen, batch_size=config.batch_size, shuffle=False, pin_memory=True,
#                         num_workers=6)
# train_data_list=[train_data_list_base,train_data_list_ext]
# val_data_list=[val_data_list_base]
#
# ##############
# import cv2
# #b-0,g-1,y-1,2 , r-2
# for i in ["blue","green","red","yellow"]:
#     img=cv2.imread("/home/minasian/PycharmProjects/prot_atlas/pac/input_ext/HPAv18/train/49495_735_G7_1_"+i+".jpg")
#     import matplotlib.pyplot as plt
#     import numpy as np
#     # # plt.imshow(img)
#     # [0.98039446 0.0079704  0.01163514]
#     # [0.10331002 0.79542022 0.10126977]
#     # [0.05202446 0.04258582 0.90538972]
#     # [0.0740666  0.46528988 0.46064352]
#     # img=cv2.resize(img,(512,512))
#     print(np.sum(img,axis=(0,1))/np.sum(img))
# img_here=img.reshape(-1,512,512)
# np.sum(img_here,axis=(1,2))
# fast_plot(img_here)