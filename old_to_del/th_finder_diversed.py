import numpy as np
import pickle
import  matplotlib.pyplot as plt
from sklearn.metrics import f1_score
import pandas as pd

with open('bninception_bcelog_fold_1_model_best_loss_for_diff_th_VAL_TTA.pkl', 'rb') as f:
    df=pickle.load(f)
df_keys=list(df.keys())
# np.asarray(df[df_keys[0]]["y_true"].tolist()).shape
f1_cv=[]
th_cv=[]
for i in range(0,len(df_keys)):
    # i=0
    df[df_keys[i]]["y_true"]
    target=np.asarray(df[df_keys[i]]["y_true"].tolist())/8
    output=np.asarray(df[df_keys[i]]["y_prob"].tolist())
    # target=np.asarray( [m["labels_true"] for m in df[0]["mio"]])[:,0,:]
    output=np.mean(output,axis=1)
target=target[:,0,:]
np.sum(target[:,22])
output.shape
# output=np.asarray([np.mean(df[0]["mio"][i]["probs_pred"],axis=0) for i in range(0,df[0]["mio"].__len__())])
# output=np.asarray([np.mean(df[0]["mio"][i]["probs_pred"],axis=0) for i in range(0,df[0]["mio"].__len__())])


pd.DataFrame(np.asarray(th_cv)).to_csv("th_cv_all.csv",index=False)
plt.plot(np.asarray(th_cv).T)
meaned_th=np.mean(th_cv,axis=0)
pd.DataFrame(meaned_th).to_csv("meaned_th.csv",index=False)

th_arr=np.asarray([th_results])
th_arr=np.asarray(th_results)
pd.DataFrame(th_arr).to_csv("th.csv",index=False)
th_arr[th_arr<0.10]=0.10
f1_score(target, output > th_arr, average='macro')

for th in np.linspace(0,1,50):
    f1_batch = f1_score(target, output > th, average='macro')
    f1_results.append([f1_batch,th])

f1_results=np.asarray(f1_results)
import matplotlib.pyplot as plt
plt.plot(f1_results[:,1],f1_results[:,0])
np.argmax(f1_results,axis=0)
f1_score_best,th_best=f1_results[np.argmax(f1_results[:,0])] #0.18367 - best threshold
# plt.axis(f1_results[:,1])
##################### For many models
with open('/home/minasian/PycharmProjects/prot_atlas/pac/list_of_filled_models_3_folds_TTA.pkl', 'rb') as f:
    list_of_filled_models=pickle.load(f)
all_fcs=[]
for i in range(0,list_of_filled_models.__len__()):
    fcs = []
    for j in range(0,list_of_filled_models[0]["mio"].__len__()):
        fcs.append(list_of_filled_models[i]["mio"][j]["probs_pred"])
    all_fcs.append(fcs)
all_fcs_array=np.asarray(all_fcs,dtype="float32")
# pred_super_mean=np.mean(all_fcs_array,axis=(0,2))
pred_super_mean=np.mean(all_fcs_array,axis=0)
pred_super_mean=np.mean(pred_super_mean,axis=1)