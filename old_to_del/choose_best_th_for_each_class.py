import pickle
import numpy as np
import matplotlib.pyplot as plt

# with open('dict_of_df_5_models_val.pkl', 'wb') as f:
#     pickle.dump(dict_of_df, f)
from sklearn.metrics import f1_score

with open('/home/minasian/PycharmProjects/prot_atlas/pac/pred_managemenet/dict_of_df_5_models_val.pkl', 'rb') as f:
    df=pickle.load(f)

find_best_based_on=list(df.keys())
test_on_rest=list(df.keys())

df_1=df[find_best_based_on[0]]
df_1.keys()
y_prob=np.asarray(df_1["y_prob"].tolist())[:,0,:]
y_true=np.asarray(df_1["y_true"].tolist())[:,0,:]
plt.subplot(221) # equivalent to: plt.subplot(2, 2, 1)
f1_results_all = []
best_f1_scores=[]

for model_pathes in find_best_based_on:
    df_current = df[model_pathes]
    df_current.keys()
    y_prob = np.asarray(df_current["y_prob"].tolist())[:, 0, :]
    y_true = np.asarray(df_current["y_true"].tolist())[:, 0, :]
    f1_results = []

    for th in np.linspace(0,1,100):
        f1_batch = f1_score(y_true, y_prob > th, average='macro')
        f1_results.append([f1_batch, th])
    f1_results_all.append(f1_results)
    f1_results=np.asarray(f1_results)
    f1_score_best, th_best = f1_results[np.argmax(f1_results[:, 0])]
    best_f1_scores.append([f1_score_best, th_best])
    plt.plot(f1_results[:,1],f1_results[:,0])
average_of_best_f1_scores=np.mean(best_f1_scores,axis=0)
plt.plot(np.mean(f1_results_all,axis=0)[:,1],np.mean(f1_results_all,axis=0)[:,0])
f1_results_averaged=np.mean(f1_results_all,axis=0)
f1_score_best,th_best=f1_results_averaged[np.argmax(f1_results_averaged[:,0])] #0.18367 - best threshold ,#0.262 - best threshold based on cv
best_of_averaged_f1_scores=f1_score_best

print(average_of_best_f1_scores[0],f1_score_best)
print(average_of_best_f1_scores[1],th_best)
