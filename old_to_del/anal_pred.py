from utils import *
import  matplotlib.pyplot as plt
from configs.config import *
import pickle
from configs.weights import label_dict
from configs.weights import probdict
from sklearn.metrics import roc_auc_score

# set random seed
######### 0. Prepare dt with filesl
config.img_weight=512
config.img_height=512
n_folds = config.n_folds

###########
def fast_plot(x):
    '''
    :param x:  [n,w,h] array
    :return:
    '''
    fig, axs = plt.subplots(2,2, figsize=(15, 6), facecolor='w', edgecolor='k')
    fig.subplots_adjust(hspace = .5, wspace=.001)
    axs = axs.ravel()
    for key,value in enumerate(x):
        axs[key].imshow(value)


# ######## 3. Make forecast (done once)
# model,chp=load_model("/home/minasian/checkpoints/best_models/bnwith_aug_with_ext_withsampler_v2get_xceptionimghw__512__FOCAL_temp_fold_0_model_best_loss_best_f1_0.6612th_unif_train_0.4747_.pth.tar",
#                      return_checkpoint=True,
#                      type="custom_512")
# model.cuda()
# val_df=chp["val_data_list"]
# # val_df=pd.concat(val_df)
# plt.plot([i[6] for i in chp["val_hist"]])
# plt.plot([i[9] for i in chp["val_hist"]])
# # 0.4747

#######
# weights=problist
# weights=[0.14472296145428357, 0.030204037426084608, 0.05350953937559427, 0.03322676201353685, 0.03615102651670727, 0.04260016203716727, 0.02778191935275154, 0.04564257864147589, 0.018378981303836566, 0.018300213236411098, 0.018132831093131987, 0.02861883006914711, 0.024631196655732907, 0.02314444938307724, 0.028352987841586165, 0.0180639090341347, 0.023075527324079963, 0.01992480462706133, 0.026738242459364115, 0.03244892734771038, 0.019550656306790367, 0.05504551669039086, 0.025753641616545794, 0.04705055784670609, 0.02102755757101785, 0.09887010020423434, 0.02108663362158695, 0.017965448949852872]
# probdict
# labels_l_l = [list(map(int, val_df.iloc[i].Target.split(' '))) for i in range(0,val_df.shape[0])]
# sampler=WeightedRandomSampler([max([probdict[j] for j in i]) for i in labels_l_l],num_samples=labels_l_l.__len__(),replacement=True)
#
# labels = [list(map(int, val_df.iloc[i].Target.split(' '))) for i in range(0,val_df.shape[0])]
# val_gen = HumanDataset(val_df, config.train_data, augument=False, mode="train",do_TTA=True)
#
# val_loader = DataLoader(val_gen, batch_size=28, shuffle=False, pin_memory=True, num_workers=6,sampler=sampler)
import numpy as np
# np.sum(np.asarray([[i,diffdict[i]] for i in diffdict.keys()])[:,1]>0.05)
# config.batch_size=28
# val_gen = HumanDataset(val_df[0], config.train_data,
#                        val_df[1],config.train_data_ext,
#                        augument=False, mode="val",do_TTA=True,
#                        there_is_extension=True)
# val_loader = DataLoader(val_gen, batch_size=config.batch_size, shuffle=False, pin_memory=True,num_workers=6)
# # val_loader = DataLoader(val_gen,  batch_size=28, shuffle=False, pin_memory=True, num_workers=6)
# data_loader=val_loader

# s=iter(val_loader)
# next(s)
# next(iter(val_loader))
# fc_df=make_forecast(model,val_loader,save_as="temp_val2")

with open('temp_val2.pickle', 'rb') as f:
    fc_df = pickle.load(f)

y_prob=fc_df["y_prob"]
array_of_pred=np.mean(np.asarray(y_prob.tolist())[:,:,:],axis=1)
array_of_true=np.asarray(fc_df["y_true"].tolist())

ths_best,f1_best=find_best_f1_and_ths_diversed(array_of_pred,array_of_true,delta=0.01)
ths_best,f1_best=find_best_f1_and_ths_unified(array_of_pred,array_of_true,delta=0.01)


# best_f1_ga,best_ths_ga,best_f1_loop,best_th_loop=fit_ga(y_true=array_of_true,y_prob=array_of_pred,its=10000)

# {i:best_th_loop[i] for i in range(0,best_th_loop.__len__())}


#best ths
roc_aucs=np.asarray([[roc_auc_score(array_of_true[:,i],array_of_pred[:,i]),np.sum(array_of_true[:,i])] for i in range(0,28)])[:,0]
print(np.mean(roc_aucs))
temp_difficulty=1-roc_aucs
difficulty=temp_difficulty/np.sum(temp_difficulty)
{i:difficulty[i] for i in range(0,difficulty.shape[0])}
# difficulty=difficulty/max(difficulty)

new_prob_list=np.asarray([probdict[i] for i in probdict.keys()])*difficulty
new_prob_list=new_prob_list/np.sum(new_prob_list)

# ########################################
# with open('/home/minasian/PycharmProjects/prot_atlas/pac/variables/anal_pred.pkl', 'wb') as f:
#     pickle.dump(fc_df, f)
# with open('/home/minasian/PycharmProjects/prot_atlas/pac/variables/anal_pred.pkl', 'rb') as f:
#     fc_df=pickle.load(f)
#
# import scipy
# y_prob=np.asarray(fc_df["y_prob"].tolist(),dtype="float32")
# y_prob_mean=np.median(y_prob,1)
# y_prob_geom_mean=scipy.stats.mstats.gmean(y_prob,1)
#
# y_true=np.mean(np.asarray(fc_df["y_true"].tolist(),dtype="float32"),axis=1)
#
# # difficulty=[[roc_auc_score(y_true[:,i],y_prob_mean[:,i]),np.sum(y_true[:,i])] for i in range(0,28)]
# difficulty=[roc_auc_score(y_true[:,i],y_prob_mean[:,i]) for i in range(0,28)]
# difficulty_new=(1-np.asarray(difficulty))/sum(1-np.asarray(difficulty))
#
#
# all_preds=np.asarray([df_of_all_models[i]["y_prob"].tolist() for i in model_pathes])
# # all_preds=expit(all_preds)
# best_averager_after_sigmoid=np.mean(np.mean(all_preds,axis=0),axis=1)
#
# from scipy.stats.mstats import gmean
# # geometric_mean_after_sigmoid=gmean(gmean(all_preds,axis=0),axis=1)
#
#
# # best_averager=np.mean(np.mean(all_preds,axis=0),axis=1)
# # th=np.asarray(pd.read_csv("meaned_th.csv",index_col=False))[:,0]
# # th[th<0.1863]=0.1863
# # th=np.asarray([0.4693877551020408, 0.4081632653061224, 0.4693877551020408, 0.44897959183673464, 0.4693877551020408, 0.42857142857142855, 0.42857142857142855, 0.44897959183673464, 0.4081632653061224, 0.36734693877551017, 0.36734693877551017, 0.4081632653061224, 0.3877551020408163, 0.44897959183673464, 0.4693877551020408, 0.32653061224489793, 0.36734693877551017, 0.36734693877551017, 0.3877551020408163, 0.4081632653061224, 0.3469387755102041, 0.42857142857142855, 0.4081632653061224, 0.4897959183673469, 0.4081632653061224, 0.44897959183673464, 0.4081632653061224, 0.22448979591836732])
# # th=[0.48484848484848486, 0.38383838383838387, 0.4141414141414142, 0.393939393939394, 0.393939393939394, 0.4040404040404041, 0.38383838383838387, 0.42424242424242425, 0.27272727272727276, 0.27272727272727276, 0.23232323232323235, 0.42424242424242425, 0.4141414141414142, 0.393939393939394, 0.4646464646464647, 0.12121212121212122, 0.27272727272727276, 0.31313131313131315, 0.36363636363636365, 0.37373737373737376, 0.30303030303030304, 0.42424242424242425, 0.3535353535353536, 0.4646464646464647, 0.42424242424242425, 0.4646464646464647, 0.32323232323232326, 0.11111111111111112]
# th=[0.48484848484848486, 0.43434343434343436, 0.4747474747474748, 0.4646464646464647, 0.48484848484848486, 0.43434343434343436, 0.4141414141414142, 0.4646464646464647, 0.393939393939394, 0.4141414141414142, 0.393939393939394, 0.48484848484848486, 0.4040404040404041, 0.393939393939394, 0.4747474747474748, 0.42424242424242425, 0.42424242424242425, 0.38383838383838387, 0.4444444444444445, 0.4141414141414142, 0.393939393939394, 0.4545454545454546, 0.43434343434343436, 0.4444444444444445, 0.4141414141414142, 0.4646464646464647, 0.4141414141414142, 0.32323232323232326]
# th=0.4141414141414142
# submission_args=[list(np.argwhere(i>th)[:,0]) for i in best_averager_after_sigmoid]
# submission_args=np.asarray(submission_args)
# submission_args[[len(i)==0 for i in submission_args  ]]=np.asarray([i for i in np.argmax(best_averager_after_sigmoid[[len(i)==0 for i in submission_args  ]],axis=1)])
# submission_args=[[i] if not isinstance(i,list) else i for i in  submission_args]
# submission_args=[sorted(i) for i in submission_args]
# submissions=[]
# sample_submission_df = pd.read_csv(config.path_sample_submission_csv)
# # np.sum([len(i)==0 for i in sample_submission_df.Predicted])
#
#
# for i in submission_args:
#     subrow=' '.join(list([str(m) for m in i]))
#     submissions.append(subrow)
# sample_submission_df['Predicted'] = submissions
# sample_submission_df.to_csv("wo_tta_bninception_bcelog_fold_cv_focal_loss_bnwith_aug_wo_ext_fold_0_model_best_loss_to_unified.csv", index=None)
#
#
#
# all_trues=np.asarray([df_of_all_models[i]["y_true"].tolist() for i in model_pathes])
#
#
#
# #         test_gens = HumanDataset(all_files, config.train_data, augument=False, mode="val", do_TTA=True)
# #         test_loader = DataLoader(test_gens, 1, shuffle=False, pin_memory=True, num_workers=8)
# #         df_output=make_forecast(test_loader,
# #                       model,
# #                       current_model_path="/home/minasian/PycharmProjects/prot_atlas/pac/checkpoints/best_models/bninception_bcelog_fold_2_model_best_f1.pth.tar")
# #         df_output["y_true"][0].shape
# #         list_of_filled_models.append({"model_path":current_model_path,"mio":list_of_current_model})
# #
# # list_of_filled_models[0]
# import numpy as np
#
# name_label_dict = {0:   ('Nucleoplasm', 12885),1:   ('Nuclear membrane', 1254),2:   ('Nucleoli', 3621),3:   ('Nucleoli fibrillar center', 1561),4:   ('Nuclear speckles', 1858),5:   ('Nuclear bodies', 2513),6:   ('Endoplasmic reticulum', 1008),7:   ('Golgi apparatus', 2822),8:   ('Peroxisomes', 53),9:   ('Endosomes', 45),10:  ('Lysosomes', 28),11:  ('Intermediate filaments', 1093),12:  ('Actin filaments', 688),13:  ('Focal adhesion sites', 537),14:  ('Microtubules', 1066),15:  ('Microtubule ends', 21),16:  ('Cytokinetic bridge', 530),17:  ('Mitotic spindle', 210),18:  ('Microtubule organizing center', 902),19:  ('Centrosome', 1482),20:  ('Lipid droplets', 172),21:  ('Plasma membrane', 3777),22:  ('Cell junctions', 802),23:  ('Mitochondria', 2965),24:  ('Aggresome', 322),25:  ('Cytosol', 8228),26:  ('Cytoplasmic bodies', 328),27:  ('Rods &amp; rings', 11)}
#
n_labels = 50782
#
def cls_wts(label_dict, mu=0.5):
    prob_dict, prob_dict_bal = {}, {}
    max_ent_wt = 1/28
    for i in range(28):
        prob_dict[i] = label_dict[i][1]/n_labels
        if prob_dict[i] > max_ent_wt:
            prob_dict_bal[i] = prob_dict[i]-mu*(prob_dict[i] - max_ent_wt)
        else:
            prob_dict_bal[i] = prob_dict[i]+mu*(max_ent_wt - prob_dict[i])
    return prob_dict, prob_dict_bal
#
list(cls_wts(label_dict)[1].values())
# "[0.14472296145428357, 0.030204037426084608, 0.05350953937559427, 0.03322676201353685, 0.03615102651670727, 0.04260016203716727, 0.02778191935275154, 0.04564257864147589, 0.018378981303836566, 0.018300213236411098, 0.018132831093131987, 0.02861883006914711, 0.024631196655732907, 0.02314444938307724, 0.028352987841586165, 0.0180639090341347, 0.023075527324079963, 0.01992480462706133, 0.026738242459364115, 0.03244892734771038, 0.019550656306790367, 0.05504551669039086, 0.025753641616545794, 0.04705055784670609, 0.02102755757101785, 0.09887010020423434, 0.02108663362158695, 0.017965448949852872]"