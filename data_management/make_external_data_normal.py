from utils import *
from configs.config import *
import pathlib
import cv2
import sklearn
import pandas as pd

all_files_ext = pd.read_csv(config.path_train_csv_ext)
all_files_ext = sklearn.utils.shuffle(all_files_ext, random_state=config.random_state)
all_files_ext.reset_index(drop=True, inplace=True)
# all_files_ext, config.train_data_ext
base_path_ext=config.train_data_ext
images_df_ext=all_files_ext
if not isinstance(base_path_ext, pathlib.Path):
    base_path_ext = pathlib.Path(base_path_ext)
images_df_ext = images_df_ext.copy()
images_df_ext.Id = images_df_ext.Id.apply(lambda x: str(base_path_ext) + "\\" + x)
images_df_ext.Id = images_df_ext.Id.apply(lambda x: "/t".join(("/".join(x.split("\\"))).split("\t")))


all_files_base = pd.read_csv(config.path_train_csv)
all_files_base = sklearn.utils.shuffle(all_files_base, random_state=config.random_state)
all_files_base.reset_index(drop=True, inplace=True)
# all_files_base, config.train_data_base
base_path_base=config.train_data
images_df_base=all_files_base
if not isinstance(base_path_base, pathlib.Path):
    base_path_base = pathlib.Path(base_path_base)
images_df_base = images_df_base.copy()
images_df_base.Id = images_df_base.Id.apply(lambda x: str(base_path_base) + "\\" + x)
images_df_base.Id = images_df_base.Id.apply(lambda x: "/t".join(("/".join(x.split("\\"))).split("\t")))
images_df_all=pd.concat([images_df_base,images_df_ext])
index=0
for index in range(0,images_df_all.shape[0]):
    row = images_df_all.iloc[index]
    filename = str(row.Id)
    imgs = np.stack([cv2.imread(filename + "_red" + ".png", -1),
                     cv2.imread(filename + "_green" + ".png", -1),
                     cv2.imread(filename + "_blue" + ".png", -1),
                     cv2.imread(filename + "_yellow" + ".png", -1)])
    assert imgs.shape[1]==512
    fp = np.memmap(filename+".memmap", dtype='uint8', mode='w+', shape=(4, 512, 512))
    fp[:] = imgs[:]
    if index%100==0:
        print(index)
        new_fp=np.memmap(filename+".memmap", dtype='uint8', mode='r', shape=(4, 512, 512))
        # a,b,c,d=np.asarray(new_fp)
        assert np.array_equal(np.asarray(fp),np.asarray(new_fp))
        del new_fp
    del fp

# for index in range(0,images_df_all.shape[0]):
#     row = images_df_all.iloc[index]
#     filename = str(row.Id)
#     opened_array = np.asarray(np.memmap(filename + ".memmap", dtype='uint8', mode='r', shape=(4, 512, 512)))
#     if np.sum(opened_array)==0:
#         print(index,"was empty")
#         imgs = np.stack([cv2.imread(filename + "_red" + ".png", -1),
#                          cv2.imread(filename + "_green" + ".png", -1),
#                          cv2.imread(filename + "_blue" + ".png", -1),
#                          cv2.imread(filename + "_yellow" + ".png", -1)])
#         assert imgs.shape[1] == 512
#         fp = np.memmap(filename+".memmap", dtype='uint8', mode='w+', shape=(4, 512, 512))
#         fp[:] = imgs[:]
#         del fp
#         del imgs
#     # if index%100==0:
#     #     print(index)
#     #     new_fp=np.memmap(filename+".memmap", dtype='uint8', mode='r', shape=(4, 512, 512))
#     #     # a,b,c,d=np.asarray(new_fp)
#     #     assert np.array_equal(np.asarray(fp),np.asarray(new_fp))
#     del opened_array



# t = Timer(lambda:   np.stack([cv2.imread(filename  + "_red"   + ".png",-1),
#                     cv2.imread(filename + "_green" + ".png",-1),
#                     cv2.imread(filename + "_blue"  + ".png",-1),
#                     cv2.imread(filename + "_yellow"+ ".png",-1)]))
# print(t.timeit(number=10000))

# timeit(cv2.imread(filename + "_red" + ".jpg",-1))
# timeit(cv2.imread(filename + "_red" + ".png",-1))
# timeit(cv2.imread(filename + "_red" + "2.jpg",-1))
# timeit([cv2.imread(filename  + "_red"   + ".png",-1),
#                 cv2.imread(filename + "_green" + ".png",-1),
#                 cv2.imread(filename + "_blue"  + ".png",-1),
#                 cv2.imread(filename + "_yellow"+ ".png",-1)])
# imgjpg=cv2.imread(filename + "_red" + ".jpg")
# img2jpg=cv2.imread(filename + "_red" + "2.jpg",-1)
# imgpng=cv2.imread(filename + "_red" + ".png",-1)
# np.array_equal(img2jpg,imgpng)

imgs=np.stack([cv2.imread(filename  + "_red"   + ".png",-1),
                cv2.imread(filename + "_green" + ".png",-1),
                cv2.imread(filename + "_blue"  + ".png",-1),
                cv2.imread(filename + "_yellow"+ ".png",-1)])
# imgs2=np.concatenate([imgs for i in range(0,10)],axis=0)
fp = np.memmap(filename, dtype='uint8', mode='w+', shape=(4,512, 512))
fp[:] =imgs[:]
del fp
newfp = np.memmap(filename, dtype='uint8', mode='r', shape=(4,512, 512))
del newfp

n=1000
from datetime import  datetime as dt
st=dt.now()
tot_imgs=[]
for i in range(0,n):
    imgs=np.asarray(np.memmap(filename, dtype='uint8', mode='r', shape=(4,512, 512)))
    tot_imgs.append(imgs)
end=dt.now()-st


print(end.total_seconds()/n)

tot_imgs_cv=[]
for i in range(0,n):
    imgs=np.stack([cv2.imread(filename  + "_red"   + ".png",-1),
                    cv2.imread(filename + "_green" + ".png",-1),
                    cv2.imread(filename + "_blue"  + ".png",-1),
                    cv2.imread(filename + "_yellow"+ ".png",-1)])
    tot_imgs_cv.append(imgs)

np.array_equal(np.concatenate(tot_imgs_cv,axis=0),np.concatenate(tot_imgs,axis=0))
# print(t.timeit(number=10000))
