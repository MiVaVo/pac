import itertools
from configs.config import config
import pandas as pd
import numpy as np

colors = ['red','green','blue','yellow']
DIR = "input/HPAv18/jpg/"
v18_url = 'http://v18.proteinatlas.org/images/'

img_list_new= pd.read_csv(config.path_train_csv_ext)
img_list_base=pd.read_csv(config.path_train_csv)

img_list_new.drop_duplicates(keep='first',inplace=True)

print(img_list_new.shape)
print(img_list_base.shape)

def return_uniques(img_list,simple=False):
    res_lists=[[int(j) for j in i.split(" ")] for i in img_list.iloc[:,1].tolist()]
    if simple==True:
        return res_lists
    merged = list(itertools.chain.from_iterable(res_lists))
    return np.unique(merged,return_counts=True)

res_base=return_uniques(img_list_base,simple=True)
np.unique(res_base,return_counts=True)[1].shape
res_new=return_uniques(img_list_new,simple=True)
np.unique(res_new,return_counts=True)[1].shape

import pandas as pd
img_list_new.iloc[:,0].shape
df_new=pd.concat([img_list_base,img_list_new])
df_new.shape
res_concat=return_uniques(df_new)
res_concat[1]/sum(res_concat[1])
########################################### Stratified  train test splits ##########
# from iterstrat.ml_stratifiers import RepeatedMultilabelStratifiedKFold,MultilabelStratifiedShuffleSplit
# import numpy as np
#
# X = np.array([[1,2], [3,4], [1,2], [3,4], [1,2], [3,4], [1,2], [3,4], [3,4]])
# y = np.array([[0,0,0], [0,0,0], [0,1,0], [0,1,0], [1,1,0], [1,1,0], [1,0,0], [1,0,0], [0,0,1]])
#
# rmskf = MultilabelStratifiedShuffleSplit(n_splits=5,test_size=0.5)
#
# for train_index, test_index in rmskf.split(np.zeros(y.shape[0]), y):
#    print("TRAIN:", train_index, "TEST:", test_index)
#    X_train, X_test = X[train_index], X[test_index]
#    y_train, y_test = y[train_index], y[test_index]
#
# #TODO:
# # I.Deal with additional data and stratified train-test split
# # 1) Make stratified train test split of base data, train 1 fold
# # 2) Also make stratified train test split of additional data , add fold_1_new to fold_1_base
# # 3) Compare how god model predicts in this two cases
# # II.Make oversampling
# # Based on the previously selected best model
# # 1) make stratified oversampling and make local validation.Then add statistic to my own docs
# # III. Compute f1 loss based on different tresholds