import h5py
from datetime import datetime as dt
from utils import *
from configs.config import *
from torch.utils.data import Dataset
from torchvision import transforms as T
from sklearn.preprocessing import MultiLabelBinarizer
from imgaug import augmenters as iaa
import pathlib
import cv2
import pandas as pd

class HumanDataset(Dataset):

    def __init__(self,images_df_base,  base_path_base,
                      images_df_ext=None,   base_path_ext=None,
                 augument=True,mode="train",do_TTA=False,there_is_extension=False,general_mode="classific"):
        '''
        Main class that is used in dataloader
        '''
        print("---------------------Running HumanDataset initialization--------------------------------")
        self.mode = mode
        self.do_TTA=do_TTA
        self.augument = augument
        self.general_mode=general_mode
        if not isinstance(base_path_base, pathlib.Path):
            base_path_base = pathlib.Path(base_path_base)
        # self.dah_type=dah_type
        self.images_df_base = images_df_base.copy()
        self.images_df_base.Id = self.images_df_base.Id.apply(lambda x:str(base_path_base)+"\\"+x)
        self.images_df_base.Id=self.images_df_base.Id.apply(lambda x:"/t".join(("/".join(x.split("\\"))).split("\t")))
        self.mlb = MultiLabelBinarizer(classes = np.arange(0,config.num_classes))
        self.mlb.fit(np.arange(0,config.num_classes))

        if there_is_extension:
            if not isinstance(base_path_ext, pathlib.Path):
                base_path_ext = pathlib.Path(base_path_ext)
            self.images_df_ext = images_df_ext.copy()
            self.images_df_ext.Id = self.images_df_ext.Id.apply(lambda x: str(base_path_ext) + "\\" + x)
            self.images_df_ext.Id = self.images_df_ext.Id.apply(lambda x: "/t".join(("/".join(x.split("\\"))).split("\t")))
            # self.mlb = MultiLabelBinarizer(classes=np.arange(0, config.num_classes))
            # self.mlb.fit(np.arange(0, config.num_classes))
            self.images_df=pd.concat([self.images_df_base,self.images_df_ext])

        else:
            self.images_df = self.images_df_base



    def __len__(self):
        return len(self.images_df)

    def __getitem__(self,index):

        self.h5files = h5py.File('/home/minasian/PycharmProjects/prot_atlas/pac/pac_all.h5', 'r')
        index=int(index)
        st=dt.now()
        X = self.read_images(index)
        if self.mode == "train":
            labels = np.array(list(map(int, self.images_df.iloc[index].Target.split(' '))))
            y  = np.eye(config.num_classes,dtype=np.float)[labels].sum(axis=0)
            if self.general_mode=="regress":
                y=np.asarray([sum(y)])
            if self.augument:
                X = self.augumentor(X)
            X = T.Compose([T.ToPILImage(), T.ToTensor()])(X)
            return X.float(), y

        elif self.mode == "pred":
            y_id = str(self.images_df.iloc[index].Id)
            y_true = []
            y = [y_id, y_true]
            if self.do_TTA:
                X = self.TTA(X)
                X = [T.Compose([T.ToPILImage(), T.ToTensor()])(i) for i in X]
                X = torch.stack(X)

                return X.float(), y
            else:
                X = np.expand_dims(X, 0)
                X = [T.Compose([T.ToPILImage(), T.ToTensor()])(i) for i in X]
                X = torch.stack(X)
                return X.float(), y

    def read_images(self,index):
        # print(index)
        row = self.images_df.iloc[index]
        filename = str(row.Id)
        if self.mode!="pred":
            return np.stack([cv2.resize(i.astype("uint8"), (config.img_weight, config.img_height), interpolation=cv2.INTER_AREA)
                         if i.shape[1] != config.img_height else i.astype("uint8") for i in
                         self.h5files.get(filename)[()]],
                         axis=-1)
        else:
            ending=".png"
            r = cv2.imread(filename + "_red" + ending, -1)
            g = cv2.imread(filename + "_green" + ending, -1)
            b = cv2.imread(filename + "_blue" + ending, -1)
            y = cv2.imread(filename + "_yellow" + ending, -1)
            return np.stack([cv2.resize(i, (config.img_weight, config.img_height), interpolation=cv2.INTER_AREA)
                         if i.shape[1] != config.img_height else i for i in
                         [r,g,b,y]],
                         axis=-1)


    def augumentor(self,image):
        img_initial_shapes=image.shape[1]
        augment_img =  iaa.Sequential([iaa.OneOf([
                        iaa.Noop(),
                        iaa.Affine(rotate=90),
                        iaa.Affine(rotate=180),
                        iaa.Affine(rotate=270),
                        iaa.Fliplr(1.0),
                        iaa.Sequential([iaa.Affine(rotate=90), iaa.Fliplr(1.0)]),
                        iaa.Sequential([iaa.Affine(rotate=180), iaa.Fliplr(1.0)]),
                        iaa.Sequential([iaa.Affine(rotate=270), iaa.Fliplr(1.0)])]),
            iaa.Sequential([iaa.Affine(
                # scale={"x": (0.8, 1.2), "y": (0.8, 1.2)},
                # translate_percent={"x": (-0.05, 0.05), "y": (-0.05, 0.05)},
                rotate=(-20, 20),
                shear=(-5, 5),
                fit_output=True,
                mode=["symmetric", "reflect"]
            ),iaa.Scale({"height": img_initial_shapes, "width": img_initial_shapes},interpolation="area")]),
            iaa.Affine(
                scale={"x": (0.8, 1.2), "y": (0.8, 1.2)},
                translate_percent={"x": (-0.05, 0.05), "y": (-0.05, 0.05)},
                mode=["symmetric", "reflect"]
            ),
                        iaa.OneOf([
                            iaa.CropAndPad(
                                percent=(0, 0.3),
                                pad_mode=["symmetric","reflect"]
                                # pad_cval=(0, 128)
                            ),
                            iaa.Multiply((0.9, 1.1), per_channel=False),
                            # iaa.Noop(),
                            iaa.Sometimes(0.2,
                                          iaa.GaussianBlur(sigma=(0, 0.5))
                                          ),

                            iaa.Sometimes(0.2,
                                          iaa.Crop(percent=0.1)
                                          )])
        # # ])
        ])
        return augment_img.augment_image(image)

    def TTA(self,image):
        fliped_img=iaa.Fliplr(1).augment_image(image)

        res_images= [t.augment_image(image) for t in [iaa.Affine(rotate=0),
                                                              iaa.Affine(rotate=90),
                                                              iaa.Affine(rotate=180),
                                                              iaa.Affine(rotate=270)]]+\
                    [t.augment_image(fliped_img) for t in [iaa.Affine(rotate=0),
                                                              iaa.Affine(rotate=90),
                                                              iaa.Affine(rotate=180),
                                                              iaa.Affine(rotate=270)]]
        return np.asarray(res_images)