import numpy as np
import torch
from torch.utils.data import DataLoader, WeightedRandomSampler
import pandas as pd
from configs.config import config
from configs.weights import probdict, diffdict,probdiffdict
from data_management.data import HumanDataset
from src.helpers import return_mlsss

config.train_data
include_ext = False
fold=0
n_folds = config.n_folds
# 4.2 get model
all_files_base = pd.read_csv(config.path_train_csv)
labels_l_l = [list(map(int, all_files_base.iloc[i].Target.split(' '))) for i in range(0, all_files_base.shape[0])]
uniques = np.unique([a for m in labels_l_l for a in m], return_counts=True)
shurely_train = uniques[0][uniques[1] < config.min_number_of_occur_to_test]
shurely_train_bool = [np.any([j in shurely_train for j in i]) for i in labels_l_l]
all_files_strategy_base = return_mlsss(config.n_folds, 0.2, all_files_base, config,
                                       shurely_train_bool=shurely_train_bool)
config.model_name = "aug_wo_ext_withsampler_" + "adawd"
train_data_list = all_files_strategy_base[fold]["train"]
val_data_list = all_files_strategy_base[fold]["test"]
print(train_data_list.shape)
labels_l_l = [list(map(int, train_data_list.iloc[i].Target.split(' '))) for i in
              range(0, train_data_list.shape[0])]
config.sampler_type="probdict"
if config.sampler_type=="probdict":
    sampler = WeightedRandomSampler([max([probdict[j] for j in i]) for i in labels_l_l],
                                    num_samples=labels_l_l.__len__(), replacement=True)
elif config.sampler_type=="diffdict":
    sampler=WeightedRandomSampler([max([diffdict[j] for j in i]) for i in labels_l_l],
                                num_samples=labels_l_l.__len__(), replacement=True)

sampler=WeightedRandomSampler([max([probdiffdict[j] for j in i]) for i in labels_l_l],
                                num_samples=labels_l_l.__len__(), replacement=True)

# sampler=WeightedRandomSampler(np.repeat(0.5,28).tolist(),
#                                 num_samples=labels_l_l.__len__(), replacement=True)
train_gen = HumanDataset(train_data_list, config.train_data, mode="train")
# train_gen[0]
train_loader = DataLoader(train_gen, batch_size=8, shuffle=False,
                                              pin_memory=True, num_workers=6)

probs_array=np.asarray([probdict[i] for i in probdict.keys()])
probs_array/np.sum(probs_array)
probdiff_array=np.asarray([probdiffdict[i] for i in probdiffdict.keys()])
probdiff_array/np.sum(probdiff_array)-probs_array/np.sum(probs_array)
############ prodiffdict is has less attention that probdict
train_iter=iter(train_loader)
# print(filename)
x,y=next(train_iter)
x=x.cpu().numpy()
y=y.cpu().numpy()
from src.funcs import fast_plot
x_to_plot=(x[:,1,:,:]*255).astype("uint8")
fast_plot(x_to_plot)
print(np.sum(np.asarray(y),axis=0))

device = torch.device("cuda" if torch.cuda.is_available()
                                  else "cpu")
x.to(device)
y.to(device)

#probdict
# array([30.,  6.,  8.,  4.,  3.,  7.,  3.,  2., 10.,  7.,  4.,  5.,  4.,
#         5.,  0.,  4., 11.,  7.,  7.,  2.,  5.,  6.,  4.,  6.,  7., 18.,
#         1.,  7.])
ds=pd.read_csv(config.path_train_csv)
train_gen = HumanDataset(ds,config.train_data,mode="val",do_TTA=True,augument=False)

train_loader = DataLoader(train_gen,batch_size=12,shuffle=True,pin_memory=True,num_workers=3)
x.view(-1,4,512,512)[39,3,323,232]
x[39//8,8-39//8,3,323,232]
x_np=train_gen[1][0].numpy()[0]
x_np=x_np[:,0,:,:]
np.unique(x_np,axis=0,return_counts=True)
uns=np.unique(x_np,axis=1,return_counts=True)

