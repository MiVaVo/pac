import  pandas as pd
import numpy as np
from PIL import Image
from configs.config import  config

all_files_base = pd.read_csv(config.path_train_csv)
all_files_ext = pd.read_csv("/home/minasian/PycharmProjects/prot_atlas/pac/input_ext/HPAv18/HPAv18RGBY_WithoutUncertain_wodpl.csv")
critical_number_of_occurancies=1000
ind_ext =np.asarray([[int(j) for j in i.split(" ")] for i in all_files_ext.iloc[:, 1].tolist()])
ind_base=np.asarray([[int(j) for j in i.split(" ")] for i in all_files_base.iloc[:, 1].tolist()])

unique_base=np.unique(ind_base,return_counts=True)
unique_ext=np.unique(ind_ext,return_counts=True)
np.unique([set(i)for i in ind_ext],return_counts=True)
surely_include=np.asarray([list(i) for i in list(set(map(tuple, [set(i)for i in unique_ext[0]]))-set(map(tuple,[set(i)for i in unique_base[0]])))])
additional_to_include= set(map(tuple,  [set(i) for i in unique_base[0][list(unique_base[1]<critical_number_of_occurancies)]])).\
    intersection(set(map(tuple, [set(i) for i in  unique_ext[0]])))
additional_to_include=np.asarray([list(i) for i in list(additional_to_include)])
all_include=np.concatenate([surely_include,additional_to_include])
all_include=[set(i) for i in all_include]
ind_ext_set=[set(i) for i in ind_ext]
res=[np.any([i==j for j in all_include]) for i in   ind_ext_set]

path_to_save=config.path_train_csv_ext.split(".")[0]+"_filered.csv"
all_files_ext_filtered=all_files_ext[res]
all_files_ext_filtered.reset_index(drop=True,inplace=True)
# all_files_ext_filtered.to_csv(path_to_save,index=False)


img_path=config.train_data_ext+ all_files_ext_filtered.iloc[124,0]+"_yellow.jpg"
import cv2

res_rgb=cv2.resize(cv2.imread(img_path),(512,512))

cv2.imshow("rgb",res_rgb)
cv2.imshow("gs",cv2.resize(cv2.imread(img_path,0),(512,512)))
cv2.imshow("rgb->max",np.max(res_rgb,axis=2))
cv2.imshow("PIL->l",cv2.resize(np.array(Image.open(img_path).convert('L')),(512,512)));cv2.waitKey(0);cv2.destroyAllWindows()


from PIL import Image
img=np.array(Image.open(img_path).convert('L'))
# plt.imshow(res)
# res[::5]
# import cv2
# from datetime import datetime as dt
# import matplotlib.pyplot as plt
# st=dt.now()
# img=cv2.imread(config.train_data_ext+all_files_ext_filtered.iloc[123,0]+"_yellow.jpg",0)
# cv2.resize(img,(512,512))
# print(dt.now()-st)
# path_ext=config.train_data_ext+all_files_ext_filtered.iloc[123,0]+"_yellow.jpg"
# path_base=config.train_data+all_files_base.iloc[123,0]+"_yellow.png"
# g1 = np.array(Image.open(path_ext))
# plt.imshow(g1)
# plt.imshow(g1)
# cv2.resize(g1.astype(np.uint8), (config.img_weight, config.img_height))
#
# g2 = cv2.imread(path_base,0)
# plt.imshow(g2)
# np.equal(g2,g1)
# g2[0,0,:]

