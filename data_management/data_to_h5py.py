import h5py
from utils import *
from configs.config import *
import pathlib
import cv2
import sklearn
import pandas as pd
all_files_ext = pd.read_csv(config.path_train_csv_ext)
all_files_ext = sklearn.utils.shuffle(all_files_ext, random_state=config.random_state)
all_files_ext.reset_index(drop=True, inplace=True)
# all_files_ext, config.train_data_ext
base_path_ext=config.train_data_ext
images_df_ext=all_files_ext
if not isinstance(base_path_ext, pathlib.Path):
    base_path_ext = pathlib.Path(base_path_ext)
images_df_ext = images_df_ext.copy()
images_df_ext.Id = images_df_ext.Id.apply(lambda x: str(base_path_ext) + "\\" + x)
images_df_ext.Id = images_df_ext.Id.apply(lambda x: "/t".join(("/".join(x.split("\\"))).split("\t")))


all_files_base = pd.read_csv(config.path_train_csv)
all_files_base = sklearn.utils.shuffle(all_files_base, random_state=config.random_state)
all_files_base.reset_index(drop=True, inplace=True)
# all_files_base, config.train_data_base
base_path_base=config.train_data
images_df_base=all_files_base
if not isinstance(base_path_base, pathlib.Path):
    base_path_base = pathlib.Path(base_path_base)
images_df_base = images_df_base.copy()
images_df_base.Id = images_df_base.Id.apply(lambda x: str(base_path_base) + "\\" + x)
images_df_base.Id = images_df_base.Id.apply(lambda x: "/t".join(("/".join(x.split("\\"))).split("\t")))
images_df_all=pd.concat([images_df_base,images_df_ext])
hfall = h5py.File('pac_all_2.h5', 'w')
for index in range(0,images_df_all.shape[0]):
    row = images_df_all.iloc[index]
    filename = str(row.Id)
    imgs = np.stack([cv2.imread(filename + "_red"    + ".png", -1),
                     cv2.imread(filename + "_green"  + ".png", -1),
                     cv2.imread(filename + "_blue"   + ".png", -1),
                     cv2.imread(filename + "_yellow" + ".png", -1)])
    assert imgs.shape[1]==512
    hfall.create_dataset(filename, data=imgs)
    # if index%1000==0:
    print(index)

hfall.close()
