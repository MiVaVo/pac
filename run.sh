#!/bin/bash
python3 -m virtualenv ./python_venvs/tg_3
source ./python_venvs/tg_3/bin/activate
pip install --upgrade pip
pip install -r requirements.txt -r requirements_wheels_u_gpu.txt
./python_venvs/tg_3/bin/python main.py