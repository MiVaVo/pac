import sys
import torch
import shutil
import numpy as np 
from configs.config import config
from torch import nn
import torch.nn.functional as F 
from sklearn.metrics import f1_score
import os


# save best model
def save_checkpoint(state, is_best_loss,is_best_f1,fold,model_ext_name):
    # filename_checkpoint=config.weights + config.model_name + os.sep +str(fold) + os.sep + "checkpoint.pth.tar"
    filename = config.weights + config.model_name + os.sep +str(fold) + os.sep + "checkpoint.pth.tar"
    directory=config.weights + config.model_name + os.sep +str(fold) + os.sep
    if not os.path.exists(directory):
        os.makedirs(directory)
    torch.save(state, filename)
    if is_best_loss:
        shutil.copyfile(filename,"%s/%s_fold_%s_model_best_loss_%s_.pth.tar"%(config.best_models,config.model_name,str(fold),model_ext_name))
    if is_best_f1:
        shutil.copyfile(filename,"%s/%s_fold_%s_model_best_f1_%s_.pth.tar"%(config.best_models,config.model_name,str(fold),model_ext_name))

# evaluate meters
class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

# print logger
class Logger(object):
    def __init__(self):
        self.terminal = sys.stdout  #stdout
        self.file = None

    def open(self, file, mode=None):
        if mode is None: mode ='w'
        self.file = open(file, mode)

    def write(self, message, is_terminal=1, is_file=1 ):
        if '\r' in message: is_file=0

        if is_terminal == 1:
            self.terminal.write(message)
            self.terminal.flush()
            #time.sleep(1)

        if is_file == 1:
            self.file.write(message)
            self.file.flush()

    def flush(self):
        # this flush method is needed for python 3 compatibility.
        # this handles the flush command by doing nothing.
        # you might want to specify some extra behavior here.
        pass

# class FocalLoss(nn.Module):
#     def __init__(self, alpha=0.25,gamma=2):
#         super(FocalLoss, self).__init__()
#         self.alpha = alpha
#         self.gamma = gamma
#
#     def forward(self, input, target):
#         '''Focal loss.
#         Args:
#           x: (tensor) sized [N,D].
#           y: (tensor) sized [N,].
#         Return:
#           (tensor) focal loss.
#         '''
#         t = Variable(target).cuda()  # [N,20]
#
#         p = input.sigmoid()
#         pt = p*t + (1-p)*(1-t)         # pt = p if t > 0 else 1-p
#         w = self.alpha*t + (1-self.alpha)*(1-t)  # w = alpha if t > 0 else 1-alpha
#         w = w * (1-pt).pow(self.gamma)
#         return F.binary_cross_entropy_with_logits(input, t, w, size_average=False)

def find_best_f1_and_ths_diversed(output,target,th_each_class=None,delta=0.01):
    if th_each_class is None :
        f1_each_class=[]
        th_each_class=[]
        for each_class in range(0,28):
            # each_class=0
            f1_best=0
            th_best=0
            for th in np.linspace(0, 1, 100):
                # print(th)
                # th=0
                f1_current=f1_score(target[:,each_class], np.asarray(output[:,each_class]>th).astype(int),average="macro")
                # print(th,f1_current)

                if f1_current>f1_best+delta:
                    f1_best=f1_current
                    th_best=th
            f1_each_class.append(f1_best)
            th_each_class.append(th_best)
    final_f1=f1_score(target, np.asarray(output>np.asarray(th_each_class)).astype(int),average="macro")
    return th_each_class,final_f1


def find_best_f1_and_ths_unified(output,target,th=None,delta=0.01):
    if th is None :
        f1_best=0
        th_best=0
        for th in np.linspace(0, 1, 100):
            # print(th)
            # th=0
            f1_current=f1_score(target, np.asarray(output>th).astype(int),average="macro")
            # print(th,f1_current)

            if f1_current>f1_best+delta:
                f1_best=f1_current
                th_best=th
        return th_best, f1_best
    else:
        final_f1=f1_score(target, np.asarray(output>th).astype(int),average="macro")
        return th,final_f1



class FocalLoss(nn.Module):
    def __init__(self, gamma=2):
        super().__init__()
        self.gamma = gamma

    def forward(self, input, target):
        if not (target.size() == input.size()):
            raise ValueError("Target size ({}) must be the same as input size ({})"
                             .format(target.size(), input.size()))

        max_val = (-input).clamp(min=0)
        loss = input-input * target + max_val + \
               ((-max_val).exp() + (-input - max_val).exp()).log()

        invprobs = F.logsigmoid(-input * (target * 2.0 - 1.0))
        loss = (invprobs * self.gamma).exp() * loss

        return loss.sum(dim=1).mean()


def get_learning_rate(optimizer):
    lr=[]
    for param_group in optimizer.param_groups:
       lr +=[ param_group['lr'] ]

    #assert(len(lr)==1) #we support only one param_group
    lr = lr[0]

    return lr

def time_to_str(t, mode='min'):
    if mode=='min':
        t  = int(t)/60
        hr = t//60
        min = t%60
        return '%2d hr %02d min'%(hr,min)

    elif mode=='sec':
        t   = int(t)
        min = t//60
        sec = t%60
        return '%2d min %02d sec'%(min,sec)


    else:
        raise NotImplementedError
