import torch
from pretrainedmodels import se_resnext50_32x4d, xception,inceptionresnetv2
from pretrainedmodels.models.inceptionresnetv2 import BasicConv2d
from pretrainedmodels.models import bninception,pnasnet5large
from torch import nn
from configs.config import config
from collections import OrderedDict
from torchvision.models import resnet50


def get_net(train_only_last_layers=False,path_to_model=None,type=1):
    model = bninception(pretrained="imagenet")


    model.global_pool = nn.AdaptiveAvgPool2d(1)
    model.conv1_7x7_s2 = nn.Conv2d(config.channels, 64, kernel_size=(7, 7), stride=(2, 2), padding=(3, 3))
    if type==1:
        model.last_linear = nn.Sequential(
                    nn.BatchNorm1d(1024),
                    nn.Dropout(0.5),
                    nn.Linear(1024, config.num_classes))
    else:
        model.last_linear = nn.Sequential(
                nn.BatchNorm1d(1024),
                nn.Dropout(0.5),
                nn.Linear(1024, 512),
                nn.Linear(512, config.num_classes),
            )
    if path_to_model is not None:
        # path_to_model = "/home/minasian/PycharmProjects/prot_atlas/pac/best_models_artem/bninception_bcelog_fold_0_model_best_loss.pth.tar"
        best_model = torch.load(path_to_model)
        # best_model = torch.load("checkpoints/bninception_bcelog/0/checkpoint.pth.tar")
        model.load_state_dict(best_model["state_dict"])
        if train_only_last_layers:
            for param in model.parameters():
                param.requires_grad = False
        model.last_linear = nn.Sequential(
            nn.BatchNorm1d(1024),
            nn.Dropout(0.5),
            nn.Linear(1024, config.num_classes),
        )
    if train_only_last_layers==False:
            for param in model.parameters():
                param.requires_grad = True
    return model

def get_net_v2(train_only_last_layers=False,path_to_model=None,type=1):
    model = bninception(pretrained="imagenet")
    model.conv1_7x7_s2 = nn.Conv2d(config.channels, 64, kernel_size=(7, 7), stride=(2, 2), padding=(3, 3))
    if type==1:
        # model.last_linear = nn.Sequential(
        #             nn.BatchNorm1d(1024),
        #             nn.Dropout(0.5),
        #             nn.Linear(1024, config.num_classes))
        model.last_linear = nn.Linear(1024, config.num_classes)
    else:
        model.last_linear = nn.Sequential(
                nn.BatchNorm1d(1024),
                nn.Dropout(0.5),
                nn.Linear(1024, 512),
                nn.Linear(512, config.num_classes),
            )
    if path_to_model is not None:
        # path_to_model = "/home/minasian/PycharmProjects/prot_atlas/pac/best_models_artem/bninception_bcelog_fold_0_model_best_loss.pth.tar"
        best_model = torch.load(path_to_model)
        # best_model = torch.load("checkpoints/bninception_bcelog/0/checkpoint.pth.tar")
        model.load_state_dict(best_model["state_dict"])
        if train_only_last_layers:
            for param in model.parameters():
                param.requires_grad = False
        model.last_linear = nn.Sequential(
            nn.BatchNorm1d(1024),
            nn.Dropout(0.5),
            nn.Linear(1024, config.num_classes),
        )
    if train_only_last_layers==False:
            for param in model.parameters():
                param.requires_grad = True
    return model

def get_pnasnet():
    model = pnasnet5large(pretrained="imagenet",num_classes=1000)
    model.conv_0 = nn.Sequential(OrderedDict([
        ('conv', nn.Conv2d(4, 96, kernel_size=3, stride=2, bias=False)),
        ('bn', nn.BatchNorm2d(96, eps=0.001))
    ]))
    # model.global_pool = nn.AdaptiveAvgPool2d(1)
    # model.conv1_7x7_s2 = nn.Conv2d(config.channels, 64, kernel_size=(7, 7), stride=(2, 2), padding=(3, 3))
    # model.last_linear = nn.Sequential(
    #             nn.BatchNorm1d(1024),
    #             nn.Dropout(0.5),
    #             nn.Linear(1024, config.num_classes),
    #         )
    model.last_linear = nn.Linear(4320, config.num_classes)
    return model

def get_se_resnext50_32x4d(dropout_p=0.2):
    # from torch import nn

    model = se_resnext50_32x4d(pretrained="imagenet",num_classes=1000)
    inplanes = 64
    layer0_modules = [
        ('conv1', nn.Conv2d(4, inplanes, kernel_size=7, stride=2,
                            padding=3, bias=False)),
        ('bn1', nn.BatchNorm2d(inplanes)),
        ('relu1', nn.ReLU(inplace=True)),
    ]

    layer0_modules.append(('pool', nn.MaxPool2d(3, stride=2,
                                                ceil_mode=True)))
    model.layer0 = nn.Sequential(OrderedDict(layer0_modules))
    # from config import config
    # model.avg_pool=nn.AdaptiveAvgPool2d(1)
    model.dropout = nn.Dropout(dropout_p)
    model.last_linear=nn.Linear(2048, config.num_classes)
    # model.last_linear = nn.Sequential(
    #             nn.BatchNorm1d(1024),
    #             nn.Dropout(0.5),
    #             nn.Linear(1024, config.num_classes),
    #         )
    return model

def get_xception(pretrained=True,modified_last=False,with_dropout=False):
    # from torch import nn
    if pretrained:
        model = xception(pretrained="imagenet",num_classes=1000)
    else:
        model = xception(pretrained=False, num_classes=config.num_classes)
    model.conv1 = nn.Conv2d(4, 32, 3, 2, 0, bias=False)
    if modified_last:
        model.last_linear = nn.Sequential(nn.Linear(2048, 128),nn.Dropout(0.5),nn.Linear(128, config.num_classes))
    else:
        model.last_linear = nn.Linear(2048, config.num_classes)
    if with_dropout:
        model.last_linear = nn.Sequential(nn.Dropout(0.0),nn.Linear(2048, config.num_classes))
    #         )
    return model

def get_resnet50():
    # from torch import nn
    model = resnet50(pretrained=True)
    model.conv1 = nn.Conv2d(4, 64, kernel_size=7, stride=2, padding=3,
                           bias=False)
    model.avgpool = nn.AdaptiveAvgPool2d(1)
    model.fc= nn.Linear(2048, config.num_classes)
    #         )
    return model

def get_inceptionresnetv2():
    # from torch import nn
    model = inceptionresnetv2(pretrained="imagenet")
    model.input_space = None
    model.input_size = (128, 128, 3)
    model.mean = None
    model.std = None

    model.conv2d_1a = BasicConv2d(4, 32, kernel_size=3, stride=2)
    model.maxpool_3a = nn.MaxPool2d(4, stride=2)
    model.maxpool_5a = nn.MaxPool2d(4, stride=2)

    model.avgpool_1a=nn.AdaptiveAvgPool2d(1)
    model.last_linear = nn.Linear(1536, config.num_classes)
    #         )
    return model
