from pathlib import Path
from torch.utils.data import WeightedRandomSampler
from configs.weights import probdict
from src.forecast_best import make_forecast
from src.helpers import make_folders, logger, return_mlsss
from src.libs import *
from utils import *
from datetime import  datetime as dt
from six.moves import cPickle as pickle
if config.cuda_exists:
    os.environ["CUDA_VISIBLE_DEVICES"] = "1"
    torch.backends.cudnn.benchmark = True
warnings.filterwarnings('ignore')

log=logger()
# device = torch.device("cuda" if torch.cuda.is_available()
#                                   else "cpu")
# device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
# class Loss_lr_it_epoch_tracker:
#     def __init__(self):
#         self.loss=[]
#         self.lr=[]

# lliet=Loss_lr_it_epoch_tracker()

def train(train_loader, model, criterion, optimizer, epoch, valid_loss, best_results, start,scheduler):
    losses = AverageMeter()
    f1 = AverageMeter()
    # model.cuda()
    model.train()
    y_porbs=[]
    y_true=[]
    st=dt.now()
    torch.cuda.empty_cache()
    for i, (images, target) in enumerate(train_loader):
        time_to_load=dt.now()-st
        images = images.cuda(non_blocking=True) if config.cuda_exists else images
        # print(images.shape)
        target= torch.from_numpy(np.array(target)).float().cuda(non_blocking=True) if config.cuda_exists else torch.from_numpy(np.array(target)).float()
        # compute output
        output = model(images)
        loss = criterion(output, target)
        # print(loss)
        losses.update(loss.item(), images.size(0))
        y_prob = output.sigmoid().cpu().data.numpy()
        y_porbs.append(y_prob)
        y_true.append(target.cpu().data.numpy())

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        print('\r', end='', flush=True)

        message = '%s %5.1f %6.1f         |         %0.3f  %0.3f           |         %0.3f  %0.4f         |         %s  %s    | %s' % ( \
            "train", i / len(train_loader) + epoch, epoch,
            losses.avg, f1.avg,
            valid_loss[0], valid_loss[1],
            str(best_results[0])[:8], str(best_results[1])[:8],
            time_to_str((timer() - start), 'min'))
        print(message, end='', flush=True)
        # print("\n//////////////////////////////////////////////////////////// end of cycle /////////////////////////////////////////////////")
        # torch.cuda.empty_cache()
        st=dt.now()
    log.write("\n")


    y_true=np.concatenate(y_true)
    y_porbs=np.concatenate(y_porbs)
    th_each_class,f1_overall=find_best_f1_and_ths_diversed(y_porbs,y_true)
    th_unified, f1_unified=find_best_f1_and_ths_unified(y_porbs,y_true)
    print("Macro F1 div best train = ",round(f1_overall,3))# " Best th = ",th_each_class)
    print("Macro F1 unif best train = ",round(f1_unified,3))# " Best th = ",th_unified)

    # log.write(message)
    # log.write("\n")
    return [losses.avg, f1.avg,
            th_each_class,f1_overall,
            th_unified,f1_unified]
# 2. evaluate fuunction

def evaluate(val_loader,model,criterion,epoch,train_loss,best_results,start,current_lr):
    print("Start evalutaion at lr=",np.around(current_lr,5))
    losses = AverageMeter()
    f1 = AverageMeter()
    # model.cuda()
    model.eval()
    y_porbs=[]
    y_true=[]
    st = dt.now()
    # torch.cuda.empty_cache()
    with torch.no_grad():
        for i, (images,target) in enumerate(val_loader):
            time_to_load = dt.now() - st
            # print("Time to val_loader=",time_to_load)
            images_var= images.cuda(non_blocking=True)
            target=torch.from_numpy(np.array(target)).float().cuda(non_blocking=True)

            output = model(images_var)
            loss = criterion(output,target)
            losses.update(loss.item(),images_var.size(0))
            y_porbs.append(output.sigmoid().cpu().data.numpy())
            y_true.append(target.cpu().data.numpy())

            print('\r',end='',flush=True)
            message = '%s   %5.1f %6.1f         |         %0.3f  %0.3f           |         %0.3f  %0.4f         |         %s  %s    | %s' % (\
                    "val", i/len(val_loader) + epoch, epoch,
                    train_loss[0], train_loss[1],
                    losses.avg, f1.avg,
                    str(best_results[0])[:8],str(best_results[1])[:8],
                    time_to_str((timer() - start),'min'))

            print(message, end='',flush=True)
            st = dt.now()

        log.write("\n")
        #log.write(message)
        #log.write("\n")

    y_true = np.concatenate(y_true)
    y_porbs = np.concatenate(y_porbs)
    th_each_class, f1_overall_val_th = find_best_f1_and_ths_diversed(y_porbs,y_true)
    th_each_class_train_th, f1_overall_train_th = find_best_f1_and_ths_diversed( y_porbs,y_true,th_each_class=train_loss[2])
    th_unified, f1_unified=find_best_f1_and_ths_unified(y_porbs,y_true)
    th_unified_train_th, f1_unified_train_th= find_best_f1_and_ths_unified(y_porbs,y_true,th=train_loss[4])
    # for th in np.linspace(0, 1, 100):
    #     f1_here = f1_score(y_true, y_porbs > th, average='macro')
    #     if f1_here > best_f1:
    #         best_f1 = f1_here
    #         best_th = th
    # best_th = round(best_th, 4)
    # best_f1 = round(best_f1, 4)
    print("Macro F1  div based on val ths = ", round(f1_overall_val_th,3))#  "Th div on this set = ", th_each_class)
    print("Macro F1  div based on train ths = ",round( f1_overall_train_th,3))#  "Th div on this set = ", th_each_class_train_th)
    print("Macro F1  unif based on val ths = ",round( f1_unified,3))#   "Th unif based on this set = ", th_unified)
    print("Macro F1  unif based based on train ths = ",round( f1_unified_train_th,3))#"Th unif based on this set = ", th_unified_train_th)


    return [losses.avg,f1.avg,
            th_each_class_train_th,f1_overall_train_th,
            th_each_class,f1_overall_val_th,
            th_unified_train_th,f1_unified_train_th,
            th_unified,f1_unified]

# 3. test model on public dataset and save the probability matrix
def test(test_loader,model,folds):
    sample_submission_df = pd.read_csv(config.path_sample_submission_csv)
    #3.1 confirm the model converted to cuda
    filenames,labels ,submissions= [],[],[]
    if config.cuda_exists:
        model.cuda()
    model.eval()
    submit_results = []
    for i,(input,filepath) in enumerate(tqdm(test_loader)):
        #3.2 change everything to cuda and get only basename
        filepath = [os.path.basename(x) for x in filepath]
        with torch.no_grad():
            image_var=input.cuda(non_blocking=True) if config.cuda_exists else input
            # image_var = input.cuda(non_blocking=True)
            y_pred = model(image_var)
            label = y_pred.sigmoid().cpu().data.numpy()
            #print(label > 0.5)

            labels.append(label > 0.15)
            filenames.append(filepath)

    for row in np.concatenate(labels):
        subrow = ' '.join(list([str(i) for i in np.nonzero(row)[0]]))
        submissions.append(subrow)
    sample_submission_df['Predicted'] = submissions
    sample_submission_df.to_csv('./submit/%s_bestloss_submission.csv'%config.model_name, index=None)
    # 4. main function

def main():
    # for include_ext in [False,True]:
        include_ext=True
        n_folds=config.n_folds
        # 4.2 get model

        if not Path("/home/minasian/PycharmProjects/prot_atlas/pac/temp_pkls/all_files_strategy_base.pkl").exists():
            all_files_base = pd.read_csv(config.path_train_csv)
            labels_l_l = [list(map(int, all_files_base.iloc[i].Target.split(' '))) for i in range(0, all_files_base.shape[0])]
            uniques = np.unique([a for m in labels_l_l for a in m], return_counts=True)
            shurely_train = uniques[0][uniques[1] < config.min_number_of_occur_to_test]
            shurely_train_bool = [np.any([j in shurely_train for j in i]) for i in labels_l_l]
            all_files_strategy_base = return_mlsss(config.n_folds, 0.2, all_files_base, config,
                                                   shurely_train_bool=shurely_train_bool)
            with open('/home/minasian/PycharmProjects/prot_atlas/pac/temp_pkls/all_files_strategy_base.pkl', 'wb') as handle:
                pickle.dump(all_files_strategy_base, handle, protocol=pickle.HIGHEST_PROTOCOL)
        else:
            with open('/home/minasian/PycharmProjects/prot_atlas/pac/temp_pkls/all_files_strategy_base.pkl', 'rb') as handle:
                all_files_strategy_base=pickle.load(handle)

        if not Path("/home/minasian/PycharmProjects/prot_atlas/pac/temp_pkls/all_files_strategy_ext.pkl").exists():
            all_files_ext = pd.read_csv(config.path_train_csv_ext)
            all_files_ext = sklearn.utils.shuffle(all_files_ext, random_state=config.random_state)
            all_files_ext.reset_index(drop=True, inplace=True)
            all_files_strategy_ext = return_mlsss(config.n_folds, 0.2, all_files_ext, config,
                                                  shurely_train_bool=None)

            with open('/home/minasian/PycharmProjects/prot_atlas/pac/temp_pkls/all_files_strategy_ext.pkl', 'wb') as handle:
                pickle.dump(all_files_strategy_ext, handle, protocol=pickle.HIGHEST_PROTOCOL)
        else:
            with open('/home/minasian/PycharmProjects/prot_atlas/pac/temp_pkls/all_files_strategy_ext.pkl', 'rb') as handle:
                all_files_strategy_ext = pickle.load(handle)
        test_files = pd.read_csv(config.path_test_csv)

        # print(all_files_strategy[0]["train"])
        for fold in range(0,n_folds):
            include_ext=False
            if fold>4:
                include_ext=True
                # config.epochs=
            # if fold>0:
            #     continue
            model_add_name = "get_xception"+"imghw__"+str(config.img_weight)+"_"+"_FOCAL_"
            model=get_xception(pretrained=False,modified_last=False,with_dropout=False)
            # model = nn.DataParallel(model)
            # model.std
            # model = model)
            # best_model = torch.load("/home/minasian/checkpoints/aug_with_ext_withsampler_get_xceptionimghw__299__FOCAL_temp/0/checkpoint_299_FL_sampler_clr_ext_29_epoch.tar")
            # best_model["val_data_list"][1]
            # np.sort(best_model["val_data_list"],axis=0).shape
            # np.sort(all_files_strategy[fold]["train"],axis=0).shape
            # model.load_state_dict(best_model["state_dict"])
            # del best_model
            model.cuda()
            # model=get_resnet50()
            # torch.cuda.empty_cache()
            # best_model=torch.load("/home/minasian/PycharmProjects/prot_atlas/pac/checkpoints/best_models/resnet50_fold_0_model_best_loss.pth.tar")
            # model.load_state_dict(best_model["state_dict"])
            # model = nn.DataParallel(model)

            # model=get_resnet50()
            # model=get_xception()
            # model=get_pnasnet()
            # model=get_se_resnext50_32x4d()
            # model=get_net_wo_bn()
            # model=nn.Sequential(nn.DataParallel(nn.Sequential(nn.Conv2d(in_channels=4,
            #                               out_channels=8,kernel_size=3,padding=1),nn.MaxPool2d(2,stride=2
            #                                                                ),
            #                                                   nn.Conv2d(in_channels=8,
            #                                                             out_channels=4,
            #                                                             kernel_size=3,
            #                                                             padding=1))),model)
            # best_model=torch.load("/home/minasian/checkpoints/best_models/bnwith_aug_with_ext_withsampler_v2get_xceptionimghw__512__FOCAL_temp_fold_0_model_best_f1_best_f1_0.6612th_unif_train_0.4747_.pth.tar")
            # model.load_state_dict(best_model["state_dict"])

            # criterion
            optimizer = optim.SGD(model.parameters(), lr=config.lr, momentum=0.9, nesterov=True)

            # optimizer = optim.Adamax(model.parameters(),weight_decay=1e-06,lr=config.lr)

            # for i in model.parameters():
            #     print(i.shape)
            # optimizer
            # criterion=Custom_Loss().cuda();
            criterion = nn.BCEWithLogitsLoss().cuda()
            # criterion =FocalLoss()
            # criterion = F1Loss().cuda()
            best_results = [np.inf, 0]
            val_metrics = [np.inf, 0]
            # fold=0

            print("Current fold "+str(fold))
            make_folders(fold)
            # load dataset
            model_add_name+="Final_Fantazy"

            def update_loaders(fold):
                if include_ext == False:
                    config.model_name = "aug_wo_ext" + model_add_name
                    train_data_list = all_files_strategy_base[fold]["train"]
                    val_data_list = all_files_strategy_base[fold]["test"]
                    print(train_data_list.shape)
                    labels_l_l = [list(map(int, train_data_list.iloc[i].Target.split(' '))) for i in
                                  range(0, train_data_list.shape[0])]
                    train_gen = HumanDataset(train_data_list, config.train_data, mode="train")
                    sampler = WeightedRandomSampler([max([probdict[j] for j in i]) for i in labels_l_l],
                                                    num_samples=labels_l_l.__len__(), replacement=True)
                    if fold%2==0:
                        config.model_name =config.model_name +" with_sampler"
                    # if config.sampler_type=="probdict":

                    # elif config.sampler_type=="diffdict":
                    #     sampler=WeightedRandomSampler([max([diffdict[j] for j in i]) for i in labels_l_l],
                    #                                 num_samples=labels_l_l.__len__(), replacement=True)
                    # elif config.sampler_type=="probdiffdict":
                    #     sampler = WeightedRandomSampler([max([probdiffdict[j] for j in i]) for i in labels_l_l],
                    #                                     num_samples=labels_l_l.__len__(), replacement=True)
                        train_loader = DataLoader(train_gen, batch_size=config.batch_size, shuffle=False,
                                                  pin_memory=True, num_workers=6, sampler=sampler)
                        # train_loader = DataLoader(train_gen, batch_size=config.batch_size, shuffle=True,
                        #                           pin_memory=True, num_workers=6)
                    else:
                        config.model_name = config.model_name + " wo_sampler"
                        train_loader = DataLoader(train_gen, batch_size=config.batch_size, shuffle=True,
                                                  pin_memory=True, num_workers=6)

                    # x,y=next(iter(train_loader))
                    # x=np.asarray(x)
                    # np.max(x)
                    val_gen = HumanDataset(val_data_list, config.train_data, augument=False, mode="train")
                    val_loader = DataLoader(val_gen, batch_size=config.batch_size*4, shuffle=False,
                                            pin_memory=True, num_workers=6)
                if include_ext == True:
                    config.model_name = "aug_with_ext" + model_add_name
                    train_data_list_base = all_files_strategy_base[fold]["train"]
                    val_data_list_base = all_files_strategy_base[fold]["test"]
                    train_data_list_ext = all_files_strategy_ext[fold]["train"]
                    val_data_list_ext = all_files_strategy_ext[fold]["test"]
                    train_data_list_all = pd.concat([train_data_list_base, train_data_list_ext])
                    labels_l_l = [list(map(int, train_data_list_all.iloc[i].Target.split(' '))) for i in
                                  range(0, train_data_list_all.shape[0])]
                    sampler = WeightedRandomSampler([max([probdict[j] for j in i]) for i in labels_l_l],
                                                    num_samples=labels_l_l.__len__(), replacement=True)
                    train_gen = HumanDataset(train_data_list_base, config.train_data,
                                             train_data_list_ext, config.train_data_ext, augument=True,
                                             mode="train",
                                             there_is_extension=True)
                    if fold%2==0:
                        config.model_name =config.model_name +" with_sampler"

                        train_loader = DataLoader(train_gen, batch_size=config.batch_size, shuffle=False,
                                                  pin_memory=True, num_workers=6, sampler=sampler)
                    # if config.sampler_type=="probdict":
                    #     sampler = WeightedRandomSampler([max([probdict[j] for j in i]) for i in labels_l_l],
                    #                                     num_samples=labels_l_l.__len__(), replacement=True)
                    # elif config.sampler_type=="diffdict":
                    #     sampler=WeightedRandomSampler([max([diffdict[j] for j in i]) for i in labels_l_l],
                    #                                 num_samples=labels_l_l.__len__(), replacement=True)
                    # elif config.sampler_type=="probdiffdict":
                    #     sampler = WeightedRandomSampler([max([probdiffdict[j] for j in i]) for i in labels_l_l],
                    #                                     num_samples=labels_l_l.__len__(), replacement=True)


                    # train_loader = DataLoader(train_gen, batch_size=config.batch_size, pin_memory=True, num_workers=6,
                    #                           sampler=sampler)
                    else:
                        train_loader = DataLoader(train_gen, batch_size=config.batch_size, pin_memory=True,
                                                  num_workers=6,shuffle=True)
                    print(train_data_list_base.shape[0] + train_data_list_ext.shape[0])

                    val_gen = HumanDataset(val_data_list_base, config.train_data,
                                           val_data_list_ext, config.train_data_ext,
                                           augument=False, mode="train", there_is_extension=True)
                    val_loader = DataLoader(val_gen, batch_size=config.batch_size*4, shuffle=False, pin_memory=True,
                                            num_workers=6)
                    train_data_list = [train_data_list_base, train_data_list_ext]
                    val_data_list = [val_data_list_base, val_data_list_ext]
                return train_loader,val_loader,train_data_list,val_data_list

            test_gens = HumanDataset(test_files, config.test_data, augument=False, mode="pred", do_TTA=True,
                                     there_is_extension=False)
            test_loader = DataLoader(test_gens,int(config.batch_size/1.5), shuffle=False, pin_memory=True, num_workers=6)

            # include_ext=True
            # train_loader, val_loader, train_data_list, val_data_list=update_loaders()
            # if include_ext==False:
            #     config.model_name="bnwith_aug_wo_ext_withsampler_v2"+model_add_name
            #     train_data_list = all_files_strategy_base[fold]["train"]
            #     val_data_list = all_files_strategy_base[fold]["test"]
            #     print(train_data_list.shape)
            #     labels_l_l = [list(map(int, train_data_list.iloc[i].Target.split(' '))) for i in range(0, train_data_list.shape[0])]
            #     sampler = WeightedRandomSampler([max([probdict[j] for j in i]) for i in labels_l_l],
            #                                     num_samples=labels_l_l.__len__(), replacement=True)
            #
            #     train_gen = HumanDataset(train_data_list,config.train_data,mode="train")
            #     train_loader = DataLoader(train_gen,batch_size=config.batch_size,shuffle=False,
            #                               pin_memory=True,num_workers=6,sampler=sampler)
            #     # x,y=next(iter(train_loader))
            #     # x=np.asarray(x)
            #     # np.max(x)
            #     val_gen = HumanDataset(val_data_list,config.train_data,augument=False,mode="train")
            #     val_loader = DataLoader(val_gen,batch_size=config.batch_size,shuffle=False,
            #                             pin_memory=True,num_workers=6)
            #
            #     test_gen = HumanDataset(test_files,config.test_data,augument=False,mode="test")
            #     test_loader = DataLoader(test_gen,config.batch_size,shuffle=False,pin_memory=True,num_workers=6)
            # if include_ext==True:
            #     all_files_ext = pd.read_csv(config.path_train_csv_ext)
            #     all_files_ext = sklearn.utils.shuffle(all_files_ext, random_state=config.random_state)
            #     all_files_ext.reset_index(drop=True, inplace=True)
            #     all_files_strategy_ext = return_mlsss(config.n_folds, 0.2, all_files_ext, config,
            #                                           shurely_train_bool=None)
            #     config.model_name="bnwith_aug_with_ext_withsampler_v2"+model_add_name
            #     train_data_list_base = all_files_strategy_base[fold]["train"]
            #     val_data_list_base = all_files_strategy_base[fold]["test"]
            #
            #     train_data_list_ext = all_files_strategy_ext[fold]["train"]
            #     val_data_list_ext = all_files_strategy_ext[fold]["test"]
            #     train_data_list_all=pd.concat([train_data_list_base,train_data_list_ext])
            #     labels_l_l = [list(map(int, train_data_list_all.iloc[i].Target.split(' '))) for i in
            #                   range(0, train_data_list_all.shape[0])]
            #     sampler = WeightedRandomSampler([max([probdict[j] for j in i]) for i in labels_l_l],
            #                                     num_samples=labels_l_l.__len__(), replacement=True)
            #
            #     print(train_data_list_base.shape[0]+train_data_list_ext.shape[0])
            #     train_gen = HumanDataset(train_data_list_base,config.train_data,
            #                              train_data_list_ext, config.train_data_ext,augument=True,mode="train",there_is_extension=True)
            #     train_loader = DataLoader(train_gen,batch_size=config.batch_size,pin_memory=True,num_workers=6,sampler=sampler)
            #
            #     val_gen = HumanDataset(val_data_list_base, config.train_data,
            #                             val_data_list_ext,config.train_data_ext,
            #                            augument=False, mode="train",there_is_extension=True)
            #     val_loader = DataLoader(val_gen, batch_size=config.batch_size, shuffle=False, pin_memory=True,
            #                             num_workers=6
            #                             )
            #     train_data_list=[train_data_list_base,train_data_list_ext]
            #     val_data_list=[val_data_list_base,val_data_list_ext]

                #
                # val_gen = HumanDataset(val_data_list_base,config.train_data,
                #                        val_data_list_ext,config.train_data_ext,augument=False,mode="train",there_is_extension=True)
                # val_loader = DataLoader(val_gen,batch_size=config.batch_size,shuffle=False,pin_memory=True,num_workers=6)
            # lr_scheduler
            # lr_scheduler.StepLR
            # scheduler = lr_scheduler.StepLR(optimizer,step_size=3,gamma=0.45)
            scheduler = lr_scheduler.StepLR(optimizer,step_size=1,gamma=0.1)

            # scheduler = lr_scheduler.ReduceLROnPlateau(optimizer,factor=0.5,
            #                                            patience=3,min_lr=0.00000001)
            # from cls import CyclicLR
            # scheduler=CyclicLR(optimizer,max_lr=0.0005,base_lr=0.000025)
            # optimizer.step()
            # for i in range(0,2001):
            #     scheduler.batch_step()
            #     print(scheduler.get_lr())
            # scheduler = lr_scheduler.CosineAnnealingLR(optimizer,T_max=75,eta_min=0.000002)
            # lrs=[]
            # for i in range(0,75):
            #     scheduler.step(i)
            #     optimizer.step()
            #     lr = get_learning_rate(optimizer)
            #     print(lr)
            #     lrs.append(lr)
            # from matplotlib import  pyplot as plt
            # plt.plot(lrs)
            # print("MINIMAL LR = ",scheduler.min_lrs)
            start = timer()
            val_hist=[]
            train_hist=[]
            #train
            config.sampler_type = "pobdict"
            train_loader, val_loader, train_data_list, val_data_list = update_loaders(fold)

            for epoch in range(0,config.epochs+1):
                # epoch=0
                scheduler.step(epoch)
                lr=get_learning_rate(optimizer)
                print( config.img_weight,config.batch_size,include_ext,"Current LR=",lr)
                torch.cuda.empty_cache()
                train_metrics = train(train_loader,model,criterion,optimizer,epoch,val_metrics,best_results,start,scheduler)
                # va
                # if (epoch)%5==0:
                torch.cuda.empty_cache()
                lr=get_learning_rate(optimizer)
                val_metrics = evaluate(val_loader,model,criterion,epoch,train_metrics,best_results,start,lr)
                is_best_loss = val_metrics[0] < best_results[0]
                best_results[0] = min(val_metrics[0],best_results[0])
                is_best_f1 = val_metrics[7] > best_results[1]
                best_results[1] = max(val_metrics[7],best_results[1])
                val_hist.append(val_metrics)
                train_hist.append(train_metrics)
                # save model
                save_checkpoint({
                            "epoch":epoch + 1,
                            "model_name":config.model_name+"best_f1_"+str(best_results[1])+"th_unif_train_"+str(val_metrics[6]),
                            "state_dict":model.state_dict(),
                            "best_loss":best_results[0],
                            "optimizer":optimizer.state_dict(),
                            "fold":fold,
                            "best_f1":best_results[1],
                            "val_hist":val_hist,
                            "best_f1_th_unified_train":val_metrics[6],
                            "best_f1_th_unified_val":val_metrics[8],
                            "best_f1_th_div_train":val_metrics[2],
                            "best_f1_th_div_val":val_metrics[4],
                            "train_hist": train_hist,
                            "train_data_list":train_data_list,
                            "val_data_list":val_data_list
                },is_best_loss,is_best_f1,fold,model_ext_name="best_f1_"+str(round(best_results[1],4))+"th_unif_train_"+str(round(val_metrics[6],4)))
                # print logs
                print('\r',end='',flush=True)
                log.write('%s  %5.1f %6.1f         |         %0.3f  %0.3f           |         %0.3f  %0.4f         |         %s  %s    | %s' % (\
                        "best", epoch, epoch,
                        train_metrics[0], train_metrics[1],
                        val_metrics[0], val_metrics[1],
                        str(best_results[0])[:8],str(best_results[1])[:8],
                        time_to_str((timer() - start),'min'))
                    )
                log.write("\n")
                log.write("\n")
                time.sleep(0.01)
            torch.cuda.empty_cache()
            path_to_save_forecast="/home/minasian/checkpoints/forecasts/"
            # model.eval()
            make_forecast(model,
                          test_loader,
                          limit=None,
                          path_to_save_forecast=path_to_save_forecast+config.model_name+"fold___"+str(fold))
            torch.cuda.empty_cache()

# import torch

if __name__ == "__main__":
    # import torch.multiprocessing as multiprocessing
    # multiprocessing.set_start_method("spawn", force=True)
    main()

# 0.0002
# train  14.0   13.0         |         0.575  0.215           |         0.631  0.2137         |         0.631378  0.235140    |  0 hr 53 min
# Macro F1 best =  0.616  Best th =  0.404
# val    14.0   13.0         |         0.575  0.215           |         0.627  0.2127         |         0.631378  0.235140    |  0 hr 54 min
# Macro F1 best =  0.5808  Best th =  0.4141
# best   13.0   13.0         |         0.575  0.215           |         0.627  0.2127         |         0.627415  0.235140    |  0 hr 54 min




# 2.0000000000000003e-06
#/home/minasian/PycharmProjects/prot_atlas/pac/checkpoints/best_models/bninception_fold_0_model_best_f1.pth_backup.tar
# train  33.0   32.0         |         0.562  0.214           |         0.573  0.2169         |         0.567548  0.216865    |  4 hr 54 min
# Macro F1  best train =  0.631  Best th =  [0.4693877551020408, 0.4081632653061224, 0.4693877551020408, 0.44897959183673464, 0.4693877551020408, 0.42857142857142855, 0.42857142857142855, 0.44897959183673464, 0.4081632653061224, 0.36734693877551017, 0.36734693877551017, 0.4081632653061224, 0.3877551020408163, 0.44897959183673464, 0.4693877551020408, 0.32653061224489793, 0.36734693877551017, 0.36734693877551017, 0.3877551020408163, 0.4081632653061224, 0.3469387755102041, 0.42857142857142855, 0.4081632653061224, 0.4897959183673469, 0.4081632653061224, 0.44897959183673464, 0.4081632653061224, 0.22448979591836732]
# val    33.0   32.0         |         0.562  0.214           |         0.569  0.2155         |         0.567548  0.216865    |  4 hr 55 min
# Macro F1  based on val ths =  0.651 Th on this set =  [0.5102040816326531, 0.4693877551020408, 0.3877551020408163, 0.42857142857142855, 0.4081632653061224, 0.4693877551020408, 0.3877551020408163, 0.44897959183673464, 0.32653061224489793, 0.2857142857142857, 0.2857142857142857, 0.4081632653061224, 0.42857142857142855, 0.44897959183673464, 0.4081632653061224, 0.5306122448979591, 0.36734693877551017, 0.3877551020408163, 0.36734693877551017, 0.4081632653061224, 0.3469387755102041, 0.42857142857142855, 0.3877551020408163, 0.3877551020408163, 0.4081632653061224, 0.44897959183673464, 0.42857142857142855, 0.22448979591836732] 3
# Macro F1  based on train ths =  0.609 Th on this set =  [0.4693877551020408, 0.4081632653061224, 0.4693877551020408, 0.44897959183673464, 0.4693877551020408, 0.42857142857142855, 0.42857142857142855, 0.44897959183673464, 0.4081632653061224, 0.36734693877551017, 0.36734693877551017, 0.4081632653061224, 0.3877551020408163, 0.44897959183673464, 0.4693877551020408, 0.32653061224489793, 0.36734693877551017, 0.36734693877551017, 0.3877551020408163, 0.4081632653061224, 0.3469387755102041, 0.42857142857142855, 0.4081632653061224, 0.4897959183673469, 0.4081632653061224, 0.44897959183673464, 0.4081632653061224, 0.22448979591836732]
# best   32.0   32.0         |         0.562  0.214           |         0.569  0.2155         |         0.567548  0.216865    |  4 hr 55 min

#resnet50
# 0.00020000000000000004
# train  28.0   27.0         |         0.724  0.128           |         0.836  0.1300         |         0.817736  0.133685    |  8 hr 06 min
# Macro F1 div best train =  0.497  Best th =  [0.494949494949495, 0.42424242424242425, 0.4141414141414142, 0.4141414141414142, 0.4040404040404041, 0.4040404040404041, 0.393939393939394, 0.43434343434343436, 0.24242424242424243, 0.22222222222222224, 0.20202020202020204, 0.37373737373737376, 0.393939393939394, 0.37373737373737376, 0.393939393939394, 0.11111111111111112, 0.25252525252525254, 0.29292929292929293, 0.38383838383838387, 0.38383838383838387, 0.2828282828282829, 0.43434343434343436, 0.37373737373737376, 0.42424242424242425, 0.3535353535353536, 0.4545454545454546, 0.3535353535353536, 0.11111111111111112]
# Macro F1 unif best train =  0.441  Best th =  0.37373737373737376
# val    28.0   27.0         |         0.724  0.128           |         0.839  0.1312         |         0.817736  0.133685    |  8 hr 07 min
# Macro F1  div based on val ths =  0.483 Th div on this set =  [0.4646464646464647, 0.6060606060606061, 0.3535353535353536, 0.43434343434343436, 0.3535353535353536, 0.4646464646464647, 0.36363636363636365, 0.4545454545454546, 0.21212121212121213, 0.4141414141414142, 0.24242424242424243, 0.33333333333333337, 0.37373737373737376, 0.36363636363636365, 0.7272727272727273, 0.18181818181818182, 0.2828282828282829, 0.3535353535353536, 0.38383838383838387, 0.37373737373737376, 0.37373737373737376, 0.4040404040404041, 0.32323232323232326, 0.3434343434343435, 0.43434343434343436, 0.43434343434343436, 0.393939393939394, 0.14141414141414144]
# Macro F1  div based on train ths =  0.455 Th div on this set =  [0.494949494949495, 0.42424242424242425, 0.4141414141414142, 0.4141414141414142, 0.4040404040404041, 0.4040404040404041, 0.393939393939394, 0.43434343434343436, 0.24242424242424243, 0.22222222222222224, 0.20202020202020204, 0.37373737373737376, 0.393939393939394, 0.37373737373737376, 0.393939393939394, 0.11111111111111112, 0.25252525252525254, 0.29292929292929293, 0.38383838383838387, 0.38383838383838387, 0.2828282828282829, 0.43434343434343436, 0.37373737373737376, 0.42424242424242425, 0.3535353535353536, 0.4545454545454546, 0.3535353535353536, 0.11111111111111112]
# Macro F1  unif based on val ths =  0.446 Th unif based on this set =  0.37373737373737376
# Macro F1  unif based based on train ths =  0.446 Th unif based on this set =  0.37373737373737376
# best   27.0   27.0         |         0.724  0.128           |         0.839  0.1312         |         0.817736  0.133685    |  8 hr 07 min



########## resnet 50 with hard augmentation
# 2.0000000000000004e-07
# train  41.0   40.0         |         0.688  0.132           |         0.786  0.1346         |         0.768799  0.139163    | 11 hr 55 min
# Macro F1 div best train =  0.529  Best th =  [0.48484848484848486, 0.38383838383838387, 0.4141414141414142, 0.393939393939394, 0.393939393939394, 0.4040404040404041, 0.38383838383838387, 0.42424242424242425, 0.27272727272727276, 0.27272727272727276, 0.23232323232323235, 0.42424242424242425, 0.4141414141414142, 0.393939393939394, 0.4646464646464647, 0.12121212121212122, 0.27272727272727276, 0.31313131313131315, 0.36363636363636365, 0.37373737373737376, 0.30303030303030304, 0.42424242424242425, 0.3535353535353536, 0.4646464646464647, 0.42424242424242425, 0.4646464646464647, 0.32323232323232326, 0.11111111111111112]
# Macro F1 unif best train =  0.469  Best th =  0.3535353535353536
# val    41.0   40.0         |         0.688  0.132           |         0.806  0.1379         |         0.768799  0.139163    | 11 hr 56 min
# Macro F1  div based on val ths =  0.51 Th div on this set =  [0.4646464646464647, 0.5757575757575758, 0.393939393939394, 0.4545454545454546, 0.42424242424242425, 0.4141414141414142, 0.36363636363636365, 0.5151515151515152, 0.24242424242424243, 0.30303030303030304, 0.27272727272727276, 0.4141414141414142, 0.5555555555555556, 0.4747474747474748, 0.6565656565656566, 0.19191919191919193, 0.32323232323232326, 0.3535353535353536, 0.42424242424242425, 0.4141414141414142, 0.3535353535353536, 0.38383838383838387, 0.5656565656565657, 0.393939393939394, 0.6060606060606061, 0.4545454545454546, 0.393939393939394, 0.07070707070707072]
# Macro F1  div based on train ths =  0.48 Th div on this set =  [0.48484848484848486, 0.38383838383838387, 0.4141414141414142, 0.393939393939394, 0.393939393939394, 0.4040404040404041, 0.38383838383838387, 0.42424242424242425, 0.27272727272727276, 0.27272727272727276, 0.23232323232323235, 0.42424242424242425, 0.4141414141414142, 0.393939393939394, 0.4646464646464647, 0.12121212121212122, 0.27272727272727276, 0.31313131313131315, 0.36363636363636365, 0.37373737373737376, 0.30303030303030304, 0.42424242424242425, 0.3535353535353536, 0.4646464646464647, 0.42424242424242425, 0.4646464646464647, 0.32323232323232326, 0.11111111111111112]
# Macro F1  unif based on val ths =  0.474 Th unif based on this set =  0.38383838383838387
# Macro F1  unif based based on train ths =  0.468 Th unif based on this set =  0.3535353535353536
# best   40.0   40.0         |         0.688  0.132           |         0.806  0.1379         |         0.768799  0.139163    | 11 hr 56 min



# 2.0000000000000004e-07
# train  75.0   74.0         |         0.559  0.167           |         0.654  0.1562         |         0.635317  0.162465    | 13 hr 09 min
# Macro F1 div best train =  0.698  Best th =  [0.48484848484848486, 0.43434343434343436, 0.4747474747474748, 0.4646464646464647, 0.48484848484848486, 0.43434343434343436, 0.4141414141414142, 0.4646464646464647, 0.393939393939394, 0.4141414141414142, 0.393939393939394, 0.48484848484848486, 0.4040404040404041, 0.393939393939394, 0.4747474747474748, 0.42424242424242425, 0.42424242424242425, 0.38383838383838387, 0.4444444444444445, 0.4141414141414142, 0.393939393939394, 0.4545454545454546, 0.43434343434343436, 0.4444444444444445, 0.4141414141414142, 0.4646464646464647, 0.4141414141414142, 0.32323232323232326]
# Macro F1 unif best train =  0.682  Best th =  0.4141414141414142
# val    75.0   74.0         |         0.559  0.167           |         0.634  0.1525         |         0.635317  0.162465    | 13 hr 10 min
# Macro F1  div based on val ths =  0.651 Th div on this set =  [0.48484848484848486, 0.48484848484848486, 0.4444444444444445, 0.4646464646464647, 0.43434343434343436, 0.4545454545454546, 0.37373737373737376, 0.5050505050505051, 0.38383838383838387, 0.494949494949495, 0.42424242424242425, 0.42424242424242425, 0.6161616161616162, 0.5252525252525253, 0.4747474747474748, 0.6363636363636365, 0.6767676767676768, 0.6464646464646465, 0.48484848484848486, 0.48484848484848486, 0.43434343434343436, 0.4141414141414142, 0.4444444444444445, 0.42424242424242425, 0.6363636363636365, 0.4646464646464647, 0.5454545454545455, 0.3535353535353536]
# Macro F1  div based on train ths =  0.624 Th div on this set =  [0.48484848484848486, 0.43434343434343436, 0.4747474747474748, 0.4646464646464647, 0.48484848484848486, 0.43434343434343436, 0.4141414141414142, 0.4646464646464647, 0.393939393939394, 0.4141414141414142, 0.393939393939394, 0.48484848484848486, 0.4040404040404041, 0.393939393939394, 0.4747474747474748, 0.42424242424242425, 0.42424242424242425, 0.38383838383838387, 0.4444444444444445, 0.4141414141414142, 0.393939393939394, 0.4545454545454546, 0.43434343434343436, 0.4444444444444445, 0.4141414141414142, 0.4646464646464647, 0.4141414141414142, 0.32323232323232326]
# Macro F1  unif based on val ths =  0.619 Th unif based on this set =  0.42424242424242425
# Macro F1  unif based based on train ths =  0.613 Th unif based on this set =  0.4141414141414142
# best   74.0   74.0         |         0.559  0.167           |         0.634  0.1525         |         0.633616  0.162465    | 13 hr 10 min


# with external data
# Macro F1  div based on val ths =  0.581 Th div on this set =  [0.393939393939394, 0.5151515151515152, 0.36363636363636365, 0.4747474747474748, 0.30303030303030304, 0.42424242424242425, 0.5454545454545455, 0.33333333333333337, 0.9595959595959597, 0.8686868686868687, 0.787878787878788, 0.38383838383838387, 0.4040404040404041, 0.494949494949495, 0.5353535353535354, 0.9696969696969697, 0.5757575757575758, 0.6565656565656566, 0.5454545454545455, 0.4444444444444445, 0.48484848484848486, 0.33333333333333337, 0.32323232323232326, 0.30303030303030304, 0.4646464646464647, 0.38383838383838387, 0.42424242424242425, 0.8383838383838385]
# Macro F1  div based on train ths =  0.447 Th div on this set =  [0.4747474747474748, 0.4444444444444445, 0.4444444444444445, 0.4141414141414142, 0.4646464646464647, 0.4545454545454546, 0.4545454545454546, 0.4444444444444445, 0.4747474747474748, 0.4545454545454546, 0.4545454545454546, 0.4545454545454546, 0.4444444444444445, 0.4444444444444445, 0.4646464646464647, 0.4545454545454546, 0.4141414141414142, 0.4141414141414142, 0.4040404040404041, 0.43434343434343436, 0.4040404040404041, 0.4444444444444445, 0.4444444444444445, 0.4747474747474748, 0.4444444444444445, 0.4646464646464647, 0.4141414141414142, 0.4545454545454546]
# Macro F1  unif based on val ths =  0.453 Th unif based on this set =  0.43434343434343436
# Macro F1  unif based based on train ths =  0.453 Th unif based on this set =  0.4444444444444445
# bnwith_aug_wo_ext_withsampler_fold_0_model_best_loss_best_f1_0.6869th_unif_train_0.4545_.pth


#se resnext 214 224
#2.0000000000000003e-06
# train  50.0   49.0         |         0.070  0.440           |         0.105  0.3468         |         0.104043  0.404240    |  3 hr 30 min
# Macro F1 div best train =  0.595  Best th =  [0.5050505050505051, 0.30303030303030304, 0.38383838383838387, 0.26262626262626265, 0.37373737373737376, 0.29292929292929293, 0.3535353535353536, 0.3434343434343435, 0.07070707070707072, 0.05050505050505051, 0.06060606060606061, 0.3434343434343435, 0.2828282828282829, 0.393939393939394, 0.38383838383838387, 0.020202020202020204, 0.18181818181818182, 0.14141414141414144, 0.26262626262626265, 0.20202020202020204, 0.13131313131313133, 0.393939393939394, 0.25252525252525254, 0.3434343434343435, 0.12121212121212122, 0.4747474747474748, 0.22222222222222224, 0.010101010101010102]
# Macro F1 unif best train =  0.537  Best th =  0.19191919191919193
# val    50.0   49.0         |         0.070  0.440           |         0.105  0.3461         |         0.104043  0.404240    |  3 hr 30 min
# Macro F1  div based on val ths =  0.417 Th div on this set =  [0.4444444444444445, 0.4646464646464647, 0.20202020202020204, 0.30303030303030304, 0.23232323232323235, 0.3434343434343435, 0.16161616161616163, 0.29292929292929293, 0.15151515151515152, 0.04040404040404041, 0.04040404040404041, 0.13131313131313133, 0.17171717171717174, 0.22222222222222224, 0.29292929292929293, 0.04040404040404041, 0.12121212121212122, 0.06060606060606061, 0.19191919191919193, 0.26262626262626265, 0.12121212121212122, 0.2828282828282829, 0.18181818181818182, 0.4444444444444445, 0.15151515151515152, 0.32323232323232326, 0.09090909090909091, 0.020202020202020204]
# Macro F1  div based on train ths =  0.392 Th div on this set =  [0.5050505050505051, 0.30303030303030304, 0.38383838383838387, 0.26262626262626265, 0.37373737373737376, 0.29292929292929293, 0.3535353535353536, 0.3434343434343435, 0.07070707070707072, 0.05050505050505051, 0.06060606060606061, 0.3434343434343435, 0.2828282828282829, 0.393939393939394, 0.38383838383838387, 0.020202020202020204, 0.18181818181818182, 0.14141414141414144, 0.26262626262626265, 0.20202020202020204, 0.13131313131313133, 0.393939393939394, 0.25252525252525254, 0.3434343434343435, 0.12121212121212122, 0.4747474747474748, 0.22222222222222224, 0.010101010101010102]
# Macro F1  unif based on val ths =  0.399 Th unif based on this set =  0.18181818181818182
# Macro F1  unif based based on train ths =  0.398 Th unif based on this set =  0.19191919191919193
# best   49.0   49.0         |         0.070  0.440           |         0.105  0.3461         |         0.104043  0.404240    |  3 hr 30 min
#


#xception 224
#2.0000000000000003e-06
# train  46.0   45.0         |         0.297  0.572           |         0.786  0.2402         |         0.781668  0.477036    |  2 hr 55 min
# Macro F1 div best train =  0.895  Best th =  [0.4747474747474748, 0.4545454545454546, 0.4646464646464647, 0.43434343434343436, 0.43434343434343436, 0.4545454545454546, 0.4545454545454546, 0.4646464646464647, 0.4747474747474748, 0.5050505050505051, 0.4545454545454546, 0.4444444444444445, 0.4747474747474748, 0.42424242424242425, 0.48484848484848486, 0.43434343434343436, 0.43434343434343436, 0.48484848484848486, 0.42424242424242425, 0.42424242424242425, 0.4646464646464647, 0.4747474747474748, 0.4545454545454546, 0.4444444444444445, 0.4646464646464647, 0.48484848484848486, 0.4747474747474748, 0.38383838383838387]
# Macro F1 unif best train =  0.893  Best th =  0.4444444444444445
# val    46.0   45.0         |         0.297  0.572           |         0.787  0.2413         |         0.781668  0.477036    |  2 hr 56 min
# Macro F1  div based on val ths =  0.49 Th div on this set =  [0.4747474747474748, 0.5656565656565657, 0.4141414141414142, 0.5555555555555556, 0.4141414141414142, 0.4545454545454546, 0.5252525252525253, 0.4141414141414142, 0.6666666666666667, 0.5050505050505051, 0.3535353535353536, 0.5555555555555556, 0.5252525252525253, 0.5454545454545455, 0.5454545454545455, 0.5656565656565657, 0.5353535353535354, 0.686868686868687, 0.4545454545454546, 0.5151515151515152, 0.8787878787878789, 0.393939393939394, 0.494949494949495, 0.4444444444444445, 0.5353535353535354, 0.48484848484848486, 0.6666666666666667, 0.787878787878788]
# Macro F1  div based on train ths =  0.471 Th div on this set =  [0.4747474747474748, 0.4545454545454546, 0.4646464646464647, 0.43434343434343436, 0.43434343434343436, 0.4545454545454546, 0.4545454545454546, 0.4646464646464647, 0.4747474747474748, 0.5050505050505051, 0.4545454545454546, 0.4444444444444445, 0.4747474747474748, 0.42424242424242425, 0.48484848484848486, 0.43434343434343436, 0.43434343434343436, 0.48484848484848486, 0.42424242424242425, 0.42424242424242425, 0.4646464646464647, 0.4747474747474748, 0.4545454545454546, 0.4444444444444445, 0.4646464646464647, 0.48484848484848486, 0.4747474747474748, 0.38383838383838387]
# Macro F1  unif based on val ths =  0.474 Th unif based on this set =  0.4545454545454546
# Macro F1  unif based based on train ths =  0.472 Th unif based on this set =  0.4444444444444445
# best   45.0   45.0         |         0.297  0.572           |         0.787  0.2413         |         0.781668  0.477036    |  2 hr 56 min
#


#xception with external ds
# train  50.0   49.0         |         0.173  0.626           |         0.708  0.3205         |         0.585455  0.559386    | 12 hr 21 min
# Macro F1 div best train =  0.947  Best th =  [0.494949494949495, 0.4545454545454546, 0.4545454545454546, 0.43434343434343436, 0.4747474747474748, 0.4444444444444445, 0.4444444444444445, 0.4747474747474748, 0.43434343434343436, 0.494949494949495, 0.48484848484848486, 0.43434343434343436, 0.4444444444444445, 0.4747474747474748, 0.43434343434343436, 0.4747474747474748, 0.4747474747474748, 0.48484848484848486, 0.4444444444444445, 0.43434343434343436, 0.48484848484848486, 0.4545454545454546, 0.4545454545454546, 0.4545454545454546, 0.4646464646464647, 0.4747474747474748, 0.4646464646464647, 0.5353535353535354]
# Macro F1 unif best train =  0.946  Best th =  0.4545454545454546
# val    50.0   49.0         |         0.173  0.626           |         0.700  0.3118         |         0.585455  0.559386    | 12 hr 22 min
# Macro F1  div based on val ths =  0.575 Th div on this set =  [0.494949494949495, 0.6666666666666667, 0.4545454545454546, 0.5858585858585859, 0.5151515151515152, 0.48484848484848486, 0.6060606060606061, 0.4545454545454546, 0.7777777777777778, 0.31313131313131315, 0.2828282828282829, 0.6060606060606061, 0.6161616161616162, 0.5757575757575758, 0.5858585858585859, 0.3535353535353536, 0.5252525252525253, 0.4747474747474748, 0.5151515151515152, 0.48484848484848486, 0.8989898989898991, 0.42424242424242425, 0.5454545454545455, 0.4444444444444445, 0.6464646464646465, 0.4040404040404041, 0.686868686868687, 0.9191919191919192]
# Macro F1  div based on train ths =  0.556 Th div on this set =  [0.494949494949495, 0.4545454545454546, 0.4545454545454546, 0.43434343434343436, 0.4747474747474748, 0.4444444444444445, 0.4444444444444445, 0.4747474747474748, 0.43434343434343436, 0.494949494949495, 0.48484848484848486, 0.43434343434343436, 0.4444444444444445, 0.4747474747474748, 0.43434343434343436, 0.4747474747474748, 0.4747474747474748, 0.48484848484848486, 0.4444444444444445, 0.43434343434343436, 0.48484848484848486, 0.4545454545454546, 0.4545454545454546, 0.4545454545454546, 0.4646464646464647, 0.4747474747474748, 0.4646464646464647, 0.5353535353535354]
# Macro F1  unif based on val ths =  0.561 Th unif based on this set =  0.494949494949495
# Macro F1  unif based based on train ths =  0.557 Th unif based on this set =  0.4545454545454546
# best   49.0   49.0         |         0.173  0.626           |         0.700  0.3118         |         0.585455  0.559386    | 12 hr 22 min


# 2.0000000000000003e-06
# train  41.0   40.0         |         0.098  0.710           |         0.765  0.2842         |         0.744991  0.637419    | 17 hr 16 min
# Macro F1 div best train =  0.974  Best th =  [0.48484848484848486, 0.4545454545454546, 0.4747474747474748, 0.4141414141414142, 0.4545454545454546, 0.4444444444444445, 0.4646464646464647, 0.43434343434343436, 0.5050505050505051, 0.5454545454545455, 0.43434343434343436, 0.4444444444444445, 0.4747474747474748, 0.5151515151515152, 0.4141414141414142, 0.29292929292929293, 0.4747474747474748, 0.4747474747474748, 0.4747474747474748, 0.4646464646464647, 0.5050505050505051, 0.4747474747474748, 0.4646464646464647, 0.4747474747474748, 0.4545454545454546, 0.48484848484848486, 0.494949494949495, 0.5353535353535354]
# Macro F1 unif best train =  0.974  Best th =  0.4747474747474748
# val    41.0   40.0         |         0.098  0.710           |         0.766  0.2842         |         0.744991  0.637419    | 17 hr 24 min
# Macro F1  div based on val ths =  0.663 Th div on this set =  [0.4646464646464647, 0.6565656565656566, 0.5252525252525253, 0.5757575757575758, 0.5454545454545455, 0.5858585858585859, 0.5858585858585859, 0.5353535353535354, 0.4444444444444445, 0.2828282828282829, 0.36363636363636365, 0.6363636363636365, 0.6060606060606061, 0.6767676767676768, 0.5151515151515152, 0.32323232323232326, 0.494949494949495, 0.48484848484848486, 0.5050505050505051, 0.5454545454545455, 0.6666666666666667, 0.5151515151515152, 0.5454545454545455, 0.5252525252525253, 0.6464646464646465, 0.4646464646464647, 0.5757575757575758, 0.3434343434343435]
# Macro F1  div based on train ths =  0.634 Th div on this set =  [0.48484848484848486, 0.4545454545454546, 0.4747474747474748, 0.4141414141414142, 0.4545454545454546, 0.4444444444444445, 0.4646464646464647, 0.43434343434343436, 0.5050505050505051, 0.5454545454545455, 0.43434343434343436, 0.4444444444444445, 0.4747474747474748, 0.5151515151515152, 0.4141414141414142, 0.29292929292929293, 0.4747474747474748, 0.4747474747474748, 0.4747474747474748, 0.4646464646464647, 0.5050505050505051, 0.4747474747474748, 0.4646464646464647, 0.4747474747474748, 0.4545454545454546, 0.48484848484848486, 0.494949494949495, 0.5353535353535354]
# Macro F1  unif based on val ths =  0.634 Th unif based on this set =  0.5151515151515152
# Macro F1  unif based based on train ths =  0.63 Th unif based on this set =  0.4747474747474748
# best   40.0   40.0         |         0.098  0.710           |         0.766  0.2842         |         0.744991  0.637419    | 17 hr 24 min

#degrading accuracy with BCE




# 0.000625 - last was 18 epoch 0.0003125
# train  16.0   15.0         |         0.064  0.797           |         0.781  0.3242         |         0.770267  0.654295    |  8 hr 16 min
# Macro F1 div best train =  0.985  Best th =                   [0.5050505050505051, 0.4747474747474748, 0.4545454545454546, 0.4646464646464647, 0.4747474747474748, 0.4545454545454546, 0.4747474747474748, 0.4646464646464647, 0.48484848484848486, 0.4545454545454546, 0.5252525252525253, 0.48484848484848486, 0.48484848484848486, 0.5050505050505051, 0.494949494949495, 0.27272727272727276, 0.48484848484848486, 0.5050505050505051, 0.4747474747474748, 0.4747474747474748, 0.5050505050505051, 0.4747474747474748, 0.5050505050505051, 0.4545454545454546, 0.4646464646464647, 0.5050505050505051, 0.494949494949495, 0.4545454545454546]
# Macro F1 unif best train =  0.985  Best th =  0.4747474747474748
# val    16.0   15.0         |         0.064  0.797           |         0.770  0.3229         |         0.770267  0.654295    |  8 hr 27 min
# Macro F1  div based on val ths =  0.675 Th div on this set =  [0.5050505050505051, 0.5757575757575758, 0.5252525252525253, 0.5151515151515152, 0.6060606060606061, 0.595959595959596, 0.6262626262626263, 0.5757575757575758, 0.6464646464646465, 0.27272727272727276, 0.4545454545454546, 0.5757575757575758, 0.4545454545454546, 0.5656565656565657, 0.5757575757575758, 0.29292929292929293, 0.393939393939394, 0.393939393939394, 0.5050505050505051, 0.5252525252525253, 0.5555555555555556, 0.494949494949495, 0.5555555555555556, 0.48484848484848486, 0.5151515151515152, 0.4646464646464647, 0.5252525252525253, 0.31313131313131315]
# Macro F1  div based on train ths =  0.655 Th div on this set =  [0.5050505050505051, 0.4747474747474748, 0.4545454545454546, 0.4646464646464647, 0.4747474747474748, 0.4545454545454546, 0.4747474747474748, 0.4646464646464647, 0.48484848484848486, 0.4545454545454546, 0.5252525252525253, 0.48484848484848486, 0.48484848484848486, 0.5050505050505051, 0.494949494949495, 0.27272727272727276, 0.48484848484848486, 0.5050505050505051, 0.4747474747474748, 0.4747474747474748, 0.5050505050505051, 0.4747474747474748, 0.5050505050505051, 0.4545454545454546, 0.4646464646464647, 0.5050505050505051, 0.494949494949495, 0.4545454545454546]
# Macro F1  unif based on val ths =  0.661 Th unif based on this set =  0.4747474747474748
# Macro F1  unif based based on train ths =  0.661 Th unif based on this set =  0.4747474747474748
# best   15.0   15.0         |         0.064  0.797           |         0.770  0.3229         |         0.769730  0.661180    |  8 hr 27 min
