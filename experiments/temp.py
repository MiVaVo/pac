def loss(x):
    return 10**(-6)+0.03/(x)
def loss2(x):
    return 0.05*(0.82)**x
import matplotlib.pyplot as plt
plt.plot([loss2(x) for x in range(0,50)])
print([loss2(x) for x in range(0,50)][-1])



import json

class Object:
    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
            sort_keys=True, indent=4)

me = Object()
me.name = "Onur"
me.age = 35
me.dog = Object()
me.dog.name = ["Apollo","b","C"]
me.a="Ad"
me.dog.c=Object()
me.dog.c.d="awd"
print(me.toJSON())
