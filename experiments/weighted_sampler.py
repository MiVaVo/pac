from tqdm import tqdm
from data_management.data import HumanDataset
from utils import *
from configs.config import *
from torch.utils.data import DataLoader
from torch.utils.data.sampler import WeightedRandomSampler
from configs.weights import probdict


config.img_weight=512
config.img_height=512
n_folds = config.n_folds

###########



# ######## 3. Make forecast (done once)
model,chp=load_model("/home/minasian/PycharmProjects/prot_atlas/pac/checkpoints/best_models/bnwith_aug_wo_ext_fold_0_model_best_loss.pth.tar",return_checkpoint=True)
val_df=chp["val_data_list"]
#######
# weights=problist
probdict
labels_l_l = [list(map(int, val_df.iloc[i].Target.split(' '))) for i in range(0,val_df.shape[0])]
sampler=WeightedRandomSampler([max([probdict[j] for j in i]) for i in labels_l_l],num_samples=labels_l_l.__len__(),replacement=True)

labels = [list(map(int, val_df.iloc[i].Target.split(' '))) for i in range(0,val_df.shape[0])]
val_gen = HumanDataset(val_df, config.train_data, augument=False, mode="train",do_TTA=False)
val_loader = DataLoader(val_gen, batch_size=28, shuffle=False, pin_memory=True, num_workers=6)
next(iter(val_loader))
preds=[]
for key,(x,y) in tqdm(enumerate(val_loader)):
    preds.append(y.numpy())
    if key>100:
        break

np.sum(np.concatenate(preds,axis=0),axis=0).astype(int)

# with sampler
# array([940., 126., 247., 138., 204., 265., 199., 124., 111., 169., 119.,
#        138., 120., 106., 153.,  84., 193., 117., 142., 188., 152., 219.,
#        122., 151., 129., 520., 116., 118.])

# without sampler
# array([1199,  118,  344,  139,  163,  225,   99,  267,    4,    3,    1,
#          90,   62,   43,   85,    2,   48,   21,   83,  133,   19,  335,
#          74,  280,   26,  753,   31,    1])