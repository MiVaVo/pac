import torch

class Custom_Loss(torch.nn.Module):

    def __init__(self):
        super(Custom_Loss,self).__init__()

    def forward(self,output,target):
        eps = torch.tensor(1e-9).cuda()
        output_probs=output.sigmoid().cuda()
        tp=torch.sum(output_probs*target,           dim=0)
        tn=torch.sum((1-output_probs)*(1-target),   dim=0)
        fp = torch.sum((1 - target) * output_probs, dim=0)
        fn = torch.sum(target * (1 - output_probs), dim=0)
        # print(fp.shape)
        p = tp / (tp + fp +eps)
        r = tp / (tp + fn + eps)

        f1 = 2 * p * r / (p + r + eps)
        f1 = torch.where(torch.isnan(f1), torch.zeros_like(f1), f1)
        return 1 - torch.mean(f1)


# def f1(y_true, y_pred):
#     y_pred = K.round(y_pred)
#     tp = K.sum(K.cast(y_true * y_pred, 'float'), axis=0)
#     tn = K.sum(K.cast((1 - y_true) * (1 - y_pred), 'float'), axis=0)
#     fp = K.sum(K.cast((1 - y_true) * y_pred, 'float'), axis=0)
#     fn = K.sum(K.cast(y_true * (1 - y_pred), 'float'), axis=0)
#
#     p = tp / (tp + fp + K.epsilon())
#     r = tp / (tp + fn + K.epsilon())
#
#     f1 = 2 * p * r / (p + r + K.epsilon())
#     f1 = tf.where(tf.is_nan(f1), tf.zeros_like(f1), f1)
#     return K.mean(f1)
#
#
# def f1_loss(y_true, y_pred):
#     tp = K.sum(K.cast(y_true * y_pred, 'float'), axis=0)
#     tn = K.sum(K.cast((1 - y_true) * (1 - y_pred), 'float'), axis=0)
#     fp = K.sum(K.cast((1 - y_true) * y_pred, 'float'), axis=0)
#     fn = K.sum(K.cast(y_true * (1 - y_pred), 'float'), axis=0)
#
#     p = tp / (tp + fp + K.epsilon())
#     r = tp / (tp + fn + K.epsilon())
#
#     f1 = 2 * p * r / (p + r + K.epsilon())
#     f1 = tf.where(tf.is_nan(f1), tf.zeros_like(f1), f1)
#     return 1 - K.mean(f1)