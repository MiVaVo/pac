from src.helpers import make_folders, logger, return_mlsss
from src.libs import *
from utils import *

# 1. set random seed
random.seed(2050)
np.random.seed(2050)
torch.manual_seed(2050)
torch.cuda.manual_seed_all(2050)
if config.cuda_exists:
    os.environ["CUDA_VISIBLE_DEVICES"] = "0"
    torch.backends.cudnn.benchmark = True
warnings.filterwarnings('ignore')

log = logger()


def train(train_loader, model, criterion, optimizer, epoch, valid_loss, best_results, start):
    losses = AverageMeter()
    f1 = AverageMeter()
    model.train()

    for i, (images, target) in enumerate(train_loader):
        # images,target=next(iter(train_loader))
        # print(images.shape)
        # if config.cuda_exists:
        images = images.cuda(non_blocking=True) if config.cuda_exists else images
        # print(images.shape)
        target = torch.from_numpy(np.array(target)).float().cuda(
            non_blocking=True) if config.cuda_exists else torch.from_numpy(np.array(target)).float()
        output = model(images)
        loss = criterion(output, target)
        # print(loss)
        losses.update(loss.item(), images.size(0))
        f1_batch = f1_score(target, output.sigmoid().cpu() > 0.15, average='macro')
        f1.update(f1_batch, images.size(0))
        optimizer.zero_grad()
        torch.cuda.empty_cache()
        loss.backward()
        optimizer.step()
        print('\r', end='', flush=True)
        message = '%s %5.1f %6.1f         |         %0.3f  %0.3f           |         %0.3f  %0.4f         |         %s  %s    | %s' % ( \
            "train", i / len(train_loader) + epoch, epoch,
            losses.avg, f1.avg,
            valid_loss[0], valid_loss[1],
            str(best_results[0])[:8], str(best_results[1])[:8],
            time_to_str((timer() - start), 'min'))
        print(message, end='', flush=True)
    log.write("\n")
    # log.write(message)
    # log.write("\n")
    return [losses.avg, f1.avg]


# 2. evaluate fuunction

def evaluate(val_loader, model, criterion, epoch, train_loss, best_results, start):
    # only meter loss and f1 score
    losses = AverageMeter()
    f1 = AverageMeter()
    # switch mode for evaluation
    # if config.cuda_exists:
    model.cuda() if config.cuda_exists else None
    model.eval()
    with torch.no_grad():
        for i, (images, target) in enumerate(val_loader):
            images_var = images.cuda(non_blocking=True) if config.cuda_exists else images
            target = torch.from_numpy(np.array(target)).float().cuda(
                non_blocking=True) if config.cuda_exists else torch.from_numpy(np.array(target)).float()
            # image_var = Variable(images).cuda()
            # target = Variable(torch.from_numpy(np.array(target)).long()).cuda()
            output = model(images_var)
            loss = criterion(output, target)
            losses.update(loss.item(), images_var.size(0))
            f1_batch = f1_score(target, output.sigmoid().cpu().data.numpy() > 0.15, average='macro')
            f1.update(f1_batch, images_var.size(0))
            print('\r', end='', flush=True)
            message = '%s   %5.1f %6.1f         |         %0.3f  %0.3f           |         %0.3f  %0.4f         |         %s  %s    | %s' % ( \
                "val", i / len(val_loader) + epoch, epoch,
                train_loss[0], train_loss[1],
                losses.avg, f1.avg,
                str(best_results[0])[:8], str(best_results[1])[:8],
                time_to_str((timer() - start), 'min'))

            print(message, end='', flush=True)
        log.write("\n")
        # log.write(message)
        # log.write("\n")

    return [losses.avg, f1.avg]


# 3. test model on public dataset and save the probability matrix

def test(test_loader, model, folds):
    sample_submission_df = pd.read_csv(config.path_sample_submission_csv)
    # 3.1 confirm the model converted to cuda
    filenames, labels, submissions = [], [], []
    if config.cuda_exists:
        model.cuda()
    model.eval()
    submit_results = []
    for i, (input, filepath) in enumerate(tqdm(test_loader)):
        # 3.2 change everything to cuda and get only basename
        filepath = [os.path.basename(x) for x in filepath]
        with torch.no_grad():
            image_var = input.cuda(non_blocking=True) if config.cuda_exists else input
            # image_var = input.cuda(non_blocking=True)
            y_pred = model(image_var)
            label = y_pred.sigmoid().cpu().data.numpy()
            # print(label > 0.5)

            labels.append(label > 0.15)
            filenames.append(filepath)

    for row in np.concatenate(labels):
        subrow = ' '.join(list([str(i) for i in np.nonzero(row)[0]]))
        submissions.append(subrow)
    sample_submission_df['Predicted'] = submissions
    sample_submission_df.to_csv('./submit/%s_bestloss_submission.csv' % config.model_name, index=None)


# 4. main function

def main():
    n_folds = config.n_folds
    all_files_base = pd.read_csv(config.path_train_csv)
    all_files_strategy_base=return_mlsss(config.n_folds,0.3,all_files_base,config)
    # TRAIN: [1     2     4... 31069 31070 31071]
    # TEST: [0     3     5... 31064 31065 31067]

    # TRAIN: [1     2     4... 31069 31070 31071]
    # TEST: [0     3     5... 31064 31065 31067]

    for fold in range(0, n_folds):

        # if fold!=0:
        #     continue

        # model = get_net(train_only_last_layers=True,
        #                 path_to_model="/home/minasian/PycharmProjects/prot_atlas/pac/best_models_artem/bninception_bcelog_fold_" + str(
        #                     fold) + "_model_best_loss.pth.tar")
        # path_to_model="/home/minasian/PycharmProjects/prot_atlas/pac/checkpoints/best_models/bninception_bcelog_fold_"+str(fold)+"_model_best_loss.pth.tar")
        # model = get_net(train_only_last_layers=False,
        #                 path_to_model="/home/minasian/artem_bm_v2/best_models/bninception_bcelog_fold_" + str(fold) + "_model_best_loss.pth.tar")
        # path_to_model="/home/minasian/PycharmProjects/prot_atlas/pac/checkpoints/best_models/bninception_bcelog_fold_"+str(fold)+"_model_best_loss.pth.tar"
        # best_model = torch.load("/home/minasian/artem_bm_v2/best_models/bninception_bcelog_fold_" + str(fold) + "_model_best_loss.pth.tar")
        # # best_model = torch.load("checkpoints/bninception_bcelog/0/checkpoint.pth.tar")

        # model=get_resnet50()
        # model=get_xception()
        # model=get_pnasnet()
        fold = 0
        model = get_se_resnext50_32x4d(dropout_p=0.9)
        # best_model = torch.load("/home/minasian/artem_bm_v2/best_models/bninception_bcelog_fold_" + str(fold) + "_model_best_loss.pth.tar")
        se_resnext50_32x4d_64_64_trained= torch.load("/home/minasian/PycharmProjects/prot_atlas/pac/checkpoints/best_models/resnext50_32x4d_fold_0_model_best_f1.pth_BACKUP_.tar")
        model.load_state_dict(se_resnext50_32x4d_64_64_trained["state_dict"])

        # import matplotlib.pyplot as plt
        # plt.plot(se_resnext50_32x4d_64_64_trained["val_losses"])
        model.cuda() if config.cuda_exists else None
        # model.cuda()
        # criterion
        # optimizer = optim.SGD(model.parameters(), lr=config.lr, momentum=0.9, weight_decay=1e-4)
        optimizer = optim.Adamax(model.parameters(), weight_decay=1e-06,lr=2e-3)
        optimizer.load_state_dict(se_resnext50_32x4d_64_64_trained["optimizer"])
        epoch_prev=se_resnext50_32x4d_64_64_trained["epoch"]
        # for i in model.parameters():
        #     print(i.shape)
        # optimizer
        # criterion=Custom_Loss().cuda()
        criterion = nn.BCEWithLogitsLoss().cuda() if config.cuda_exists else nn.BCEWithLogitsLoss()
        # criterion = FocalLoss().cuda()
        # criterion = F1Loss().cuda()
        start_epoch = 0
        best_loss = 999
        best_f1 = 0
        best_results = [np.inf, 0]
        val_metrics = [np.inf, 0]
        resume = False
        # fold=0
        print("Current fold " + str(fold))
        make_folders(fold)
        train_data_list =all_files_strategy_base[fold]["train"]
        val_data_list =all_files_strategy_base[fold]["test"]
        # TODO: set seed for validation
        # n_folds = 5
        #
        # load dataset
        print(train_data_list.shape)
        train_gen = HumanDataset(train_data_list, config.train_data, mode="train")
        train_loader = DataLoader(train_gen, batch_size=config.batch_size, shuffle=True, pin_memory=True, num_workers=4)
        val_gen = HumanDataset(val_data_list, config.train_data, augument=False, mode="train")
        val_loader = DataLoader(val_gen, batch_size=max(1,config.batch_size-3), shuffle=False, pin_memory=True, num_workers=4)
        # lr_scheduler
        # lr_scheduler.StepLR
        scheduler = lr_scheduler.StepLR(optimizer, step_size=35, gamma=0.1)

        # scheduler = lr_scheduler.CosineAnnealingLR(optimizer,T_max=10)
        # lr=[]
        # for i in range(0, 50):
        #     scheduler.step(i)
        #     print(scheduler.get_lr())
        #     lr.append(scheduler.get_lr())
        # plt.plot(lr)
        # lrs=[]
        # for i in range(0,1000):
        #     scheduler.step(i)
        #     optimizer.step()
        #     lr = get_learning_rate(optimizer)
        #     print(lr)
        #     lrs.append(lr)
        # plt.plot()
        start = timer()
        val_losses = []
        f1_scores=[]
        # train
        try:
            print(epoch_prev)
        except:
            epoch_prev=0
        # epoch_prev-=1
        for epoch in range(epoch_prev, config.epochs+1):
            if epoch%10==0 and epoch>0 and config.img_weight!=512:
                if epoch//10==0  :
                    config.img_weight=config.img_height=64
                    config.batch_size=300
                elif epoch//10==1:
                    config.img_weight = config.img_height = 64*2
                    config.batch_size = int(300/2.5)
                elif  epoch//10>2 :
                    config.img_weight = config.img_height = 64*2*2
                    config.batch_size = int(300/(2.5*8))
                # elif ep :
                #     config.img_weight = config.img_height = 64*2*2*2
                #     config.batch_size = int(300/(2.5*4*8))
            # elif  epoch//10==4 and epoch%10==0 and epoch>0 and config.img_weight!=512:
            #     config.img_weight = config.img_height = 64*2*2
            #     config.batch_size = int(300/(2.5*2.5))
            # if epoch%10==0 and epoch>0 and config.img_weight!=512:
            #     config.img_weight=config.img_weight*2
            #     config.img_height=config.img_height*2
            #     config.batch_size=int(config.batch_size/2.5)
                print( config.batch_size,config.img_weight)
                del train_gen, train_loader, val_gen, val_loader
                torch.cuda.empty_cache()
                train_gen = HumanDataset(train_data_list, config.train_data, mode="train")
                train_loader = DataLoader(train_gen, batch_size=config.batch_size, shuffle=True, pin_memory=True,
                                          num_workers=4)
                val_gen = HumanDataset(val_data_list, config.train_data, augument=False, mode="train")
                val_loader = DataLoader(val_gen, batch_size=max(1,config.batch_size-3), shuffle=False, pin_memory=True,
                                          num_workers=4)
            # epoch=0
            torch.cuda.empty_cache()
            scheduler.step(epoch)
            # train
            lr = get_learning_rate(scheduler.optimizer)
            print(lr)
            train_metrics = train(train_loader, model, criterion, optimizer, epoch, val_metrics, best_results, start)
            # val
            val_metrics = evaluate(val_loader, model, criterion, epoch, train_metrics, best_results, start)
            # check results
            is_best_loss = val_metrics[0] < best_results[0]
            best_results[0] = min(val_metrics[0], best_results[0])
            is_best_f1 = val_metrics[1] > best_results[1]
            best_results[1] = max(val_metrics[1], best_results[1])
            val_losses.append(val_metrics[0])
            f1_scores.append(val_metrics[1])
            # save model
            save_checkpoint({
                "epoch": epoch + 1,
                "model_name": config.model_name,
                "state_dict": model.state_dict(),
                "best_loss": best_results[0],
                "optimizer": optimizer.state_dict(),
                "fold": fold,
                "best_f1": best_results[1],
                "val_losses": val_losses,
                "train_data_list": train_data_list,
                "val_data_list": val_data_list
            }, is_best_loss, is_best_f1, fold)
            # print logs
            print('\r', end='', flush=True)
            log.write(
                '%s  %5.1f %6.1f         |         %0.3f  %0.3f           |         %0.3f  %0.4f         |         %s  %s    | %s' % ( \
                    "best", epoch, epoch,
                    train_metrics[0], train_metrics[1],
                    val_metrics[0], val_metrics[1],
                    str(best_results[0])[:8], str(best_results[1])[:8],
                    time_to_str((timer() - start), 'min'))
                )
            log.write("\n")
            log.write("\n")
            time.sleep(0.01)

        # best_model = torch.load("%s/%s_fold_%s_model_best_FINETUNED_LAST_LAYER_ARTEM_loss.pth.tar"%(config.best_models,config.model_name,str(fold)))
        # best_model = torch.load("checkpoints/bninception_bcelog/0/checkpoint.pth.tar")
        # model.load_state_dict(best_model["state_dict"])
        # test(test_loader,model,fold)
        # del modelё
        del model, train_gen, train_loader, val_gen
        # del test_gen
        # del val_gen
        if config.cuda_exists:
            torch.cuda.empty_cache()


if __name__ == "__main__":
    main()