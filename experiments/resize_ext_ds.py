import  pandas as pd
import cv2
from configs.config import  config
all_files_base = pd.read_csv(config.path_train_csv_ext)
all_files_ext = pd.read_csv("/home/minasian/PycharmProjects/prot_atlas/pac/input_ext/HPAv18/HPAv18RGBY_WithoutUncertain_wodpl.csv")
for i in all_files_ext.iloc[:,0]:
    for color in ["_red","_green","_blue","_yellow"]:
        img=cv2.imread(config.path_to_input_ext+"train/"+i+color+".jpg",0)
        if img.shape[0]!=config.img_height or img.shape[1]!=config.img_weight:
            img=cv2.resize(img,(512,512))
            cv2.imwrite()
