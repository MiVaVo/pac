from models.model import get_net
from utils import *
import random

# set random seed
random.seed(2050)
np.random.seed(2050)
torch.manual_seed(2050)
torch.cuda.manual_seed_all(2050)

model = get_net()
current_model_path="/home/minasian/PycharmProjects/prot_atlas/pac/best_models_artem/bninception_bcelog_fold_0_model_best_loss.pth.tar"
best_model = torch.load(current_model_path)
best_model["best_loss"]
model.load_state_dict(best_model["state_dict"])
# sample_submission_df = pd.read_csv(config.path_sample_submission_csv)
#3.1 confirm the model converted to cuda
model.cuda()
model.eval()


df ={"a":[1,2,3,4],"b":[2,3,4,5]}
df["a"].append(0)
