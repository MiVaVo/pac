from tqdm import tqdm
from data_management.data import HumanDataset
from models.model import get_net
from src.helpers import list_all_contained_files_in_dir
from utils import *
from configs.config import *
from torch.utils.data import DataLoader
import random
import pandas as pd
# set random seed
random.seed(2050)
np.random.seed(2050)
torch.manual_seed(2050)
torch.cuda.manual_seed_all(2050)


######### 0. Prepare dt with files
n_folds = config.n_folds
all_files = pd.read_csv(config.path_train_csv)
test_files = pd.read_csv(config.path_test_csv)
# all_files_shuffled = sklearn.utils.shuffle(all_files, random_state=config.random_state)
# # print(all_files_shuffled.shape[0])
# all_files_shuffled = all_files_shuffled.iloc[:int(all_files_shuffled.shape[0] * config.ratio_of_ds_to_use), :]
# all_files_shuffled = all_files_shuffled.reset_index(drop=True)
# all_files_strategy = prepare_validation_strategy(all_files_shuffled, n_folds=n_folds)
# fold_0_test_files=all_files_strategy[0]["test"]
# test_files=fold_0_test_files
# print(all_files_strategy[0]["train"])
######### 1. Prepare dataloader
# test_files = pd.read_csv(config.path_test_csv)
# test_gens=HumanDataset(test_files,config.test_data,augument=False,mode="test",do_TTA=True)
test_gens=HumanDataset(test_files,config.test_data,augument=False,mode="pred",do_TTA=True)
test_loader=DataLoader(test_gens,1,shuffle=False,pin_memory=True,num_workers=4)
input,filepath=next(iter(test_loader))
#TODO: 1) Make batch Dataloader for TTA 2) Make combiner across models 3) Show confusion matrix to identify weak places 4) Do cyclic learning rate

######## 2. Load model
model_pathes=list_all_contained_files_in_dir("/home/minasian/PycharmProjects/prot_atlas/pac/best_models_artem",pattern="*_loss.pth.tar")
# list_of_filled_models = [{"model_path": current_model_path,
#                           "mio": []}]
list_of_filled_models=[]
n=0
model = get_net()

for current_model_path in model_pathes:

    # current_model_path="/home/minasian/PycharmProjects/prot_atlas/pac/checkpoints/best_models/bninception_bcelog_fold_0_model_best_loss.pth.tar"
    # current_model_path="/home/minasian/PycharmProjects/prot_atlas/pac/checkpoints/best_models/bninception_bcelog_fold_2_model_best_f1.pth.tar"
    best_model = torch.load(current_model_path)
    # plt.plot(best_model["val_losses"])
    model.load_state_dict(best_model["state_dict"])
    sample_submission_df = pd.read_csv(config.path_sample_submission_csv)
    #3.1 confirm the model converted to cuda
    model.cuda()
    model.eval()
    ######## 3. Prepare submission dics

    # submit_results = []
    # # tl=[]
    # labels_tta=[]
    #
    # # for test_loader in test_loaders:
    # labels_array = []
    # # test_loader=test_loaders[0]
    # filenames, labels, submissions = [], [], []
    # # input, filepath=next(iter(test_loader))
    # all_labels=[]
    mode="pred"
    list_of_current_model=[]
    for i,(input,filepath) in enumerate(tqdm(test_loader)):
        # (input, filepath)=next(iter(test_loader))
        if mode!="pred":
            labels_true=np.asarray(filepath[1])
            filepath = [os.path.basename(x) for x in filepath[0]]

        else:
            labels_true=None
            filepath = [os.path.basename(x) for x in filepath[0]]

        with torch.no_grad():
            input=input[0,:,:,:,:]
            image_var = input.cuda(non_blocking=True)
            y_pred = model(image_var)

            label = y_pred.sigmoid().cpu().data.numpy()
            # list_of_filled_models.append({"mio":})
            list_of_current_model.append({"id":filepath,
                                                    "labels_true":labels_true,
                                                    "probs_pred":label,
                                                    "labels_pred": np.argwhere(np.mean(label,axis=0)>0.1836)[:,0]})
            # label=np.mean(label,axis=0)
            #print(label > 0.5)
            # labels.append(label > 0.15)
            # filenames.append(filepath)
    list_of_filled_models.append({"model_path":current_model_path,"mio":list_of_current_model})
    # del model
    # torch.cuda.empty_cache()

# import pickle
# with open('list_of_filled_models_3_folds_TTA.pkl', 'wb') as f:
#     pickle.dump(list_of_filled_models, f)

preds=[[j["probs_pred"] for j in i["mio"] ] for i in list_of_filled_models]
preds_array=np.asarray(preds)
submissions=[]
for i in list_of_filled_models[0]['mio']:
    subrow=' '.join(list([str(m) for m in i["labels_pred"]]))
    submissions.append(subrow)
sample_submission_df['Predicted'] = submissions
sample_submission_df.to_csv("v2_artem_finetuned_best_loss.csv", index=None)
import pickle
with open('list_of_filled_models_TTA_artem_for_val_finetuned_best_loss.pkl', 'wb') as f:
    pickle.dump(list_of_filled_models, f)

# res3=np.mean(np.asarray(all_labels),axis=1)
# labels_array=np.asarray(labels_array)
# labels_tta.append(labels_array)
#
# np.asarray(labels_tta)
# np.mean(labels_tta,axis=0).shape
# # np.std(labels_tta,axis=0)
#     # for row in np.concatenate(labels):
#     #     subrow = ' '.join(list([str(i) for i in np.nonzero(row)[0]]))
#     #     submissions.append(subrow)
#     # sample_submission_df['Predicted'] = submissions