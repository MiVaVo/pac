from platypus import NSGAII, Problem, Real, Subset, GeneticAlgorithm
from sklearn.metrics import f1_score
import pandas as pd
import  numpy as np
df=pd.read_csv("2.csv")
list(df["y_prob"][0])
from six.moves import cPickle as pickle
with open('d2.pkl', 'rb') as f:
    dict_of_df=pickle.load(f)
y_prob=np.mean(np.asarray([dict_of_df["y_prob"][i] for i in range(0,len(dict_of_df["y_prob"].keys()))]),axis=1).tolist()
y_true=np.asarray([dict_of_df["y_true"][i] for i in range(0,len(dict_of_df["y_true"].keys()))]).tolist()


def schaffer(x):
    # print(x)
    # x=np.zeros((1,28))
    y_pred=np.asarray(np.asarray(y_prob)>np.asarray(x)).astype(int)

    return  [-f1_score(np.asarray(y_true),y_pred,average="macro")]

# for i in np.linspace(0,1,100):
#     schaffer(i)

problem = Problem(28, 1)
# problem.types[:] = [Real(0.2, 0.95) for i in range(0,28)]
problem.types[:] = [Real(0.2, 0.8,np.linspace(0.2,0.8,50)) for i in range(0,28)]

problem.function = schaffer
algorithm = GeneticAlgorithm(problem,population_size = 100,offspring_size = 100)
algorithm.run(10000)
feasible_solutions = [s for s in algorithm.result if s.feasible]
feasible_solutions[0] #0.5984
print(feasible_solutions[1].objectives)
print(np.asarray(feasible_solutions[1].variables))
np.mean(np.asarray(feasible_solutions[0].variables))
#best feasible v2 0.601922
#feasible sollution v2
#[0.4382311  ,0.62520605 ,0.44488127 ,0.509292    ,0.54009121 ,0.55209738,
 # 0.57583665, 0.54082948, 0.43871021, 0.71871447 ,0.54967345 ,0.66417584,
 # 0.71306452, 0.54112389, 0.45129643, 0.24327102 ,0.75883577 ,0.59498116,
 # 0.4603257 , 0.53472784, 0.5707798 , 0.45252976 ,0.56296725 ,0.44735134,
 # 0.66282021, 0.41252019, 0.63576908, 0.55223337]
#best feasible 0.5981
#feasible best solutions
th=np.asarray([0.41192841, 0.64568972 ,0.51969631 ,0.52276098,0.54071982 ,0.58891231,
 0.4676641 , 0.5249943  ,0.38410019 ,0.42105632,0.39814366 ,0.67520664,
 0.66314035, 0.68408334 ,0.4132844  ,0.25668143,0.69272623 ,0.44432523,
 0.51443982, 0.45566476 ,0.76172566 ,0.45106934,0.55946105 ,0.49345417,
 0.60773627, 0.37195147 ,0.63078709 ,0.68985976])

f1_score(np.asarray(y_true),np.asarray(y_prob)>np.asarray(feasible_solutions[0].variables),average="macro")
#best f1 sollution
# array([0.45454545, 0.63636364, 0.52525253, 0.53535354, 0.56565657,
# 0.58585859, 0.46464646, 0.53535354, 0.37373737, 0.22222222,
# 0.21212121, 0.66666667, 0.57575758, 0.66666667, 0.41414141,
# 0.28282828, 0.67676768, 0.44444444, 0.51515152, 0.51515152,
# 0.92929293, 0.45454545, 0.54545455, 0.49494949, 0.60606061,
# 0.4040404 , 0.62626263, 0.92929293])
#best f1 sollution v2
# 0.60176
################
# [0.43434343 0.63636364 0.44444444 0.51515152 0.54545455 0.55555556
#  0.57575758 0.44444444 0.43434343 0.22222222 0.2020202  0.63636364
#  0.52525253 0.54545455 0.46464646 0.24242424 0.74747475 0.58585859
#  0.45454545 0.53535354 0.83838384 0.44444444 0.57575758 0.50505051
#  0.65656566 0.44444444 0.63636364 0.92929293]
def find_best_f1_and_ths_diversed(output,target,th_each_class=None):
    if th_each_class is None :
        f1_each_class=[]
        th_each_class=[]
        for each_class in range(0,28):
            # each_class=0
            f1_best=0
            th_best=0
            for th in np.linspace(0, 1, 100):
                # print(th)
                # th=0
                f1_current=f1_score(target[:,each_class], np.asarray(output[:,each_class]>th).astype(int),average="macro")
                # print(th,f1_current)

                if f1_current>f1_best:
                    f1_best=f1_current
                    th_best=th
            f1_each_class.append(f1_best)
            th_each_class.append(th_best)
    final_f1=f1_score(target, np.asarray(output>np.asarray(th_each_class)).astype(int),average="macro")
    return th_each_class,final_f1

th_each_class,final_f1=find_best_f1_and_ths_diversed(output=np.asarray(y_prob),target=np.asarray(y_true))
print(np.asarray(th_each_class))
print(final_f1)
np.mean(np.asarray(th_each_class))
#0.5979 - best th based on f1 finder
# 0.57432
#0.598513- best based on gen alg
# [feasible_solutions[i].objectives[0] for i in range(0,feasible_solutions.__len__())]
# feasible_solutions[4].variables
averaged_ths=[i.variables for i in feasible_solutions]
np.asarray(averaged_ths)

def fine_best_ths_based_on_ga(y_true,y_prob):
    def f1_loss_function(x):
        # print(x)
        y_pred = np.asarray(np.asarray(y_prob) > np.asarray(x)).astype(int)

        return [-f1_score(np.asarray(y_true), y_pred, average="macro")]

    problem = Problem(28, 1)
    problem.types[:] = [Real(0.01, 0.99) for i in range(0, 28)]
    # problem.types[:] = [Subset(np.linspace(0,1,2), 28) for i in range(0,28)]

    problem.function = f1_loss_function

    algorithm = NSGAII(problem)
    algorithm.run(10000)