from src.helpers import make_folders, logger, return_mlsss
from src.libs import *
from utils import *
import pandas as pd

# 1. set random seed
random.seed(2050)
np.random.seed(2050)
torch.manual_seed(2050)
torch.cuda.manual_seed_all(2050)
if config.cuda_exists:
    os.environ["CUDA_VISIBLE_DEVICES"] = "0"
    torch.backends.cudnn.benchmark = True
warnings.filterwarnings('ignore')

log=logger()




def train(train_loader, model, criterion, optimizer, epoch, valid_loss, best_results, start):
    losses = AverageMeter()
    f1 = AverageMeter()
    model.train()

    for i, (images, target) in enumerate(train_loader):
        # images,target=next(iter(train_loader))
        # print(images.shape)
        if config.cuda_exists:
            images = images.cuda(non_blocking=True)
        # print(images.shape)
        if config.cuda_exists:
            target = torch.from_numpy(np.array(target)).float().cuda(non_blocking=True)
        else:
            target = torch.from_numpy(np.array(target)).float()

        # compute output
        output = model(images)
        loss = criterion(output, target)
        print(loss)
        losses.update(loss.item(), images.size(0))

        f1_batch = f1_score(target, output.sigmoid().cpu() > 0.15, average='macro')
        f1.update(f1_batch, images.size(0))
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        print('\r', end='', flush=True)
        message = '%s %5.1f %6.1f         |         %0.3f  %0.3f           |         %0.3f  %0.4f         |         %s  %s    | %s' % ( \
            "train", i / len(train_loader) + epoch, epoch,
            losses.avg, f1.avg,
            valid_loss[0], valid_loss[1],
            str(best_results[0])[:8], str(best_results[1])[:8],
            time_to_str((timer() - start), 'min'))
        print(message, end='', flush=True)
    log.write("\n")
    # log.write(message)
    # log.write("\n")
    return [losses.avg, f1.avg]
# 2. evaluate fuunction

def evaluate(val_loader,model,criterion,epoch,train_loss,best_results,start):
    # only meter loss and f1 score
    losses = AverageMeter()
    f1 = AverageMeter()
    # switch mode for evaluation
    if config.cuda_exists:
        model.cuda()
    model.eval()
    with torch.no_grad():
        for i, (images,target) in enumerate(val_loader):
            if config.cuda_exists:
                images_var = images.cuda(non_blocking=True)
            else:
                images_var=images
            if config.cuda_exists:
                target = torch.from_numpy(np.array(target)).float().cuda(non_blocking=True)
            else:
                target = torch.from_numpy(np.array(target)).float()
            #image_var = Variable(images).cuda()
            #target = Variable(torch.from_numpy(np.array(target)).long()).cuda()
            output = model(images_var)
            loss = criterion(output,target)
            losses.update(loss.item(),images_var.size(0))
            f1_batch = f1_score(target,output.sigmoid().cpu().data.numpy() > 0.15,average='macro')
            f1.update(f1_batch,images_var.size(0))
            print('\r',end='',flush=True)
            message = '%s   %5.1f %6.1f         |         %0.3f  %0.3f           |         %0.3f  %0.4f         |         %s  %s    | %s' % (\
                    "val", i/len(val_loader) + epoch, epoch,                    
                    train_loss[0], train_loss[1], 
                    losses.avg, f1.avg,
                    str(best_results[0])[:8],str(best_results[1])[:8],
                    time_to_str((timer() - start),'min'))

            print(message, end='',flush=True)
        log.write("\n")
        #log.write(message)
        #log.write("\n")
        
    return [losses.avg,f1.avg]
# 3. test model on public dataset and save the probability matrix

def test(test_loader,model,folds):
    sample_submission_df = pd.read_csv(config.path_sample_submission_csv)
    #3.1 confirm the model converted to cuda
    filenames,labels ,submissions= [],[],[]
    if config.cuda_exists:
        model.cuda()
    model.eval()
    submit_results = []
    for i,(input,filepath) in enumerate(tqdm(test_loader)):
        #3.2 change everything to cuda and get only basename
        filepath = [os.path.basename(x) for x in filepath]
        with torch.no_grad():
            if config.cuda_exists:
                image_var = input.cuda(non_blocking=True)
            else:
                image_var=input
            # image_var = input.cuda(non_blocking=True)
            y_pred = model(image_var)
            label = y_pred.sigmoid().cpu().data.numpy()
            #print(label > 0.5)

            labels.append(label > 0.15)
            filenames.append(filepath)

    for row in np.concatenate(labels):
        subrow = ' '.join(list([str(i) for i in np.nonzero(row)[0]]))
        submissions.append(subrow)
    sample_submission_df['Predicted'] = submissions
    sample_submission_df.to_csv('./submit/%s_bestloss_submission.csv'%config.model_name, index=None)
# 4. main function

def main():
    n_folds=config.n_folds
    #
    # 4.2 get model

    all_files_base = pd.read_csv(config.path_train_csv)
    all_files_strategy_base=return_mlsss(config.n_folds,0.2,all_files_base,config)
    # mlb.fit(0)
    # mlb.fit_transform([[1,2],[1]])
    # mlb.fit_transform(1)
    test_files = pd.read_csv(config.path_test_csv)
    # all_files_shuffled = sklearn.utils.shuffle(all_files_base,random_state=config.random_state)
    # # print(all_files_shuffled.shape[0])
    # all_files_shuffled=all_files_shuffled.iloc[:int(all_files_shuffled.shape[0]*config.ratio_of_ds_to_use),:]
    # # all_files_shuffled=all_files_shuffled.reset_index(drop=True)
    # all_files_strategy=prepare_validation_strategy(all_files_shuffled,n_folds=n_folds)

    # import numpy as np

    # X = np.array([[1, 2], [3, 4], [1, 2], [3, 4], [1, 2], [3, 4], [1, 2], [3, 4], [3, 4]])
    # y = np.array([[0, 0, 0], [0, 0, 0], [0, 1, 0], [0, 1, 0], [1, 1, 0], [1, 1, 0], [1, 0, 0], [1, 0, 0], [0, 0, 1]])


    # print(all_files_strategy[0]["train"])
    for fold in range(0,n_folds):
        if fold!=0:
            continue
        # fold=0
        # model = get_net(train_only_last_layers=True,
        #                 path_to_model="/home/minasian/PycharmProjects/prot_atlas/pac/best_models_artem/bninception_bcelog_fold_" + str(
        #                     fold) + "_model_best_loss.pth.tar")
        # path_to_model="/home/minasian/PycharmProjects/prot_atlas/pac/checkpoints/best_models/bninception_bcelog_fold_"+str(fold)+"_model_best_loss.pth.tar")
        model = get_net(train_only_last_layers=False,
                        path_to_model=None)
        # path_to_model="/home/minasian/PycharmProjects/prot_atlas/pac/checkpoints/best_models/bninception_bcelog_fold_"+str(fold)+"_model_best_loss.pth.tar")
        # best_model = torch.load("/home/minasian/PycharmProjects/prot_atlas/pac/best_models_artem/bninception_bcelog_fold_" + str(fold) + "_model_best_loss.pth.tar")
        best_model=torch.load("/home/minasian/artem_bm_v2/best_models/bninception_bcelog_fold_" + str(fold) + "_model_best_loss.pth.tar")
        # best_model = torch.load("checkpoints/bninception_bcelog/0/checkpoint.pth.tar")
        model.load_state_dict(best_model["state_dict"])
        # model = get_net(train_only_last_layers=True,path_to_model="som_path")

        # model=get_resnet50()
        # model=get_xception()
        # model=get_pnasnet()
        # model=get_seresnext()
        if config.cuda_exists:
            model.cuda()
        # model.cuda()
        # criterion
        optimizer = optim.SGD(model.parameters(), lr=config.lr, momentum=0.9, weight_decay=1e-4)
        # optimizer = optim.Adamax(model.parameters(),weight_decay=1e-06,lr=0.1)

        # for i in model.parameters():
        #     print(i.shape)
        # optimizer
        # criterion=Custom_Loss().cuda()
        if config.cuda_exists:
            criterion = nn.BCEWithLogitsLoss().cuda()
        else:
            criterion = nn.BCEWithLogitsLoss()

        # criterion = FocalLoss().cuda()
        # criterion = F1Loss().cuda()
        start_epoch = 0
        best_loss = 999
        best_f1 = 0
        best_results = [np.inf, 0]
        val_metrics = [np.inf, 0]
        resume = False
        # fold=0
        print("Current fold "+str(fold))
        make_folders(fold)
        train_data_list_base=all_files_strategy_base[fold]["train"]
        val_data_list_base = all_files_strategy_base[fold]["test"]

        train_data_list_ext = all_files_strategy_ext[fold]["train"]
        val_data_list_ext = all_files_strategy_ext[fold]["test"]
        #TODO: set seed for validation
        # n_folds = 5
        #
        # load dataset
        print(train_data_list_base.shape[0]+train_data_list_ext.shape[0])
        train_gen = HumanDataset(train_data_list_base,config.train_data,
                                 train_data_list_ext, config.train_data_ext,augument=True,mode="train",there_is_extension=True)
        train_loader = DataLoader(train_gen,batch_size=config.batch_size,shuffle=True,pin_memory=True,num_workers=6)

        val_gen = HumanDataset(val_data_list_base,config.train_data,
                               val_data_list_ext,config.train_data_ext,augument=False,mode="train",there_is_extension=True)
        val_loader = DataLoader(val_gen,batch_size=config.batch_size,shuffle=False,pin_memory=True,num_workers=6)

        test_gen = HumanDataset(test_files,config.test_data,augument=False,mode="test",there_is_extension=False)
        test_loader = DataLoader(test_gen,1,shuffle=False,pin_memory=True,num_workers=4)
        # lr_scheduler
        # lr_scheduler.StepLR
        scheduler = lr_scheduler.StepLR(optimizer,step_size=1,gamma=0.87)
        # scheduler = lr_scheduler.CosineAnnealingLR(optimizer,T_max=5000)
        # lrs=[]
        # for i in range(0,1000):
        #     scheduler.step(i)
        #     optimizer.step()
        #     lr = get_learning_rate(optimizer)
        #     print(lr)
        #     lrs.append(lr)
        # plt.plot()
        start = timer()
        val_losses=[]
        #train
        train_data_list=pd.concat([train_data_list_base,train_data_list_ext])
        val_data_list=pd.concat([val_data_list_base,val_data_list_ext])

        for epoch in range(0,config.epochs):
            # epoch=0
            scheduler.step(epoch)
            # train
            lr = get_learning_rate(optimizer)
            print(lr)
            train_metrics = train(train_loader,model,criterion,optimizer,epoch,val_metrics,best_results,start)
            # val
            val_metrics = evaluate(val_loader,model,criterion,epoch,train_metrics,best_results,start)
            # check results
            is_best_loss = val_metrics[0] < best_results[0]
            best_results[0] = min(val_metrics[0],best_results[0])
            is_best_f1 = val_metrics[1] > best_results[1]
            best_results[1] = max(val_metrics[1],best_results[1])
            val_losses.append(val_metrics[0])
            # save model
            save_checkpoint({
                        "epoch":epoch + 1,
                        "model_name":config.model_name,
                        "state_dict":model.state_dict(),
                        "best_loss":best_results[0],
                        "optimizer":optimizer.state_dict(),
                        "fold":fold,
                        "best_f1":best_results[1],
                        "val_losses":val_losses,
                        "train_data_list":train_data_list,
                        "val_data_list":val_data_list
            },is_best_loss,is_best_f1,fold)
            # print logs
            print('\r',end='',flush=True)
            log.write('%s  %5.1f %6.1f         |         %0.3f  %0.3f           |         %0.3f  %0.4f         |         %s  %s    | %s' % (\
                    "best", epoch, epoch,
                    train_metrics[0], train_metrics[1],
                    val_metrics[0], val_metrics[1],
                    str(best_results[0])[:8],str(best_results[1])[:8],
                    time_to_str((timer() - start),'min'))
                )
            log.write("\n")
            log.write("\n")
            time.sleep(0.01)

        # best_model = torch.load("%s/%s_fold_%s_model_best_FINETUNED_LAST_LAYER_ARTEM_loss.pth.tar"%(config.best_models,config.model_name,str(fold)))
        # best_model = torch.load("checkpoints/bninception_bcelog/0/checkpoint.pth.tar")
        # model.load_state_dict(best_model["state_dict"])
        # test(test_loader,model,fold)
        # del modelё
        del model,train_gen,train_loader,test_gen,test_loader,val_gen
        # del test_gen
        # del val_gen
        if config.cuda_exists:
            torch.cuda.empty_cache()


if __name__ == "__main__":
    main()


# train  24.1   24.0         |         0.136  0.226           |         0.120  0.2235         |         0.116084  0.225199    | 14 hr 52 mintensor(0.1348, device='cuda:0', grad_fn=<MeanBackward1>)
