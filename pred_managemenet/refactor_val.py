from data_management.data import HumanDataset
from src.helpers import make_forecast
from utils import *
from configs.config import *
import pickle
from torch.utils.data import DataLoader
import random
import pandas as pd
# set random seed
random.seed(2050)
np.random.seed(2050)
torch.manual_seed(2050)
torch.cuda.manual_seed_all(2050)
######### 0. Prepare dt with filesl
# config.img_weight=512
# config.img_height=512
n_folds = config.n_folds
test_files = pd.read_csv(config.path_test_csv)
######### 1. Prepare dataloader
# model_path="/home/minasian/checkpoints/best_models/bnwith_aug_wo_ext_withsampler_v2get_xception_fold_0_model_best_f1_best_f1_0.477th_unif_train_0.4545_.pth.tar"
model_path="/home/minasian/checkpoints/best_models/bnwith_aug_with_ext_withsampler_v2get_xceptionimghw__299__fold_0_model_best_f1_best_f1_0.6374th_unif_train_0.4747_.pth.tar"


from src.funcs import load_model
os.environ["CUDA_VISIBLE_DEVICES"] = "0,1"
model,chp=load_model(path_to_model=model_path,return_checkpoint=True)
# model=nn.DataParallel(model)
val_df=chp["val_data_list"][0]
labels_l_l = [list(map(int, val_df.iloc[i].Target.split(' '))) for i in
                              range(0, val_df.shape[0])]
np.unique([m for k in labels_l_l  for m in k],return_counts=True)

# test_files = pd.read_csv(config.path_test_csv)
# test_gens=HumanDataset(test_files,config.test_data,augument=False,mode="pred",do_TTA=True,there_is_extension=False)
# test_loader=DataLoader(test_gens,8,shuffle=False,pin_memory=True,num_workers=6)
#
val_gens=HumanDataset(val_df,config.train_data,augument=False,mode="val",do_TTA=True,there_is_extension=False)
val_loader=DataLoader(val_gens,8,shuffle=False,pin_memory=True,num_workers=1)
# input,filepath=next(iter(test_loader))
# ######## 3. Make forecast (done once)

################################# FORECAST FUNCION ###################################
# plt.plot(best_model["val_losses"])

# model.cuda()
data_loader=val_loader
# best_model = torch.load("/home/minasian/PycharmProjects/prot_atlas/pac/checkpoints/bnwith_aug_wo_ext_withsampler/0/checkpoint.pth.tar")

limit=None
# next(model.parameters()).is_cuda
# next(iter(data_loader))


df_output=make_forecast(model,data_loader,limit=None)
df_output.to_dict()
with open('d1.pkl', 'wb') as f:
    pickle.dump(df_output.to_dict(), f)
all_preds=np.asarray([df_output["y_prob"].tolist()])
input_idx=np.asarray([i.split("/")[-1] for i in df_output["idx"].tolist()])
input_prob=np.mean(np.mean(all_preds[:,:,:,:],axis=0),axis=1)

def prepare_submission_file(input_prob,th,name_of_submission):
    '''
    :param input_prob:  array with float shaped (n_rows,28). NOT SHUFFLED !!
    :param input_idx: array of idx correspondence (n_rows,) SHOULD BE CAREFUL!!
    :param th:
    :param name_of_submission:
    :return:
    '''
    if isinstance(th,float):
        th_type="unf_th"
    else:
        th_type="dive"
        th=np.asarray(th)
    submission_args=[list(np.argwhere(i>th)[:,0]) for i in input_prob]
    submission_args=np.asarray(submission_args)
    # if there was no prediction => take the one with highes probability
    submission_args[[len(i)==0 for i in submission_args  ]]=np.asarray([i for i in np.argmax(input_prob[[len(i)==0 for i in submission_args  ]],axis=1)])
    submission_args=[[i] if not isinstance(i,list) else i for i in  submission_args]
    submission_args=[sorted(i) for i in submission_args]
    submissions=[]
    sample_submission_df = pd.read_csv(config.path_sample_submission_csv)
    for i in submission_args:
        subrow=' '.join(list([str(m) for m in i]))
        submissions.append(subrow)
    sample_submission_df['Predicted'] = submissions
    sample_submission_df.to_csv(name_of_submission+"_"+th_type+".csv", index=None)
# prepare_submission_file(input_prob,th=0.33,name_of_submission="bnwith_aug_with_ext_withsampler_v2get_xceptionimghw__299__fold_0_model_best_f1_best_f1_0.6374th_unif_train_0.4747")
#0.45, 0.5, 0.35, 0.3


#unif th is better