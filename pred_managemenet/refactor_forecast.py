from data_management.data import HumanDataset
from utils import *
from configs.config import *
from torch.utils.data import DataLoader
import random
import pandas as pd
from src.forecast_best import make_forecast, prepare_submission_file
from src.funcs import load_model
os.environ["CUDA_VISIBLE_DEVICES"] = "0,1"
# set random seed
random.seed(2050)
np.random.seed(2050)
torch.manual_seed(2050)
torch.cuda.manual_seed_all(2050)
######### 0. Prepare model
# config.img_weight=512
# config.img_height=512
n_folds = config.n_folds
test_files = pd.read_csv(config.path_test_csv)
# model_path="/home/minasian/checkpoints/best_models/bnwith_aug_wo_ext_withsampler_v2get_xception_fold_0_model_best_f1_best_f1_0.477th_unif_train_0.4545_.pth.tar"
model_path="/home/minasian/checkpoints/best_models/aug_with_extxceptionimghw__299__BCE_ADAMX_full_train_fold_0_model_best_loss_best_f1_0.4705th_unif_train_0.1717_.pth.tar"
model,chp=load_model(path_to_model=model_path,return_checkpoint=True,type="12")

######### 1. Preapre DataLoaders
# model=nn.DataParallel(model)
val_df=chp["val_data_list"]
th_new=chp["best_f1_th_unified_train"]
test_gens=HumanDataset(test_files,config.test_data,augument=False,mode="pred",do_TTA=True,there_is_extension=False)
test_loader=DataLoader(test_gens,8,shuffle=False,pin_memory=True,num_workers=6)

######### 2. Make forecast
# plt.plot(best_model["val_losses"])

# model.cuda()
data_loader=test_loader
limit=None
df_output=make_forecast(model,data_loader,limit=None)
all_preds=np.asarray([df_output["y_prob"].tolist()])
input_idx=np.asarray([i.split("/")[-1] for i in df_output["idx"].tolist()])
input_prob=np.mean(np.mean(all_preds[:,:,:,:],axis=0),axis=1)

######### 3. Make prediction files from forecasts
prepare_submission_file(input_prob,th=0.3,name_of_submission="aug_with_extxceptionimghw__299__BCE_ADAMX_full_train_fold_0_model_best_loss_best_f1_0.4705th_unif_train_0.3_.pth.tar.csv")
#0.45, 0.5, 0.35, 0.3


#unif th is better