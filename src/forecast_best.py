import torch
from tqdm import tqdm
import numpy as np
import pandas as pd
from configs.config import config
from six.moves import cPickle as pickle




def make_forecast(model,data_loader,limit=None,path_to_save_forecast=None):
    list_of_true_labels,list_of_probs_pred,list_of_ids= [[] for i in range(0,3)]
    n=0
    model.eval()
    for i, (x, y) in enumerate(tqdm(data_loader)):
        # x,y=next(iter(data_loader))
        # x=x.numpy()
        x_shape=x.size()
        batch_size=x_shape[0]
        augmentation_size=x_shape[1]
        y_id = y[0]
        y_true = np.asarray(y[1], dtype=np.float16) if y[1].__len__()!=0 else [None for i in range(0,batch_size)]
        with torch.no_grad():
            if x.size().__len__() == 5:
                x = x.view(-1, 4, config.img_height, config.img_weight)
            image_var = x.cuda(non_blocking=False)
            y_pred = model(image_var)
            y_pred=y_pred.view(batch_size,augmentation_size,-1)
            y_prob = np.asarray(y_pred.sigmoid().cpu().data.numpy(), dtype=np.float16)
            list_of_true_labels.append(y_true)
            list_of_probs_pred.append(y_prob)
            list_of_ids.append(y_id)
            n += 1
            if limit is not None:
                if n > limit:
                    break

    list_of_true_labels=np.concatenate(list_of_true_labels,axis=0).tolist()
    list_of_probs_pred=np.concatenate(list_of_probs_pred,axis=0).tolist()
    list_of_ids=np.concatenate(list_of_ids,axis=0).tolist()
    df_output = pd.DataFrame.from_dict({'idx': list_of_ids,
                                        'y_true': list_of_true_labels,
                                        'y_prob': list_of_probs_pred})
    torch.cuda.empty_cache()
    if path_to_save_forecast is not None:
        with open(path_to_save_forecast+'.pkl', 'wb') as f:
            pickle.dump(df_output, f)
    return df_output

def prepare_submission_file(input_prob,th,name_of_submission):
    '''
    :param input_prob:  array with float shaped (n_rows,28). NOT SHUFFLED !!
    :param input_idx: array of idx correspondence (n_rows,) SHOULD BE CAREFUL!!
    :param th:
    :param name_of_submission:
    :return:
    '''
    if isinstance(th,float):
        th_type="unf_th"
    else:
        th_type="dive"
        th=np.asarray(th)
    submission_args=[list(np.argwhere(i>th)[:,0]) for i in input_prob]
    submission_args=np.asarray(submission_args)
    # if there was no prediction => take the one with highes probability
    submission_args[[len(i)==0 for i in submission_args  ]]=np.asarray([i for i in np.argmax(input_prob[[len(i)==0 for i in submission_args  ]],axis=1)])
    submission_args=[[i] if not isinstance(i,list) else i for i in  submission_args]
    submission_args=[sorted(i) for i in submission_args]
    submissions=[]
    sample_submission_df = pd.read_csv(config.path_sample_submission_csv)
    for i in submission_args:
        subrow=' '.join(list([str(m) for m in i]))
        submissions.append(subrow)
    sample_submission_df['Predicted'] = submissions
    sample_submission_df.to_csv(name_of_submission+"_"+th_type+".csv", index=None)