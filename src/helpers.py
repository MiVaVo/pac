import os
from fnmatch import fnmatch
import numpy as np
import pandas as pd
import torch
from sklearn.preprocessing import MultiLabelBinarizer
from tqdm import tqdm
from utils import Logger
from datetime import datetime
from configs.config import config
import gc
from iterstrat.ml_stratifiers import MultilabelStratifiedShuffleSplit


from six.moves import cPickle as pickle
def make_folders(fold):
    # fold = 0
    # 4.1 mkdirs
    if not os.path.exists(config.submit):
        os.makedirs(config.submit)
    if not os.path.exists(config.weights + config.model_name + os.sep +str(fold)):
        os.makedirs(config.weights + config.model_name + os.sep +str(fold))
    if not os.path.exists(config.best_models):
        os.mkdir(config.best_models)
    if not os.path.exists("./logs/"):
        os.mkdir("./logs/")
    return None

def prepare_validation_strategy(all_files_shuffled,n_folds=5):
    n_of_each_holdout_sample = int(all_files_shuffled.shape[0] / n_folds)
    list_of_cross_validation_plan = []
    for i in range(0,n_folds):
        # print("Fold number "+str(i))
        # i=1
        cv_test_files=all_files_shuffled[(i)*n_of_each_holdout_sample:(i+1)*n_of_each_holdout_sample]
        diff_rows=set(all_files_shuffled.index).difference(set(cv_test_files.index))
        cv_train_files= all_files_shuffled.iloc[list(diff_rows),:]
        list_of_cross_validation_plan.append({"train":cv_train_files,"test":cv_test_files})
    return list_of_cross_validation_plan

def logger():
    if not os.path.exists("./logs/"):
        os.mkdir("./logs/")

    log = Logger()
    log.open("logs/%s_log_train.txt" % config.model_name, mode="a")
    log.write("\n----------------------------------------------- [START %s] %s\n\n" % (
    datetime.now().strftime('%Y-%m-%d %H:%M:%S'), '-' * 51))
    log.write(
        '                           |------------ Train -------------|----------- Valid -------------|----------Best Results---------|------------|\n')
    log.write(
        'mode     iter     epoch    |         loss   f1_macro        |         loss   f1_macro       |         loss   f1_macro       | time       |\n')
    log.write(
        '-------------------------------------------------------------------------------------------------------------------------------\n')
    return log

def list_all_contained_files_in_dir(root,pattern = "*.mp4" ,return_size=False):
    list_of_files=[]
    for path, subdirs, files in os.walk(root):
        for name in files:
            if type(pattern) == list:
                for i in pattern:
                    if fnmatch(name, i):
                        if return_size:
                            path_to_file=os.path.join(path, name)
                            size_of_file=os.stat(path_to_file).st_size
                            list_of_files.append([path_to_file,size_of_file])
                        else:
                            list_of_files.append(os.path.join(path, name))

            else:
                if fnmatch(name, pattern):
                    if return_size:
                        path_to_file = os.path.join(path, name)
                        size_of_file = os.stat(path_to_file).st_size
                        list_of_files.append([path_to_file, size_of_file])
                    else:
                        list_of_files.append(os.path.join(path, name))
    return list_of_files


def return_mlsss(n_folds, test_size, df, config,shurely_train_bool):
    if shurely_train_bool is not None:
        df_shurely_train=df[shurely_train_bool]
        df_rest=df[[not i for i in shurely_train_bool]]

        # n_folds, test_size, df, config=5, 0.5, all_files_ext, config
        mlb = MultiLabelBinarizer(classes=np.arange(0, config.num_classes))
        lb = np.asarray(
            [mlb.fit_transform([[int(j) for j in i.split(" ")]]) for i in df_rest.iloc[:, 1].tolist()])[:, 0, :]

        rmskf = MultilabelStratifiedShuffleSplit(n_splits=n_folds, test_size=test_size, random_state=config.random_state)
        # n=0
        files_strategy = []
        for train_index, test_index in rmskf.split(np.zeros(lb.shape[0]), lb):
            print("TRAIN:", train_index, "TEST:", test_index)
            # X_train, X_test = X[train_index], X[test_index]
            # y_train, y_test = lb[train_index], lb[test_index]
            files_strategy.append({"train": pd.concat([df_rest.iloc[train_index, :],df_shurely_train]), "test": df_rest.iloc[test_index, :]})
        return files_strategy
    else:
        # n_folds, test_size, df, config=5, 0.5, all_files_ext, config
        mlb = MultiLabelBinarizer(classes=np.arange(0, config.num_classes))
        lb = np.asarray(
            [mlb.fit_transform([[int(j) for j in i.split(" ")]]) for i in df.iloc[:, 1].tolist()])[:, 0, :]

        rmskf = MultilabelStratifiedShuffleSplit(n_splits=n_folds, test_size=test_size,
                                                 random_state=config.random_state)
        # n=0
        files_strategy = []
        for train_index, test_index in rmskf.split(np.zeros(lb.shape[0]), lb):
            print("TRAIN:", train_index, "TEST:", test_index)
            # X_train, X_test = X[train_index], X[test_index]
            # y_train, y_test = lb[train_index], lb[test_index]
            files_strategy.append({"train": df.iloc[train_index, :],
                                   "test": df.iloc[test_index, :]})
        return files_strategy


# def make_forecast(data_loader,model,current_model_path,limit=100):
#     # path_sample_submission_csv=config.path_sample_submission_csv
#     # current_model_path=model_pathes[0]
#     # type = "pred"
#     # current_model_path = "/home/minasian/PycharmProjects/prot_atlas/pac/checkpoints/best_models/bninception_bcelog_fold_0_model_best_loss.pth.tar"
#     best_model = torch.load(current_model_path) if config.cuda_exists else torch.load(current_model_path,map_location='cpu')
#     # plt.plot(best_model["val_losses"])
#     model.load_state_dict(best_model["state_dict"])
#     model.cuda() if config.cuda_exists else None
#     model.eval()
#     list_of_true_labels = []
#     list_of_probs_pred  = []
#     list_of_labels_pred = []
#     list_of_ids=[]
#     n =0
#     next(iter(data_loader))
#     for i,(x,y) in enumerate(tqdm(data_loader)):
#         y_id=y[0]
#         y_true=np.asarray(y[1],dtype=np.float16)
#         if len(y_true)==0:
#             y_true=None
#         with torch.no_grad():
#             if x.shape.__len__()==5:
#                 x = x.view(-1, 4, config.img_height, config.img_weight)
#             image_var = x.cuda(non_blocking=True) if config.cuda_exists else x
#             y_pred = model(image_var)
#
#             y_prob = np.asarray(y_pred.sigmoid().cpu().data.numpy(),dtype=np.float16)
#             list_of_true_labels.append(y_true)
#             list_of_probs_pred.append(y_prob)
#             list_of_ids.append(y_id)
#             list_of_labels_pred.append(np.argwhere(np.mean(y_prob,axis=0)>0.1836)[:,0])
#             n+=1
#             if limit is not None:
#                 if n>limit:
#                     break
#
#     df_output=pd.DataFrame.from_dict({'idx':list_of_ids,
#                             'y_true':list_of_true_labels,
#                             'y_prob':list_of_probs_pred,
#                             'y_hat':list_of_labels_pred})
#     del model, best_model
#     torch.cuda.empty_cache() if config.cuda_exists else None
#     del list_of_ids,list_of_true_labels,list_of_probs_pred,list_of_labels_pred
#     gc.collect()
#     return df_output

def make_forecast(model,data_loader,limit=None,save_as=None):
    list_of_true_labels,list_of_probs_pred,list_of_ids= [[] for i in range(0,3)]
    n=0
    for i, (x, y) in enumerate(tqdm(data_loader)):
        # x,y=next(iter(data_loader))
        # x=x.numpy()
        x_shape=x.size()
        batch_size=x_shape[0]
        augmentation_size=x_shape[1]
        y_id = y[0]
        y_true = np.asarray(y[1], dtype=np.float16) if y[1].__len__()!=0 else [None for i in range(0,batch_size)]
        with torch.no_grad():
            if x.size().__len__() == 5:
                x = x.view(-1, 4, config.img_height, config.img_weight)
            image_var = x.cuda(non_blocking=False)
            y_pred = model(image_var)
            y_pred=y_pred.view(batch_size,augmentation_size,-1)
            y_prob = np.asarray(y_pred.sigmoid().cpu().data.numpy(), dtype=np.float16)
            list_of_true_labels.append(y_true)
            list_of_probs_pred.append(y_prob)
            list_of_ids.append(y_id)
            n += 1
            if limit is not None:
                if n > limit:
                    break

    list_of_true_labels=np.concatenate(list_of_true_labels,axis=0).tolist()
    list_of_probs_pred=np.concatenate(list_of_probs_pred,axis=0).tolist()
    list_of_ids=np.concatenate(list_of_ids,axis=0).tolist()
    df_output = pd.DataFrame.from_dict({'idx': list_of_ids,
                                        'y_true': list_of_true_labels,
                                        'y_prob': list_of_probs_pred})
    torch.cuda.empty_cache()
    if save_as is not None:
        with open(str(save_as)+'.pickle', 'wb') as f:
            pickle.dump(df_output, f)

    return df_output

def make_forecast_without_sigmoid(data_loader,model,current_model_path,limit=100):
    # path_sample_submission_csv=config.path_sample_submission_csv
    # current_model_path=model_pathes[0]
    # type = "pred"
    # current_model_path = "/home/minasian/PycharmProjects/prot_atlas/pac/checkpoints/best_models/bninception_bcelog_fold_0_model_best_loss.pth.tar"
    best_model = torch.load(current_model_path) if config.cuda_exists else torch.load(current_model_path,map_location='cpu')

    # plt.plot(best_model["val_losses"])
    model.load_state_dict(best_model["state_dict"])
    model.cuda() if config.cuda_exists else None
    model.eval()
    list_of_true_labels=[]
    list_of_probs_pred = []
    list_of_labels_pred=[]
    list_of_ids=[]
    n =0
    for i,(x,y) in enumerate(tqdm(data_loader)):
        y_id=y[0]
        y_true=np.asarray(y[1],dtype=np.float16)
        if len(y_true)==0:
            y_true=None
        with torch.no_grad():
            if x.shape.__len__()==5:
                x = x[0, :, :, :, :]
            image_var = x.cuda(non_blocking=True) if config.cuda_exists else x
            y_pred = model(image_var)
            y_prob = np.asarray(y_pred.cpu().data.numpy(),dtype=np.float16)
            list_of_true_labels.append(y_true)
            list_of_probs_pred.append(y_prob)
            list_of_ids.append(y_id)
            list_of_labels_pred.append([0])
            n+=1
            if limit is not None:
                if n>limit:
                    break

    df_output=pd.DataFrame.from_dict({'idx':list_of_ids,
                            'y_true':list_of_true_labels,
                            'y_prob':list_of_probs_pred,
                            'y_hat':list_of_labels_pred})
    del model, best_model
    torch.cuda.empty_cache() if config.cuda_exists else None
    del list_of_ids,list_of_true_labels,list_of_probs_pred,list_of_labels_pred
    gc.collect()
    return df_output