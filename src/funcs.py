import gc
import math

from tqdm import tqdm
from models.model import get_xception
from utils import *
import  matplotlib.pyplot as plt
from configs.config import *
import pandas as pd
def fast_plot(x):
    '''
    :param x:  [n,w,h] array
    :return:
    '''
    fig_size=math.ceil(np.sqrt(x.shape[0]))
    fig, axs = plt.subplots(fig_size,fig_size, figsize=(15, 6))
    fig.subplots_adjust(hspace = .005, wspace=.001)
    axs = axs.ravel()
    for key,value in enumerate(x):
        axs[key].imshow(value)

def make_forecast(data_loader,model,limit=None):
    list_of_true_labels = []
    list_of_probs_pred  = []
    list_of_labels_pred = []
    list_of_ids=[]
    n =0
    next(iter(data_loader))
    for i,(x,y) in enumerate(tqdm(data_loader)):
        y_id=y[0]
        y_true=np.asarray(y[1],dtype=np.float16)
        if len(y_true)==0:
            y_true=None
        with torch.no_grad():
            if x.shape.__len__()==5:
                x = x.view(-1, 4, config.img_height, config.img_weight)
            image_var = x.cuda(non_blocking=True) if config.cuda_exists else x
            y_pred = model(image_var)

            y_prob = np.asarray(y_pred.sigmoid().cpu().data.numpy(),dtype=np.float16)
            list_of_true_labels.append(y_true)
            list_of_probs_pred.append(y_prob)
            list_of_ids.append(y_id)
            list_of_labels_pred.append(np.argwhere(np.mean(y_prob,axis=0)>0.1836)[:,0])
            n+=1
            if limit is not None:
                if n>limit:
                    break

    df_output=pd.DataFrame.from_dict({'idx':list_of_ids,
                            'y_true':list_of_true_labels,
                            'y_prob':list_of_probs_pred,
                            'y_hat':list_of_labels_pred})
    del  list_of_ids,list_of_true_labels,list_of_probs_pred,list_of_labels_pred
    torch.cuda.empty_cache() if config.cuda_exists else None
    gc.collect()
    return df_output

def load_model(path_to_model,return_checkpoint=False,type="custom_512",that_with_dropout=True):
    # model = get_net()
    # model=get_se_resnext50_32x4d()
    if type=="custom_512":
        model = get_xception(pretrained=False,with_dropout=that_with_dropout)
        model = nn.DataParallel(model)

        model = nn.Sequential(nn.DataParallel(nn.Sequential(nn.Conv2d(in_channels=4,
                                                                      out_channels=8,
                                                                      kernel_size=3,
                                                                      padding=1), nn.MaxPool2d(2, stride=2),
                                                            nn.Conv2d(in_channels=8, out_channels=4, kernel_size=3,
                                                                      padding=1))), model)
        best_model = torch.load(
            path_to_model)
        model.load_state_dict(best_model["state_dict"])
    else:

        model=get_xception(pretrained=False,modified_last=False,with_dropout=False)
        model = nn.DataParallel(model)
        best_model = torch.load(path_to_model) if config.cuda_exists else torch.load(path_to_model,
                                                                                          map_location='cpu')
        model.load_state_dict(best_model["state_dict"])

    model.cuda() if config.cuda_exists else None
    model.eval()
    if return_checkpoint:
        torch.cuda.empty_cache()
        return model,best_model
    else:
        gc.collect()
        del best_model
        torch.cuda.empty_cache()
        return model
